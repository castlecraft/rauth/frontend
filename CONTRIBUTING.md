# Development Setup

Easy to use VSCode devcontainer setup is available that configures backing services and tools required for development

Clone the repo, copy example configs and re-open in vscode devcontainer

```shell
git clone https://gitlab.com/castlecraft/rauth/frontend
code frontend
```

Once opened in devcontainer:

```shell
yarn
yarn start
# or
yarn start --ssl
```

Note: It needs R-Auth backend running on `http://0.0.0.0:3000` to make api calls.

# Checks before contributing

Merge requests are tested for conventional commits, format, lint, unit and e2e tests.

## Code format with prettier

```shell
yarn format
```

## Code lint with eslint

```shell
yarn lint --fix
```

## Run Unit tests

```shell
yarn test
```

## Run e2e tests

```shell
yarn e2e
```

## Write commit messages

Make sure to follow [Conventional Commits](https://conventionalcommits.org). The commitlint is used in pipeline to validate commit messages.

# Maintenance Guide

## Releases

Every time there are any merge request on `main` branch it will trigger automatic semantic release on gitlab.

## Dependency Upgrades

Run following command to upgrade dependencies to latest version.

```shell
yarn upgrade-interactive --latest
```

Once the dependencies are upgraded run the commands from Lint, format and tests section to verify successful upgrade.
