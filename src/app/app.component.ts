import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { BrandInfoService } from './common/brand-info/brand-info.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'R-Auth';
  constructor(
    readonly router: Router,
    private brandInfoService: BrandInfoService,
  ) {}
  ngOnInit(): void {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        const drawerContent = document.querySelector('.mat-drawer-content');
        if (drawerContent) {
          drawerContent.scrollTo(0, 0);
        }
      }
    });
    this.getFavicoUrl();
  }

  getFavicoUrl() {
    this.brandInfoService.retrieveBrandInfo().subscribe({
      next: brand => {
        if (brand.faviconUrl) {
          this.addFavicoURL(brand.faviconUrl);
        }
      },
    });
  }

  addFavicoURL(url: string) {
    const favicon: HTMLLinkElement | null =
      document.querySelector('#appFavicon');
    if (favicon) {
      favicon.href = url;
    }
  }

  scrolltoTop() {
    const options: ScrollToOptions = {
      top: 0,
      left: 0,
      behavior: 'smooth',
    };
    window.scrollTo(options);
  }
}
