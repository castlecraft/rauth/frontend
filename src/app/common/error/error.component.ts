import { Component, OnInit } from '@angular/core';
import { debounceTime, distinctUntilChanged, Observable, Subject } from 'rxjs';
import { DEFAULT_LOGIN_ERROR_MSG } from '../../auth/token/constants';
import { ErrorService } from './error.service';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.scss'],
})
export class ErrorComponent implements OnInit {
  errorArray = [];
  errorSubjectSubscription = new Subject<string>();
  errorObservabel$: Observable<string>;
  constructor(private erroservice: ErrorService) {
    this.errorObservabel$ = this.errorSubjectSubscription
      .asObservable()
      .pipe(debounceTime(400), distinctUntilChanged());
  }
  ngOnInit() {
    this.erroservice.errorSubject
      .pipe(distinctUntilChanged())
      .subscribe((res: any) => {
        this.errorSubjectSubscription.next(res);
      });

    this.errorObservabel$.subscribe(error => {
      this.setError(error);
    });
  }

  setError(error) {
    if (typeof error === 'object' && Object.keys(error).length > 0) {
      this.errorArray.push(this.extractError(error));
      setTimeout(a => {
        if (this.errorArray.length > 0) {
          this.errorArray.shift();
        }
      }, 3000);
    }
  }

  extractError(error) {
    if (error.error.message && typeof error.error.message === 'string') {
      return error.error.message;
    } else if (
      error.error.message?.message &&
      typeof error.error.message?.message == 'string'
    ) {
      return error.error.message?.message;
    } else {
      return DEFAULT_LOGIN_ERROR_MSG;
    }
  }
}
