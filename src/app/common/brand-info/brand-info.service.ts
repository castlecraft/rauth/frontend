import { Injectable } from '@angular/core';
import { TokenService } from '../../auth/token/token.service';

@Injectable({
  providedIn: 'root',
})
export class BrandInfoService {
  constructor(private readonly token: TokenService) {}

  retrieveBrandInfo() {
    return this.token.config;
  }
}
