import { TestBed } from '@angular/core/testing';

import { BrandInfoService } from './brand-info.service';
import { provideHttpClientTesting } from '@angular/common/http/testing';
import {
  provideHttpClient,
  withInterceptorsFromDi,
} from '@angular/common/http';

describe('BrandInfoService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [],
      providers: [
        provideHttpClient(withInterceptorsFromDi()),
        provideHttpClientTesting(),
      ],
    }),
  );

  it('should be created', () => {
    const service: BrandInfoService = TestBed.get(BrandInfoService);
    expect(service).toBeTruthy();
  });
});
