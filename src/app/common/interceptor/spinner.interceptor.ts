import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { HIDEERRORMSG } from '../../constants/app-constants';
import { ErrorService } from '../error/error.service';

@Injectable()
export class SpinnerInterceptor implements HttpInterceptor {
  requestStack: HttpRequest<any>[] = [];
  constructor(
    private spinner: NgxSpinnerService,
    private errorservice: ErrorService,
  ) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler,
  ): Observable<HttpEvent<any>> {
    this.spinner.show();
    this.requestStack.push(req);
    return next.handle(req).pipe(
      tap(
        event => {
          if (event instanceof HttpResponse) {
            this.hideSpinner();
          }
        },
        error => {
          if (error instanceof HttpErrorResponse) {
            if (req.headers.get('hideerrormessage') !== HIDEERRORMSG) {
              this.errorservice.errorSubject.next(error);
            }
            this.hideSpinner();
          }
        },
      ),
    );
  }
  hideSpinner() {
    this.requestStack.pop();
    if (this.requestStack.length === 0) {
      this.spinner.hide();
    }
  }
}
