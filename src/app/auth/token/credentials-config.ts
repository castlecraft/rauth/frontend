export interface OAuth2Config {
  clientId?: string;
  profileURL?: string;
  tokenURL?: string;
  authServerURL?: string;
  authorizationURL?: string;
  revocationURL?: string;
  scope?: string;
  callbackURL?: string;
  isAuthRequiredToRevoke?: boolean;
  enableChoosingAccount?: boolean;
  enableUserPhone?: boolean;
  issuerUrl?: string;
  logoUrl?: string;
  faviconUrl?: string;
  privacyUrl?: string;
  helpUrl?: string;
  termsUrl?: string;
  copyrightMessage?: string;
}
