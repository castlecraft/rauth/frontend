export interface BearerToken {
  expires_in: string;
  access_token: string;
  refresh_token?: string;
  id_token?: string;
  token_type: string;
}
