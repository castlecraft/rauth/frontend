export interface IBrowser {
  open(url: string, target: string): void;
}

export class Browser implements IBrowser {
  open(url: string, target: string): void {
    window.open(url, target);
  }
}
