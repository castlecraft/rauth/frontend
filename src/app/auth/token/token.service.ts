import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PlatformLocation } from '@angular/common';
import { Observable, forkJoin, of, throwError } from 'rxjs';
import { map, shareReplay, switchMap } from 'rxjs/operators';
import sjcl from 'sjcl';

import { StorageService } from '../storage/storage.service';
import { BearerToken } from './bearer-token.interface';
import {
  ACCESS_TOKEN,
  APP_X_WWW_FORM_URLENCODED,
  AUTHORIZATION,
  BEARER_HEADER_PREFIX,
  CODE_VERIFIER,
  CONTENT_TYPE,
  EXPIRES_IN,
  ID_TOKEN,
  LOGGED_IN,
  ONE_HOUR_IN_SECONDS_NUMBER,
  REFRESH_TOKEN,
  STATE,
  TEN_MINUTES_IN_SECONDS_NUMBER,
} from './constants';
import { OAuth2Config } from './credentials-config';
import { Browser } from './token.utils';
import { environment } from '../../../environments/environment';
import { IDTokenClaims } from '../../interfaces/id-token-claims.interfaces';

@Injectable({ providedIn: 'root' })
export class TokenService {
  private iab = new Browser();
  config: Observable<OAuth2Config & { session?: boolean }> = this.http
    .get<
      unknown & {
        infrastructureConsoleClientId?: string;
        issuerUrl?: string;
        allowedScopes?: string[];
      }
    >(
      this.platformLocation.getBaseHrefFromDOM().replace(/\/$/, '') +
        environment.routes.INFO,
    )
    .pipe(
      shareReplay(1),
      map(data => {
        return Object.assign(data, {
          clientId: data.infrastructureConsoleClientId,
          profileURL: data.issuerUrl + environment.routes.USER_INFO,
          tokenURL: data.issuerUrl + environment.routes.TOKEN_ENDPOINT,
          authServerURL: data.issuerUrl,
          authorizationURL: data.issuerUrl + environment.routes.CONFIRMATION,
          revocationURL:
            data.issuerUrl + environment.routes.REVOCATION_ENDPOINT,
          scope: data?.allowedScopes?.length
            ? data?.allowedScopes.join(' ')
            : environment.scope,
          callbackURL: data.issuerUrl + environment.routes.OAUTH2_CALLBACK,
          isAuthRequiredToRevoke: false,
          enableChoosingAccount: true,
        });
      }),
    );

  private headers = {
    [CONTENT_TYPE]: APP_X_WWW_FORM_URLENCODED,
  };

  constructor(
    private readonly http: HttpClient,
    private readonly store: StorageService,
    private readonly platformLocation: PlatformLocation,
  ) {}

  logIn() {
    this.generateAuthUrl().subscribe({
      next: url => {
        this.iab.open(url, '_self');
      },
    });
  }

  logOut() {
    this.store.clear();
    this.store.setItem(LOGGED_IN, 'false');
  }

  processCode(url: string) {
    const urlParts = new URL(url);
    const query = new URLSearchParams(urlParts.searchParams);
    const code = query.get('code') as string;
    if (!code) {
      return;
    }

    const error = query.get('error');
    if (error) {
      return;
    }

    const state = query.get('state') as string;

    this.config
      .pipe(
        switchMap(config => {
          const savedState = this.store.getItem(STATE);
          const codeVerifier = this.store.getItem(CODE_VERIFIER);
          if (savedState !== state) {
            return of({ ErrorInvalidState: true });
          }
          const req = {
            grant_type: 'authorization_code',
            code,
            redirect_uri: config.callbackURL,
            client_id: config.clientId,
            scope: config.scope,
            code_verifier: codeVerifier,
          };

          return this.http.post<BearerToken>(
            config.tokenURL,
            new URLSearchParams(req).toString(),
            {
              headers: this.headers,
            },
          );
        }),
      )
      .subscribe({
        next: (response: BearerToken) => {
          const expiresIn = response.expires_in || ONE_HOUR_IN_SECONDS_NUMBER;
          const expirationTime = new Date();
          expirationTime.setSeconds(
            expirationTime.getSeconds() + Number(expiresIn),
          );

          this.store.setItem(ACCESS_TOKEN, response.access_token);

          this.saveRefreshToken(response.refresh_token);

          this.store.setItem(EXPIRES_IN, expirationTime.toISOString());
          this.store.setItem(ID_TOKEN, response.id_token);
          this.store.setItem(LOGGED_IN, 'true');
          this.store.removeItem(STATE);
          this.store.removeItem(CODE_VERIFIER);
        },
        error: err => {
          // eslint-disable-next-line no-console
          console.error({
            timestamp: new Date().toISOString(),
            ...err,
          });
        },
      });
  }

  getToken() {
    const expiration = this.store.getItem(EXPIRES_IN);
    const accessToken = this.store.getItem(ACCESS_TOKEN);
    if (accessToken === 'undefined') {
      return throwError(() => ({ InvalidToken: { accessToken } }));
    }
    if (!expiration) {
      return throwError(() => ({ InvalidExpiration: { expiration } }));
    }

    const now = new Date();
    const expirationTime = new Date(expiration);

    // expire 10 min early
    expirationTime.setSeconds(
      expirationTime.getSeconds() - TEN_MINUTES_IN_SECONDS_NUMBER,
    );
    if (now < expirationTime) {
      return of(accessToken);
    }
    return this.refreshToken();
  }

  refreshToken() {
    return this.config.pipe(
      switchMap(config => {
        const requestBody = {
          grant_type: 'refresh_token',
          refresh_token: this.getRefreshToken(),
          redirect_uri: config.callbackURL,
          client_id: config.clientId,
          scope: config.scope,
        };
        return this.http
          .post<BearerToken>(
            config.tokenURL,
            new URLSearchParams(requestBody).toString(),
            {
              headers: this.headers,
            },
          )
          .pipe(
            switchMap(bearerToken => {
              const expirationTime = new Date();
              const expiresIn =
                bearerToken.expires_in || ONE_HOUR_IN_SECONDS_NUMBER;
              expirationTime.setSeconds(
                expirationTime.getSeconds() + Number(expiresIn),
              );
              this.store.setItem(EXPIRES_IN, expirationTime.toISOString());
              this.store.setItem(ACCESS_TOKEN, bearerToken.access_token);

              this.saveRefreshToken(bearerToken.refresh_token);
              return of(bearerToken.access_token);
            }),
          );
      }),
    );
  }

  generateAuthUrl() {
    return this.config.pipe(
      map(config => {
        const state = this.generateRandomString();
        this.store.setItem(STATE, state);

        const codeVerifier = this.generateRandomString();
        this.store.setItem(CODE_VERIFIER, codeVerifier);

        const challenge = sjcl.codec.base64
          .fromBits(sjcl.hash.sha256.hash(codeVerifier))
          .replace(/\+/g, '-')
          .replace(/\//g, '_')
          .replace(/=/g, '');

        let url = config.authorizationURL;
        url += '?scope=' + encodeURIComponent(config.scope);
        url += '&response_type=code';
        url += '&client_id=' + config.clientId;
        url += '&redirect_uri=' + encodeURIComponent(config.callbackURL);
        url += '&state=' + state;
        url += '&code_challenge_method=S256';
        url += '&prompt=select_account';
        url += '&code_challenge=' + challenge;

        return url;
      }),
    );
  }

  generateRandomString(stateLength: number = 32) {
    let result = '';
    const characters =
      'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < stateLength; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  saveRefreshToken(refreshToken: string) {
    this.store.setItem(REFRESH_TOKEN, refreshToken);
  }

  getRefreshToken(): string {
    return this.store.getItem(REFRESH_TOKEN);
  }

  loadProfile() {
    return this.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        return this.http.get<IDTokenClaims>(config.profileURL, { headers });
      }),
      shareReplay(1),
    );
  }

  getHeaders() {
    return this.getToken().pipe(
      map(token => {
        return new HttpHeaders({
          [AUTHORIZATION]: BEARER_HEADER_PREFIX + token,
        });
      }),
    );
  }

  getConfigAndHeaders() {
    return forkJoin({
      config: this.config,
      headers: this.getHeaders(),
    });
  }
}
