import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { get } from '@github/webauthn-json';
import { Observable, from } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { TokenService } from './token/token.service';
import { REVOKE_TOKEN_URL } from '../constants/url-paths';
import { HIDEERRORMSG } from '../constants/app-constants';

interface InfoResponse {
  session?: false;
  message?: any;
}

export interface OpenIDConfigurationeResponse {
  issuer: string;
  jwks_uri: string;
  authorization_endpoint: string;
  token_endpoint: string;
  userinfo_endpoint: string;
  revocation_endpoint: string;
  introspection_endpoint: string;
  response_types_supported: string[];
  subject_types_supported: string[];
  id_token_signing_alg_values_supported: string[];
}

@Injectable()
export class AuthService {
  public isAuth: boolean;
  public issuerUrl: string;

  constructor(
    private readonly http: HttpClient,
    private readonly token: TokenService,
  ) {}

  getConfig() {
    return this.token.config;
  }

  isAuthenticated(router?: Router): Observable<boolean> {
    return this.token.config.pipe(
      map((r: InfoResponse) => {
        if (r.session) {
          return r.session;
        } else if (router) {
          router.navigate(['login']);
        }
        return false;
      }),
    );
  }

  logIn(username: string, password: string, redirect: string, code?: string) {
    return this.token.config.pipe(
      switchMap(config => {
        return this.http.post(config.issuerUrl + environment.routes.LOGIN, {
          username,
          password,
          code,
          redirect,
        });
      }),
    );
  }

  passwordLessLogin(username: string, code: string, redirect: string) {
    return this.token.config.pipe(
      switchMap(config => {
        return this.http.post(
          config.issuerUrl + environment.routes.LOGIN_PASSWORDLESS,
          { username, code, redirect },
        );
      }),
    );
  }

  signUp(
    communicationEnabled: boolean,
    name: string,
    email: string,
    phone: string,
    password: string,
    redirect?: string,
  ) {
    return this.token.config.pipe(
      switchMap(config => {
        if (communicationEnabled) {
          return this.http.post(
            config.issuerUrl + environment.routes.SIGNUP_VIA_EMAIL,
            {
              name,
              email,
              redirect,
            },
          );
        }
        return this.http.post(config.issuerUrl + environment.routes.SIGNUP, {
          name,
          email,
          phone,
          password,
        });
      }),
    );
  }

  authorize(transactionId: string, consent: string = 'Allow') {
    return this.token.config.pipe(
      switchMap(config => {
        return this.http.post(config.issuerUrl + environment.routes.AUTHORIZE, {
          value: consent,
          transaction_id: transactionId,
        });
      }),
    );
  }

  verifyUser(username: string) {
    return this.token.config.pipe(
      switchMap(config => {
        return this.http.post(
          config.issuerUrl + environment.routes.CHECK_USER,
          {
            username,
          },
        );
      }),
    );
  }

  verifyLDAPUser(
    ldapUuid: string,
    username: string,
    password?: string,
    skipHeaders: boolean = false,
  ) {
    const headers = skipHeaders ? this.skiperrors() : {};
    return this.token.config.pipe(
      switchMap(config => {
        return this.http.post(
          config.issuerUrl + environment.routes.CHECK_LDAP_USER + ldapUuid,
          { username, password },
          { headers },
        );
      }),
    );
  }

  verifyPassword(username, password) {
    return this.token.config.pipe(
      switchMap(config => {
        return this.http.post(
          config.issuerUrl + environment.routes.CHECK_PASSWORD,
          {
            username,
            password,
          },
        );
      }),
    );
  }

  getSocialLogins() {
    return this.token.config.pipe(
      switchMap(config => {
        return this.http.get(
          config.issuerUrl + environment.routes.LIST_SOCIAL_LOGINS,
        );
      }),
    );
  }

  getLDAPClients() {
    return this.token.config.pipe(
      switchMap(config => {
        return this.http.get<
          { name: string; uuid: string; enabled: boolean }[]
        >(config.issuerUrl + environment.routes.LIST_LDAP_CLIENTS);
      }),
    );
  }

  forgotPassword(emailOrPhone: string) {
    return this.token.config.pipe(
      switchMap(config => {
        return this.http.post(
          config.issuerUrl + environment.routes.FORGOT_PASSWORD,
          { emailOrPhone },
        );
      }),
    );
  }

  sendOTP(userIdentifier: string) {
    return this.token.config.pipe(
      switchMap(config => {
        return this.http.post(
          config.issuerUrl + environment.routes.SEND_LOGIN_OTP,
          {
            userIdentifier,
          },
        );
      }),
    );
  }

  getSessionUsers() {
    return this.token.config.pipe(
      switchMap(config => {
        return this.http.get(
          config.issuerUrl + environment.routes.LIST_SESSION_USERS,
        );
      }),
    );
  }

  selectUser<T>(uuid) {
    return this.token.config.pipe(
      switchMap(config => {
        return this.http.post<T>(
          config.issuerUrl + environment.routes.CHOOSE_USER,
          { uuid },
        );
      }),
    );
  }

  webAuthnLogin(username: string, redirect?: string) {
    return this.token.config.pipe(
      switchMap(config => {
        return this.http
          .post<any>(
            config.issuerUrl + environment.routes.WEBAUTHN_LOGIN,
            { username },
            {
              headers: {
                'content-type': 'Application/Json',
              },
            },
          )
          .pipe(
            switchMap(challenge => {
              return from(get({ publicKey: challenge }));
            }),
            switchMap(credentials => {
              let params;
              if (redirect) params = { redirect };
              return this.http.post<{ redirect: string; loggedIn: boolean }>(
                config.issuerUrl + environment.routes.WEBAUTHN_LOGIN_CHALLENGE,
                credentials,
                {
                  headers: {
                    'content-type': 'application/Json',
                  },
                  params,
                },
              );
            }),
          );
      }),
    );
  }

  signUpViaPhone(name: string, unverifiedPhone: string) {
    return this.token.config.pipe(
      switchMap(config => {
        return this.http.post(
          config.issuerUrl + environment.routes.SIGNUP_VIA_PHONE,
          {
            name,
            unverifiedPhone,
          },
        );
      }),
    );
  }

  verifySignupPhone(unverifiedPhone: string, otp: string) {
    return this.token.config.pipe(
      switchMap(config => {
        return this.http.post(
          config.issuerUrl + environment.routes.VERIFY_PHONE_SIGNUP,
          {
            otp,
            unverifiedPhone,
          },
        );
      }),
    );
  }

  logInWithLDAP(
    client: { name: string; uuid: string },
    username: string,
    password: string,
    redirect: string,
    code?: string,
  ) {
    return this.token.config.pipe(
      switchMap(config => {
        return this.http.post(
          config.issuerUrl + environment.routes.LDAP_LOGIN + '/' + client.uuid,
          { username, password, redirect, code },
        );
      }),
    );
  }

  loginViaKerberos(realmUuid: string, redirect: string, code: string) {
    const options: { params?: { [param: string]: string } } = {};
    if (redirect) {
      options.params = { redirect };
    }
    return this.token.config.pipe(
      switchMap(config => {
        return this.http.post(
          config.issuerUrl +
            environment.routes.KERBEROS_LOGIN +
            '/' +
            realmUuid,
          { code },
          options,
        );
      }),
    );
  }

  revokeToken(url, clientId, uuid, token) {
    const payload = {
      clientId,
      uuid,
    };
    const headers = {
      Authorization: `Bearer ${token}`,
    };
    return this.http.post(url + REVOKE_TOKEN_URL, payload, { headers });
  }

  skiperrors() {
    return { hideerrormessage: HIDEERRORMSG };
  }
}
