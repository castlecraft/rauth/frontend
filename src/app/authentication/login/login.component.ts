import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { PlatformLocation } from '@angular/common';
import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subject, Subscription } from 'rxjs';
import { debounceTime, map, switchMap } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { AuthService } from '../../auth/auth.service';
import { DEFAULT_LOGIN_ERROR_MSG } from '../../auth/token/constants';
import { BrandInfoService } from '../../common/brand-info/brand-info.service';
import { PHONE_CHECK } from '../../constants/app-constants';
import {
  ACCESS_TYPE_KERBEROS,
  ACCESS_TYPE_LDAP,
  CLOSE,
  DOMAIN_USERNAME,
  DURATION,
  EMAIL_OR_PHONE,
  LOGIN_MESSAGE,
  NO_KEYS_REGISTERED,
  OTP_MESSAGE,
  PLEASE_CHECK_EMAIL,
  PLEASE_CHECK_USERNAME,
  SOMETHING_WENT_WRONG,
} from '../../constants/app-strings';
import { LoginChoice } from './login-choice';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
})
export class LoginComponent implements OnInit, OnDestroy {
  // Declared Public to allow Ahead of Time compilation for production
  public username: string = '';
  public password: string = '';
  public code: string = '';

  // subscriptionArray for memory leakage
  subscriptionArray: Subscription[] = [];
  showLoginChoice: boolean = false;
  showLDAPLogin: boolean = false;
  hideUsername: boolean = false;
  hidePassword: boolean = true;
  userInputPlaceholder = EMAIL_OR_PHONE;
  showEmailOrPhoneLogin: boolean = true;
  hideCode: boolean = true;
  enable2fa: boolean = false;
  serverError: string;
  socialLogins: { name: string; uuid: string }[] = [];
  ldapClients: { name: string; uuid: string }[] = [];
  redirect: string;
  showSocialLogins: boolean = false;
  enablePasswordLess: boolean = false;
  loginChoice: LoginChoice = LoginChoice.Standard;
  disableLoginChoice: boolean = false;
  disableResendOTP: boolean = false;
  cancelResendOTPCountDown: boolean = false;
  logoURL: string;
  copyrightMessage: string;
  service: string = LOGIN_MESSAGE;
  disableVerifyUserButton: boolean = false;
  disableVerifyPasswordButton: boolean = false;
  disableOnSubmitOTPButton: boolean = false;
  isPassHidden: boolean = true;
  accessType: string;
  accessId: string;
  organization: any = { name: '', uuid: '' };
  userEmailOrPhone: string;
  showLoginFrame: boolean = true;
  userUuid: string;
  hideForceOTP: boolean = true;
  disableSignup: boolean = false;
  codeValue: string;

  @ViewChild('password') passwordRef: ElementRef;
  @ViewChild('otp') otpRef: ElementRef;
  @ViewChild('username') usernameRef: ElementRef;
  @ViewChild('reSendOTP') reSendOTPRef: ElementRef;

  verifyUserForm = new FormGroup({
    username: new FormControl(this.username),
    organization: new FormControl(this.organization),
  });

  loginUserForm = new FormGroup({
    username: new FormControl(this.username),
    password: new FormControl(this.password),
  });

  submitOTPForm = new FormGroup({
    code: new FormControl(this.code),
  });

  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(map(result => result.matches));

  constructor(
    private authService: AuthService,
    private brandInfoService: BrandInfoService,
    private breakpointObserver: BreakpointObserver,
    private route: ActivatedRoute,
    private router: Router,
    private snackBar: MatSnackBar,
    private cd: ChangeDetectorRef,
    private readonly platformLocation: PlatformLocation,
  ) {}

  ngOnInit() {
    this.redirect = this.route.snapshot.queryParamMap.get('redirect');
    this.accessType = this.redirect
      ? new URLSearchParams(this.redirect).get('access_type')
      : this.route.snapshot.queryParamMap.get('access_type');
    this.accessId = this.redirect
      ? new URLSearchParams(this.redirect).get('access_id')
      : this.route.snapshot.queryParamMap.get('access_id');
    this.router.navigate([], {
      queryParams: { access_type: null, access_id: null },
      queryParamsHandling: 'merge',
    });
    this.kerberosLogin();
    this.getSocialLogins();
    this.getLDAPClients();
    this.getBrandInfo();
    // Set up the debounce logic for forget password
    this.setupForgetPasswordEmailSubscription();
  }

  ngAfterViewInit() {
    this.cd.detectChanges();
  }

  private forgetPasswordEmailSubject = new Subject<void>();
  private setupForgetPasswordEmailSubscription() {
    const subscription = this.forgetPasswordEmailSubject
      .pipe(
        debounceTime(2000), // Adjust the debounce time as needed
        switchMap(() =>
          this.authService.forgotPassword(this.getEmailOrPhone()),
        ),
      )
      .subscribe({
        next: success => {
          this.snackBar.open(PLEASE_CHECK_EMAIL, CLOSE, {
            duration: DURATION,
          });
          this.router.navigate(['login']);
        },
        error: error =>
          this.snackBar.open(
            error?.error?.message || PLEASE_CHECK_USERNAME,
            CLOSE,
            {
              duration: DURATION,
            },
          ),
      });

    // Store the subscription for cleanup later
    this.subscriptionArray.push(subscription);
  }
  forgotPassword() {
    // Emit a value to trigger the debounced request
    this.forgetPasswordEmailSubject.next();
  }

  onSubmitOTP() {
    this.disableOnSubmitOTPButton = true;
    this.submitOTPForm.controls.code.disable();

    if (this.loginChoice === LoginChoice.PasswordLess) {
      this.sendPasswordLessOTP();
    } else {
      this.sendStandardOTP();
    }
  }

  sendStandardOTP() {
    if (this.showLDAPLogin) {
      this.subscriptionArray.push(this.loginLDAPWithOTP());
    } else if (this.showEmailOrPhoneLogin) {
      this.subscriptionArray.push(this.loginWithOTP());
    }
  }

  loginWithOTP() {
    return this.authService
      .logIn(
        this.verifyUserForm.controls.username.value,
        this.loginUserForm.controls.password.value,
        this.redirect,
        this.submitOTPForm.controls.code.value,
      )
      .subscribe({
        next: (response: any) => {
          this.postOTPSendSuccess(response);
        },
        error: err => {
          this.postOTPSendError(err);
        },
      });
  }

  loginLDAPWithOTP() {
    return this.authService
      .logInWithLDAP(
        this.verifyUserForm.controls.organization.value,
        this.verifyUserForm.controls.username.value,
        this.loginUserForm.controls.password.value,
        this.redirect,
        this.submitOTPForm.controls.code.value,
      )
      .subscribe({
        next: (response: any) => {
          this.postOTPSendSuccess(response);
        },
        error: err => {
          this.postOTPSendError(err);
        },
      });
  }

  sendPasswordLessOTP() {
    this.subscriptionArray.push(
      this.authService
        .passwordLessLogin(
          this.getEmailOrPhone(),
          this.submitOTPForm.controls.code.value,
          this.redirect,
        )
        .subscribe({
          next: (response: any) => {
            this.postOTPSendSuccess(response);
          },
          error: err => {
            this.postOTPSendError(err);
          },
        }),
    );
  }

  getEmailOrPhone() {
    return this.containsNumericPlusAt(
      this.verifyUserForm.controls.username.value,
    )
      ? this.verifyUserForm.controls.username.value
      : this.userUuid;
  }

  postOTPSendSuccess(response) {
    this.submitOTPForm.controls.code.setErrors(null);
    this.redirectAsPerQuery(response.path);
  }

  postOTPSendError(error) {
    this.disableOnSubmitOTPButton = false;
    this.submitOTPForm.controls.code.enable();

    this.serverError =
      error?.error?.message?.message ||
      error?.message ||
      DEFAULT_LOGIN_ERROR_MSG;
    this.submitOTPForm.controls.code.setErrors({ incorrect: true });
  }

  callLDAP(index, selectedOrganization?) {
    this.subscriptionArray.push(
      this.authService
        .logInWithLDAP(
          selectedOrganization ? selectedOrganization : this.ldapClients[index],
          this.verifyUserForm.controls.username.value,
          this.loginUserForm.controls.password.value,
          this.redirect,
        )
        .subscribe({
          next: (response: any) => {
            this.loginUserForm.controls.password.setErrors(null);
            this.redirectAsPerQuery(response.path);
          },
          error: err => {
            if (selectedOrganization || index === this.ldapClients.length - 1) {
              this.clearLoginForm(true);
              this.serverError =
                err.error.message && err.error.message.message
                  ? err.error.message.message
                  : DEFAULT_LOGIN_ERROR_MSG;
              this.loginUserForm.controls.password.setErrors({
                incorrect: true,
              });
            } else {
              this.callLDAP(index + 1);
            }
          },
        }),
    );
  }

  onSubmitPassword(event) {
    this.disableVerifyPasswordButton = true;
    this.loginUserForm.controls.password.disable();
    if (this.enable2fa) {
      this.verifyPassword();
    } else if (this.showLDAPLogin) {
      this.callLDAP(0, this.verifyUserForm.controls.organization.value);
    } else {
      this.logIn();
    }
  }

  logIn() {
    this.subscriptionArray.push(
      this.authService
        .logIn(
          this.verifyUserForm.controls.username.value,
          this.loginUserForm.controls.password.value,
          this.redirect,
        )
        .subscribe({
          next: (response: any) => {
            this.loginUserForm.controls.password.setErrors(null);
            this.redirectAsPerQuery(response.path);
          },
          error: err => {
            this.disableVerifyPasswordButton = false;
            this.loginUserForm.controls.password.enable();

            this.serverError = err.error.message;
            this.loginUserForm.controls.password.setErrors({
              incorrect: true,
            });
          },
        }),
    );
  }

  verifyPassword() {
    if (this.showLDAPLogin) {
      this.subscriptionArray.push(this.ldapPasswordVerification());
    } else if (this.showEmailOrPhoneLogin) {
      this.subscriptionArray.push(this.passwordVerification());
    }
  }

  ldapPasswordVerification() {
    return this.authService
      .verifyLDAPUser(
        this.verifyUserForm.controls.organization.value.uuid,
        this.verifyUserForm.controls.username.value,
        this.loginUserForm.controls.password.value,
      )
      .subscribe({
        next: (response: any) => {
          this.postPasswordVerificationSuccess();
          if (response?.resendOTPInfo?.resendOTPAfterSec) {
            this.delayResendOTP(response?.resendOTPInfo?.resendOTPAfterSec);
          } else {
            this.delayResendOTP(response?.resendOTPInfo?.resendOTPDelayInSec);
          }
        },
        error: err => {
          this.postPasswordVerificationError(err);
        },
      });
  }

  passwordVerification() {
    return this.authService
      .verifyPassword(
        this.verifyUserForm.controls.username.value,
        this.loginUserForm.controls.password.value,
      )
      .subscribe({
        next: (response: any) => {
          this.postPasswordVerificationSuccess();
          if (response?.resendOTPInfo?.resendOTPAfterSec) {
            this.delayResendOTP(response?.resendOTPInfo?.resendOTPAfterSec);
          } else {
            this.delayResendOTP(response?.resendOTPInfo?.resendOTPDelayInSec);
          }
        },
        error: err => {
          this.postPasswordVerificationError(err);
        },
      });
  }

  postPasswordVerificationSuccess() {
    this.loginUserForm.controls.password.setErrors(null);
    this.hideCode = false;
    this.hidePassword = true;
    this.snackBar.open(this.SendOtpMessage(this.userEmailOrPhone), CLOSE, {
      duration: DURATION,
    });
    this.disableVerifyPasswordButton = false;
    this.loginUserForm.controls.password.enable();

    setTimeout(() => this.otpRef.nativeElement.focus());
  }

  postPasswordVerificationError(err) {
    this.disableVerifyPasswordButton = false;
    this.loginUserForm.controls.password.enable();

    this.serverError = err.error.message?.message || err.error.message;
    this.loginUserForm.controls.password.setErrors({
      incorrect: true,
    });
  }

  containsNumericPlusAt(inputString) {
    const phonePattern = /^\+\d{10,}$/; // Matches '+', followed by at least 10 digits
    const emailPattern = /^[\w.-]+@[a-zA-Z\d.-]+\.[a-zA-Z]{2,}$/; // Matches valid email format

    // Check if it matches either phone number or email pattern
    return phonePattern.test(inputString) || emailPattern.test(inputString);
  }

  verifyUser() {
    if (
      this.containsNumericPlusAt(this.verifyUserForm.controls.username.value) ||
      this.ldapClients.length === 0
    ) {
      this.showEmailOrPhoneLogin = true;
      this.showLDAPLogin = false;
    } else {
      this.showEmailOrPhoneLogin = false;
      this.showLDAPLogin = true;
    }
    this.disableVerifyUserButton = true;
    this.verifyUserForm.controls.username.disable();
    if (this.showEmailOrPhoneLogin) {
      this.verifyDomainUser();
    } else if (this.showLDAPLogin) {
      this.verifyLDAPUser(0, this.verifyUserForm.controls.organization.value);
    }
  }

  verifyLDAPUser(index: number, ldapClient?) {
    this.subscriptionArray.push(
      this.authService
        .verifyLDAPUser(
          ldapClient && ldapClient.uuid
            ? ldapClient.uuid
            : this.ldapClients[index].uuid,
          this.verifyUserForm.controls.username.value,
          undefined,
          !(
            (ldapClient && ldapClient.uuid) ||
            index === this.ldapClients.length - 1
          ),
        )
        .subscribe({
          next: (response: any) => {
            this.userEmailOrPhone =
              response?.user?.email || response?.user?.phone;
            this.userUuid = response?.user?.uuid;
            this.verifyUserForm.controls.organization.setValue(
              this.ldapClients[index],
            );
            this.postUserVerificationSuccess(response);
          },
          error: err => {
            if (
              (ldapClient && ldapClient.uuid) ||
              index === this.ldapClients.length - 1
            ) {
              this.disableVerifyUserButton = false;
              this.verifyUserForm.controls.username.enable();
              this.serverError =
                err.error?.message?.message || SOMETHING_WENT_WRONG;
              this.verifyUserForm.controls.username.setErrors({
                incorrect: true,
              });
            } else {
              this.verifyLDAPUser(index + 1);
            }
          },
        }),
    );
  }

  verifyDomainUser() {
    this.subscriptionArray.push(
      this.authService
        .verifyUser(this.verifyUserForm.controls.username.value)
        .subscribe({
          next: (response: any) => {
            this.postUserVerificationSuccess(response);
            if (
              response.user.enablePasswordLess &&
              response.user.source_id === PHONE_CHECK
            ) {
              this.showPasswordLessLogin();
            }
          },
          error: err => {
            this.disableVerifyUserButton = false;
            this.verifyUserForm.controls.username.enable();
            this.serverError = err.error.message;
            this.verifyUserForm.controls.username.setErrors({
              incorrect: true,
            });
          },
        }),
    );
  }

  postUserVerificationSuccess(response) {
    this.verifyUserForm.controls.username.setErrors(null);
    this.loginUserForm.controls.username.setValue(
      this.verifyUserForm.controls.username.value,
    );
    this.hideUsername = true;
    this.hidePassword = false;
    this.enable2fa = response.user.enable2fa || response.user.enableOTP;
    this.enablePasswordLess = response.user.enablePasswordLess;
    this.userUuid = response.user.uuid;
    this.disableLoginChoice = this.enablePasswordLess;
    // TODO: https://github.com/angular/angular/issues/12463
    setTimeout(() => this.passwordRef.nativeElement.focus());

    this.disableVerifyUserButton = false;
    this.verifyUserForm.controls.username.enable();
  }

  resendOTP() {
    this.disableResendOTP = true;
    this.subscriptionArray.push(
      this.authService.sendOTP(this.getEmailOrPhone()).subscribe({
        next: (success: any) => {
          if (success?.resendOTPInfo?.resendOTPAfterSec) {
            this.delayResendOTP(success?.resendOTPInfo?.resendOTPAfterSec);
          } else {
            this.delayResendOTP(success?.resendOTPInfo?.resendOTPDelayInSec);
          }
        },
        error: error => {},
      }),
    );
  }

  delayResendOTP(resendOTPAfterSec) {
    this.disableResendOTP = true;
    this.cancelResendOTPCountDown = false;
    const endTime = new Date().getTime() + resendOTPAfterSec * 1000;

    const updateCountdown = () => {
      if (this.cancelResendOTPCountDown) {
        return;
      }

      const now = new Date().getTime();
      const distance = endTime - now;
      const secondsRemaining = Math.floor(distance / 1000);

      if (secondsRemaining <= 0) {
        this.disableResendOTP = false;
        this.submitOTPForm.controls.code.setErrors(null);
        return;
      }
      this.serverError = `Resend OTP after ${secondsRemaining} sec`;
      this.submitOTPForm.controls.code.setErrors({ incorrect: true });

      requestAnimationFrame(updateCountdown);
    };
    updateCountdown();
  }

  connectWith(login) {
    this.subscriptionArray.push(
      this.authService.getConfig().subscribe({
        next: info => {
          this.setRedirect();
          window.location.href =
            info.issuerUrl +
            environment.routes.SOCIAL_LOGIN_CALLBACK_ENDPOINT +
            '/' +
            login.uuid +
            '?redirect=' +
            encodeURIComponent(this.redirect);
        },
        error: error => {},
      }),
    );
  }

  getLDAPClients() {
    this.subscriptionArray.push(
      this.authService.getLDAPClients().subscribe({
        next: response => {
          this.ldapClients = response;
          const accessIds = response.map(client => client.uuid);
          if (this.accessType === ACCESS_TYPE_LDAP) {
            if (accessIds.includes(this.accessId)) {
              this.ldapClients = this.ldapClients.filter(
                client => client.uuid === this.accessId,
              );
            }
            this.setLDAPLogin();
          }
          if (this.ldapClients.length > 0) {
            this.showLoginChoice = true;
          }
        },
        error: err => {},
      }),
    );
  }

  getSocialLogins() {
    this.subscriptionArray.push(
      this.authService.getSocialLogins().subscribe({
        next: (response: { name: string; uuid: string }[]) => {
          this.socialLogins = response;
          if (this.socialLogins.length > 0) {
            this.showSocialLogins = true;
          }
        },
        error: err => {},
      }),
    );
  }

  showPasswordLessLogin() {
    this.loginChoice = LoginChoice.PasswordLess;
    this.hidePassword = true;
    this.hideCode = false;
    this.resendOTP();
  }

  webAuthnLogin() {
    this.subscriptionArray.push(
      this.authService
        .webAuthnLogin(this.getEmailOrPhone(), this.redirect)
        .subscribe({
          next: response => {
            this.redirectAsPerQuery(response.redirect);
          },
          error: error => {
            this.snackBar.open(
              error?.error?.message || NO_KEYS_REGISTERED,
              CLOSE,
              {
                duration: DURATION,
              },
            );
          },
        }),
    );
  }

  chooseAccount() {
    const queryParams = { ...this.route.snapshot.queryParams };
    delete queryParams.login_type;
    this.router.navigate([environment.routes.ACCOUNT_CHOOSE_ROUTE], {
      queryParams,
    });
  }

  getBrandInfo() {
    this.subscriptionArray.push(
      this.brandInfoService.retrieveBrandInfo().subscribe({
        next: (brand: any) => {
          this.logoURL = brand.logoUrl;
          this.copyrightMessage = brand.copyrightMessage;
          this.service = brand.service;
          this.disableSignup = brand.disableSignup;
        },
        error: error => {},
      }),
    );
  }

  redirectAsPerQuery(redirectPath: string) {
    const user = new URLSearchParams(redirectPath).get('user');
    if (this.accessType === ACCESS_TYPE_KERBEROS && user) {
      this.userUuid = user;
      this.resendOTP();
      this.showLoginFrame = true;
      this.hideUsername = true;
      this.hideForceOTP = false;
      return;
    }

    const loginType = this.route.snapshot.queryParams.login_type;
    if (!loginType || this.accessType === ACCESS_TYPE_KERBEROS) {
      this.showLoginFrame = false;
      window.location.href = redirectPath;
      return;
    } else if (loginType === 'add_account') {
      this.chooseAccount();
      return;
    }
  }

  clearLoginForm(password?) {
    if (password) {
      this.hideUsername = true;
      this.hidePassword = false;
      this.showLDAPLogin = true;
    } else {
      this.hideUsername = false;
      this.hidePassword = true;
      this.showLDAPLogin = false;
    }
    this.hideCode = true;
    this.cancelResendOTPCountDown = true;
    this.serverError = null;

    this.verifyUserForm.reset();
    this.loginUserForm.reset();
    this.submitOTPForm.reset();

    this.disableVerifyUserButton = false;
    this.disableVerifyPasswordButton = false;

    this.showEmailOrPhoneLogin = true;
    this.userInputPlaceholder = EMAIL_OR_PHONE;
  }

  togglePassHidden() {
    this.isPassHidden = !this.isPassHidden;
  }

  setLDAPLogin() {
    this.showLDAPLogin = true;
    this.showEmailOrPhoneLogin = false;
    this.userInputPlaceholder = DOMAIN_USERNAME;
  }

  setEmailOrPhoneLogin() {
    this.showEmailOrPhoneLogin = true;
    this.showLDAPLogin = false;
    this.userInputPlaceholder = EMAIL_OR_PHONE;
  }

  toggleLogin() {
    this.showLDAPLogin = !this.showLDAPLogin;
    this.showEmailOrPhoneLogin = !this.showEmailOrPhoneLogin;

    if (this.showLDAPLogin) {
      this.userInputPlaceholder = DOMAIN_USERNAME;
    }

    if (this.showEmailOrPhoneLogin) {
      this.userInputPlaceholder = EMAIL_OR_PHONE;
    }
  }

  setRedirect() {
    const query: any = { ...this.route.snapshot.queryParamMap };
    if (!this.redirect && query.params) {
      const params = new URLSearchParams();
      for (const key in query.params) {
        if (query.params.hasOwnProperty(key)) {
          params.set(key, query.params[key]);
        }
      }
      this.redirect =
        this.platformLocation.getBaseHrefFromDOM().replace(/\/$/, '') +
        environment.routes.ACCOUNT_CHOOSE_ROUTE +
        '?' +
        params.toString();
    }
  }

  navigateTo(url) {
    this.router.navigate([url]);
  }

  handleKeyUpEnter(): void {
    if (!this.hideCode) {
      this.onSubmitOTP();
    } else if (!this.hideForceOTP) {
      this.kerberosUserLogin();
    }
  }

  kerberosUserLogin() {
    this.codeValue = this.submitOTPForm.controls.code.value;
    this.disableOnSubmitOTPButton = true;
    this.submitOTPForm.controls.code.disable();
    this.submitOTPForm.controls.code.setErrors(null);

    this.hideUsername = false;
    this.hideForceOTP = true;

    this.kerberosLogin();
  }

  postOTPKerberosUserError(err) {
    this.showLoginFrame = true;
    this.hideUsername = true;
    this.hideForceOTP = false;
    this.disableOnSubmitOTPButton = false;
    this.submitOTPForm.controls.code.enable();
    this.submitOTPForm.controls.code.setValue('');
    this.serverError = err?.error?.message || DEFAULT_LOGIN_ERROR_MSG;
    this.submitOTPForm.controls.code.setErrors({ incorrect: true });
  }

  kerberosLogin() {
    if (this.accessType === ACCESS_TYPE_KERBEROS && this.accessId) {
      this.showLoginFrame = false;
      this.subscriptionArray.push(
        this.authService
          .loginViaKerberos(this.accessId, this.redirect, this.codeValue)
          .subscribe({
            next: (response: any) => {
              this.redirectAsPerQuery(response.path);
            },
            error: err => {
              if (this.codeValue) {
                this.postOTPKerberosUserError(err);
              } else {
                this.showLoginFrame = true;
                this.clearLoginForm();
                this.setEmailOrPhoneLogin();
              }
            },
          }),
      );
    }
  }

  SendOtpMessage(userEmailOrPhone) {
    let messaseString = OTP_MESSAGE;
    messaseString = messaseString.replace(
      '#',
      userEmailOrPhone && userEmailOrPhone.includes('@')
        ? this.maskEmail(userEmailOrPhone)
        : this.maskPhone(userEmailOrPhone),
    );
    return messaseString;
  }

  maskPhone(phoneNumber) {
    const maskedPart = '*'.repeat(phoneNumber.length - 4);
    const visiblePart = phoneNumber.slice(-4);
    return maskedPart + visiblePart;
  }

  maskEmail(email) {
    const [localPart, domain] = email.split('@');
    const maskedLocalPart =
      localPart.slice(0, 2) + '*'.repeat(localPart.length - 2);
    return `${maskedLocalPart}@${domain}`;
  }

  ngOnDestroy(): void {
    this.subscriptionArray.forEach((subscription: Subscription) =>
      subscription.unsubscribe(),
    );
  }
}
