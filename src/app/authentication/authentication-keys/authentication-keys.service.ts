import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { switchMap } from 'rxjs/operators';
import { from } from 'rxjs';
import {
  create,
  parseCreationOptionsFromJSON,
} from '@github/webauthn-json/browser-ponyfill';
import { environment } from '../../../environments/environment';
import { TokenService } from '../../auth/token/token.service';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationKeysService {
  constructor(
    private readonly http: HttpClient,
    private readonly token: TokenService,
  ) {}

  addAuthKey(userUuid: string) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        return this.http
          .post<any>(
            config.issuerUrl +
              environment.routes.WEBAUTHN_REQUEST_REGISTER_ENDPOINT,
            { userUuid },
            { headers },
          )
          .pipe(
            switchMap(challenge => {
              return from(
                create(parseCreationOptionsFromJSON({ publicKey: challenge })),
              );
            }),
            switchMap(credentials => {
              const body = { credentials, userUuid };
              return this.http.post<{ registered: string }>(
                config.issuerUrl +
                  environment.routes.WEBAUTHN_REGISTER_ENDPOINT,
                body,
                { headers },
              );
            }),
          );
      }),
    );
  }

  renameAuthKey(uuid: string, name: string) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        return this.http.post<any>(
          config.issuerUrl +
            environment.routes.WEBAUTHN_RENAME_AUTHN_ENDPOINT +
            '/' +
            uuid,
          { name },
          { headers },
        );
      }),
    );
  }

  removeAuthKey(uuid: string, accessToken: string) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        return this.http.post<any>(
          config.issuerUrl +
            environment.routes.WEBAUTHN_REMOVE_AUTHN_ENDPOINT +
            '/' +
            uuid,
          {},
          { headers },
        );
      }),
    );
  }

  getAuthenticators(userUuid: string, accessToken: string) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        return this.http.get(
          config.issuerUrl +
            environment.routes.WEBAUTHN_AUTHNS_ENDPOINT +
            '/' +
            userUuid,
          { headers },
        );
      }),
    );
  }
}
