import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AccountComponent } from './account/account.component';
import { AuthenticationKeysComponent } from './authentication-keys/authentication-keys.component';
import { RenameAuthKeyDialog } from './authentication-keys/rename-key-dialog.component';
import { BrandInfoComponent } from './brand-info/brand-info.component';
import { ChooseAccountComponent } from './choose-account/choose-account.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { VerifyEmailComponent } from './verify-email/verify-email.component';
import { VerifyGeneratePasswordComponent } from './verify-generate-password/verify-generate-password.component';
import { AuthenticationKeysService } from './authentication-keys/authentication-keys.service';
import { AuthServerMaterialModule } from '../auth-server-material/auth-server-material.module';
import { PasswordRequirementComponent } from '../common/password-requirement/password-requirement.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ListingService } from '../administration/listing/listing.service';
import { ConfirmationDialogComponent } from '../administration/confirmation-dialog/confirmation-dialog.component';

@NgModule({
  declarations: [
    LoginComponent,
    SignupComponent,
    AccountComponent,
    PasswordRequirementComponent,
    VerifyGeneratePasswordComponent,
    ChooseAccountComponent,
    BrandInfoComponent,
    AuthenticationKeysComponent,
    RenameAuthKeyDialog,
    VerifyEmailComponent,
    ConfirmationDialogComponent,
  ],
  imports: [
    RouterModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AuthServerMaterialModule,
  ],
  providers: [AuthenticationKeysService, ListingService],
})
export class AuthenticationModule {}
