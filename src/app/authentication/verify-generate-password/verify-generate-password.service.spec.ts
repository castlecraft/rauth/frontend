import { TestBed } from '@angular/core/testing';

import { VerifyGeneratePasswordService } from './verify-generate-password.service';
import { provideHttpClientTesting } from '@angular/common/http/testing';
import {
  provideHttpClient,
  withInterceptorsFromDi,
} from '@angular/common/http';

describe('VerifyGeneratePasswordService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [],
      providers: [
        provideHttpClient(withInterceptorsFromDi()),
        provideHttpClientTesting(),
      ],
    }),
  );

  it('should be created', () => {
    const service: VerifyGeneratePasswordService = TestBed.get(
      VerifyGeneratePasswordService,
    );
    expect(service).toBeTruthy();
  });
});
