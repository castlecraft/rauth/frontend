import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { TokenService } from '../../auth/token/token.service';
import { switchMap } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class VerifyGeneratePasswordService {
  constructor(
    private readonly http: HttpClient,
    private readonly token: TokenService,
  ) {}

  updateUser(verificationCode: string, password: string) {
    return this.token.config.pipe(
      switchMap(config => {
        return this.http.post(
          config.issuerUrl + environment.routes.GENERATE_PASSWORD,
          {
            verificationCode,
            password,
          },
        );
      }),
    );
  }

  verifyEmail(verificationCode: string) {
    return this.token.config.pipe(
      switchMap(config => {
        return this.http.post(
          config.issuerUrl + environment.routes.VERIFY_EMAIL,
          {
            verificationCode,
          },
        );
      }),
    );
  }
}
