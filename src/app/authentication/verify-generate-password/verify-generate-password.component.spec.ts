import { waitForAsync, ComponentFixture, TestBed } from '@angular/core/testing';
import { Component } from '@angular/core';
import { VerifyGeneratePasswordComponent } from './verify-generate-password.component';
import { AuthServerMaterialModule } from '../../auth-server-material/auth-server-material.module';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule } from '@angular/forms';
import { provideHttpClientTesting } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  provideHttpClient,
  withInterceptorsFromDi,
} from '@angular/common/http';

@Component({ selector: 'app-password-requirement', template: '' })
class PasswordRequirementComponent {}

describe('VerifyGeneratePasswordComponent', () => {
  let component: VerifyGeneratePasswordComponent;
  let fixture: ComponentFixture<VerifyGeneratePasswordComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        VerifyGeneratePasswordComponent,
        PasswordRequirementComponent,
      ],
      imports: [
        AuthServerMaterialModule,
        RouterTestingModule,
        FormsModule,
        BrowserAnimationsModule,
      ],
      providers: [
        provideHttpClient(withInterceptorsFromDi()),
        provideHttpClientTesting(),
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerifyGeneratePasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
