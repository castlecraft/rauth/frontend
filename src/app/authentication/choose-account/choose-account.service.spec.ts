import { TestBed } from '@angular/core/testing';

import { ChooseAccountService } from './choose-account.service';
import { provideHttpClientTesting } from '@angular/common/http/testing';
import {
  provideHttpClient,
  withInterceptorsFromDi,
} from '@angular/common/http';

describe('ChooseAccountService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [],
      providers: [
        provideHttpClient(withInterceptorsFromDi()),
        provideHttpClientTesting(),
      ],
    }),
  );

  it('should be created', () => {
    const service: ChooseAccountService = TestBed.get(ChooseAccountService);
    expect(service).toBeTruthy();
  });
});
