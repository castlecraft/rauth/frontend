import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { TokenService } from '../../auth/token/token.service';
import { switchMap } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ChooseAccountService {
  constructor(
    private readonly http: HttpClient,
    private readonly token: TokenService,
  ) {}

  getSessionUsers() {
    return this.token.config.pipe(
      switchMap(config => {
        return this.http.get(
          config.issuerUrl + environment.routes.LIST_SESSION_USERS,
        );
      }),
    );
  }

  chooseUser(uuid) {
    return this.token.config.pipe(
      switchMap(config => {
        return this.http.post(
          config.issuerUrl + environment.routes.CHOOSE_USER,
          { uuid },
        );
      }),
    );
  }

  logoutUser(uuid) {
    return this.token.config.pipe(
      switchMap(config => {
        return this.http.get(
          config.issuerUrl + environment.routes.LOGOUT + '/' + uuid,
        );
      }),
    );
  }
}
