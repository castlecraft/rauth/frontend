import { Injectable } from '@angular/core';
import { TokenService } from '../../auth/token/token.service';

@Injectable({
  providedIn: 'root',
})
export class SignupService {
  constructor(private readonly token: TokenService) {}

  getServerInfo() {
    return this.token.config;
  }
}
