import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { timer } from 'rxjs';
import { AuthService } from '../../auth/auth.service';
import { ServerInfo } from '../../common/server-info.interface';
import {
  CLOSE,
  LONG_DURATION,
  PLEASE_CHECK_EMAIL,
} from '../../constants/app-strings';
import { OTP_SENT_TO } from '../../constants/messages';
import { SignupService } from './signup.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
})
export class SignupComponent implements OnInit {
  public name: string;
  public email: string;
  public phone: string;
  public password: string;
  public communicationEnabled: boolean = false;
  otp: string;
  enableUserPhone = false;
  isSignUpViaEmail = true;
  isSignUpViaPhone = false;
  isNameAndPhoneDisabled = false;
  isOTPSendButtonDisabled = false;
  isSignUpDisabled = false;
  redirect: string;
  postSignupRedirect: string;
  isEmailDisabled = false;

  constructor(
    private authService: AuthService,
    private snackBar: MatSnackBar,
    private signupService: SignupService,
    private router: Router,
    private route: ActivatedRoute,
    private location: Location,
  ) {}

  ngOnInit() {
    this.signupService.getServerInfo().subscribe({
      next: (response: ServerInfo) => {
        if (response.communication) {
          this.communicationEnabled = response.communication;
          this.enableUserPhone = response.enableUserPhone;
        }
        if (response.disableSignup) this.isSignUpDisabled = true;
      },
    });
    this.route.queryParams.subscribe(params => {
      this.redirect = params.redirect;
      this.postSignupRedirect = params['post-signup'];
    });
  }

  onSubmit() {
    this.authService
      .signUp(
        this.communicationEnabled,
        this.name,
        this.email,
        this.phone,
        this.password,
        this.redirect,
      )
      .subscribe({
        next: (response: any) => {
          this.isNameAndPhoneDisabled = true;
          this.isEmailDisabled = true;
          this.snackBar.open(PLEASE_CHECK_EMAIL, CLOSE, {
            duration: LONG_DURATION,
          });
          this.redirectAfterSignup();
        },
        error: err => {
          if (typeof err.error.message === 'string') {
            this.snackBar.open(err.error.message, null, {
              duration: LONG_DURATION,
            });
          } else {
            this.snackBar.open(err?.error?.message || err?.toString(), null, {
              duration: LONG_DURATION,
            });
          }
        },
      });
  }

  signUpViaPhone() {
    this.isNameAndPhoneDisabled = false;
    this.isSignUpViaEmail = !this.isSignUpViaEmail;
    this.isSignUpViaPhone = !this.isSignUpViaPhone;
  }

  onSubmitPhone() {
    this.isOTPSendButtonDisabled = true;
    setTimeout(() => (this.isOTPSendButtonDisabled = false), LONG_DURATION);
    this.authService.signUpViaPhone(this.name, this.phone).subscribe({
      next: success => {
        this.snackBar.open(OTP_SENT_TO + this.phone, null, {
          duration: LONG_DURATION,
        });
        this.isNameAndPhoneDisabled = true;
        this.redirectAfterSignup();
      },
      error: error => {
        this.snackBar.open(error?.error?.message || error?.toString(), null, {
          duration: LONG_DURATION,
        });
      },
    });
  }

  verifyPhoneSignup() {
    this.authService.verifySignupPhone(this.phone, this.otp).subscribe({
      next: success => {
        this.router.navigateByUrl('/account');
      },
      error: error => {
        this.snackBar.open(error?.error?.message || error?.toString(), null, {
          duration: LONG_DURATION,
        });
      },
    });
  }

  redirectAfterSignup() {
    timer(LONG_DURATION).subscribe(() => {
      if (this.postSignupRedirect) {
        window.location.href = this.postSignupRedirect;
      } else {
        this.router.navigateByUrl('/account');
      }
    });
  }

  back() {
    this.location.back();
  }

  disableSignup() {
    return this.isEmailDisabled || this.isSignUpDisabled;
  }
}
