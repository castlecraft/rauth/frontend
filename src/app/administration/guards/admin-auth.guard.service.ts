import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  RouterStateSnapshot,
} from '@angular/router';
import { Observable, catchError, map, of } from 'rxjs';
import { TokenService } from '../../auth/token/token.service';
import { ADMINISTRATOR } from '../../constants/app-constants';
import { IDTokenClaims } from '../../interfaces/id-token-claims.interfaces';

@Injectable({
  providedIn: 'root',
})
export class AdminAuthGuard implements CanActivate {
  constructor(private token: TokenService) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<boolean> {
    return this.token.loadProfile().pipe(
      map((profile: IDTokenClaims) => {
        if (profile?.roles?.includes(ADMINISTRATOR)) {
          return true;
        }
        return false;
      }),
      catchError(error => {
        return of(false);
      }),
    );
  }
}
