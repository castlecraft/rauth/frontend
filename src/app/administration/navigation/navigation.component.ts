import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';

import { AuthService } from '../../auth/auth.service';
import { SET_ITEM, StorageService } from '../../auth/storage/storage.service';
import { ACCESS_TOKEN, LOGGED_IN } from '../../auth/token/constants';
import { TokenService } from '../../auth/token/token.service';
import { BrandInfoService } from '../../common/brand-info/brand-info.service';
import { ADMINISTRATOR } from '../../constants/app-constants';
import { DEFAULT_THEME, THEME } from '../../constants/app-strings';
import { FORM, LIST, NEW_ID } from '../../constants/common';
import { APP_URL, COMMUNICATION, USER_UUID } from '../../constants/storage';
import { LOGOUT_URL } from '../../constants/url-paths';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss'],
})
export class NavigationComponent implements OnInit {
  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(map(result => result.matches));
  tokenIsValid: boolean;
  isAdmin: boolean = false;
  fullName: string = '';
  route: string;
  loggedIn: boolean;
  isCommunicationEnabled: boolean;
  list: string = '';
  allowedBack = '';
  currentRouteArray = [];
  logoURL: string = '';
  avatarName: string = '';
  profileBlock: boolean = false;
  currentTheme = '';
  constructor(
    private breakpointObserver: BreakpointObserver,
    private router: Router,
    private token: TokenService,
    private store: StorageService,
    private location: Location,
    private brandInfoService: BrandInfoService,
    private authservice: AuthService,
  ) {}

  ngOnInit(): void {
    this.list = LIST;
    this.allowedBack = FORM;
    this.getBrandInfo();
    this.getTheme();
    this.token.loadProfile().subscribe({
      next: (profile: any) => {
        this.isAdmin = profile?.roles.includes(ADMINISTRATOR);
        this.fullName = profile.full_name;
        if (profile.full_name) {
          this.avatarName =
            profile.full_name.split(' ')[0].charAt(0) +
            profile.full_name.split(' ')[1].charAt(0);
        }
      },
    });
    this.currentRouteArray = this.router.url.split('/');
    this.router.events
      .pipe(filter(route => route instanceof NavigationEnd))
      .subscribe({
        next: (route: NavigationEnd) => {
          this.route = route.url;
          this.currentRouteArray = this.route.split('/');
        },
      });
    if (!this.route) {
      this.route = this.router.url;
    }
    const loggedIn = this.store.getItem(LOGGED_IN);
    if (loggedIn === 'true') {
      this.tokenIsValid = true;
      this.loggedIn = true;
    }

    this.store.changes.subscribe({
      next: res => {
        if (res.event === SET_ITEM && res.value?.key === LOGGED_IN) {
          if (res.value?.value === 'true') {
            this.loggedIn = true;
            this.tokenIsValid = true;
          }
        }
      },
      error: error => {},
    });

    try {
      this.isCommunicationEnabled = JSON.parse(
        this.store.getItem(COMMUNICATION),
      );
    } catch (error) {
      this.isCommunicationEnabled = false;
    }
  }

  back() {
    this.location.back();
  }

  login() {
    this.token.logIn();
  }

  chooseAccount() {
    const appURL = this.store.getItem(APP_URL);
    window.open(appURL, '_blank', 'noreferrer=true');
  }

  logoutCurrentUser() {
    this.token.config.subscribe({
      next: config => {
        const userUUID = this.store.getItem(USER_UUID);
        const token = this.store.getItem(ACCESS_TOKEN);
        this.store.clear();
        this.authservice
          .revokeToken(config.issuerUrl, config.clientId, userUUID, token)
          .subscribe({
            next: res => {
              window.location.href =
                config.issuerUrl +
                LOGOUT_URL +
                '/' +
                userUUID +
                '?redirect=' +
                encodeURIComponent(location.href);
            },
            error: error => {},
          });
      },
    });
  }

  addModel() {
    // TODO: make it better in UI/UX
    const routeArray = this.route.split('/');
    routeArray.slice(-1);
    if (!routeArray.includes(NEW_ID) && routeArray.includes(FORM)) {
      routeArray.pop();
      routeArray.push(NEW_ID);
    }
    if (routeArray.includes(LIST)) {
      routeArray.push(NEW_ID);
    }
    routeArray.filter(n => n);
    const route = routeArray.map(component => {
      if (component === LIST) {
        return FORM;
      }
      return component;
    });
    this.router
      .navigateByUrl('admin', { skipLocationChange: true })
      .then(() => this.router.navigate(route));
  }

  getBrandInfo() {
    this.brandInfoService.retrieveBrandInfo().subscribe({
      next: brand => {
        this.logoURL = brand.logoUrl;
      },
    });
  }

  toggleAvatar() {
    this.profileBlock = !this.profileBlock;
  }

  getTheme(themeName?) {
    if (!themeName) {
      themeName = this.store.getItem(THEME);
    }
    for (const array of Array.from(document.body.classList)) {
      if (array.includes('theme')) {
        document.body.classList.remove(array);
      }
    }
    document.body.classList.add(themeName || DEFAULT_THEME);
    this.store.setItem(THEME, themeName || DEFAULT_THEME);
    this.currentTheme = themeName || DEFAULT_THEME;
  }

  isSelected(route) {
    return this.router.url.includes(route);
  }
}
