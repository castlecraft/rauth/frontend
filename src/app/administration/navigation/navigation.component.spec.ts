import { Component } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { EMPTY } from 'rxjs';

import { AuthServerMaterialModule } from '../../auth-server-material/auth-server-material.module';
import { AuthService } from '../../auth/auth.service';
import { TokenService } from '../../auth/token/token.service';
import { BrandInfoService } from '../../common/brand-info/brand-info.service';
import { NavigationComponent } from './navigation.component';
import { NavigationService } from './navigation.service';

describe('NavigationComponent', () => {
  let component: NavigationComponent;
  let fixture: ComponentFixture<NavigationComponent>;
  const navSvc: Partial<NavigationService> = {};
  const tokenSvc: Partial<TokenService> = {
    logIn() {},
    logOut() {},
    loadProfile() {
      return EMPTY;
    },
  };
  const authSvc: Partial<AuthService> = {};
  const brandInfoSvc: Partial<BrandInfoService> = {
    retrieveBrandInfo() {
      return EMPTY;
    },
  };

  @Component({ selector: 'app-home', template: '' })
  class HomeComponent {}

  beforeEach(fakeAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        BrowserAnimationsModule,
        AuthServerMaterialModule,
      ],
      declarations: [HomeComponent, NavigationComponent],
      providers: [
        { provide: NavigationService, useValue: navSvc },
        { provide: TokenService, useValue: tokenSvc },
        { provide: AuthService, useValue: authSvc },
        { provide: BrandInfoService, useValue: brandInfoSvc },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(NavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should compile', () => {
    expect(component).toBeDefined();
  });
});
