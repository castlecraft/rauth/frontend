import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { switchMap } from 'rxjs';
import { TokenService } from '../../auth/token/token.service';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class UpdatePhoneService {
  constructor(
    private readonly http: HttpClient,
    private readonly token: TokenService,
  ) {}

  addUnverifiedPhone(unverifiedPhone: string) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url = config.issuerUrl + environment.routes.ADD_UNVERIFIED_PHONE;
        return this.http.post(url, { unverifiedPhone }, { headers });
      }),
    );
  }

  verifyPhone(otp: string) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url = config.issuerUrl + environment.routes.VERIFY_PHONE;
        return this.http.post(url, { otp }, { headers });
      }),
    );
  }
}
