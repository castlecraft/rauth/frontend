import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { TokenService } from '../../auth/token/token.service';
import {
  CREATE_EMAIL_ENDPOINT,
  EMAIL_DELETE_ENDPOINT,
  EMAIL_UPDATE_ENDPOINT,
  FIND_ROLE_ENDPOINT,
  GET_EMAIL_ENDPOINT,
} from '../../constants/url-paths';

@Injectable({
  providedIn: 'root',
})
export class EmailService {
  constructor(
    private readonly http: HttpClient,
    private token: TokenService,
  ) {}

  createEmail(
    name: string,
    disabled: boolean = false,
    host: string,
    port: number,
    user: string,
    pass: string,
    from: string,
    secure: boolean,
    saveLog: boolean,
  ) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url = config.issuerUrl + CREATE_EMAIL_ENDPOINT;
        const emailData = {
          name,
          disabled,
          host,
          port,
          user,
          pass,
          from,
          secure,
          saveLog,
        };

        return this.http.post(url, emailData, {
          headers,
        });
      }),
    );
  }

  getEmail(uuid: string): Observable<any> {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url = config.issuerUrl + `${GET_EMAIL_ENDPOINT}/${uuid}`;
        return this.http.get<string>(url, { headers });
      }),
    );
  }

  getEmails() {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url = config.issuerUrl + FIND_ROLE_ENDPOINT;
        return this.http.get<string>(url, { headers });
      }),
    );
  }

  updateEmail(
    uuid: string,
    name: string,
    disabled: boolean = false,
    host: string,
    port: number,
    user: string,
    pass: string,
    from: string,
    secure: boolean,
    saveLog: boolean,
  ) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url = config.issuerUrl + EMAIL_UPDATE_ENDPOINT;
        const emailData = {
          uuid,
          name,
          disabled,
          host,
          port,
          user,
          pass,
          from,
          secure,
          saveLog,
        };
        return this.http.post(url, emailData, { headers });
      }),
    );
  }

  deleteEmailAccount(uuid: string) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url = config.issuerUrl + `${EMAIL_DELETE_ENDPOINT}/${uuid}`;
        return this.http.post(url, undefined, { headers });
      }),
    );
  }
}
