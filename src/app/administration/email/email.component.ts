import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { FormGroup, FormControl } from '@angular/forms';
import { NEW_ID, DURATION } from '../../constants/common';
import { EmailService } from './email.service';
import {
  CREATE_SUCCESSFUL,
  CLOSE,
  CREATE_ERROR,
  UPDATE_SUCCESSFUL,
  UPDATE_ERROR,
  DELETE_ERROR,
} from '../../constants/messages';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';
import { KeyEventService } from '../../common/key-event/key-event.service';
import { RouteMapping } from '../../constants/route-mapping';
// export const EMAIL_LIST_ROUTE = ['admin', 'list', 'email'];

@Component({
  selector: 'app-email',
  templateUrl: './email.component.html',
})
export class EmailComponent implements OnInit {
  uuid: string;
  name: string;
  isDisabled: boolean = false;
  host: string;
  port: number;
  user: string;
  pass: string;
  from: string;
  hide: boolean = true;
  secure: boolean;
  saveLog: boolean;

  emailForm = new FormGroup({
    name: new FormControl(),
    isDisabled: new FormControl(),
    host: new FormControl(),
    port: new FormControl(),
    user: new FormControl(),
    pass: new FormControl(),
    from: new FormControl(),
    secure: new FormControl(),
    saveLog: new FormControl(),
  });
  new = NEW_ID;
  routeKey: string = '';
  constructor(
    private readonly emailService: EmailService,
    route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private router: Router,
    private dialog: MatDialog,
    private keyEventService: KeyEventService,
  ) {
    this.uuid = route.snapshot.params.id;
  }

  ngOnInit() {
    this.keyEventService.registerRouteFunction(
      '/admin/form/email/:id',
      this.HitSave.bind(this),
    );
    if (this.uuid && this.uuid !== this.new) {
      this.emailService.getEmail(this.uuid).subscribe({
        next: response => {
          this.name = response.name;
          this.isDisabled = response.disabled;
          this.host = response.host;
          this.port = response.port;
          this.user = response.user;
          this.pass = response.pass;
          this.from = response.from;
          this.secure = response.secure;
          this.saveLog = response.saveLog;
          this.emailForm.controls.name.setValue(response.name);
          this.emailForm.controls.isDisabled.setValue(response.disabled);
          this.emailForm.controls.host.setValue(response.host);
          this.emailForm.controls.port.setValue(response.port);
          this.emailForm.controls.user.setValue(response.user);
          this.emailForm.controls.pass.setValue(response.pass);
          this.emailForm.controls.from.setValue(response.from);
          this.emailForm.controls.secure.setValue(response.secure);
          this.emailForm.controls.saveLog.setValue(response.saveLog);
        },
      });
    }
    this.routeKey =
      this.router.url.split('/')[this.router.url.split('/').length - 2];
  }

  HitSave() {
    if (this.host) {
      this.updateEmail();
    } else {
      this.createEmail();
    }
  }

  createEmail() {
    this.emailService
      .createEmail(
        this.emailForm.controls.name.value,
        this.emailForm.controls.isDisabled.value,
        this.emailForm.controls.host.value,
        this.emailForm.controls.port.value,
        this.emailForm.controls.user.value,
        this.emailForm.controls.pass.value,
        this.emailForm.controls.from.value,
        this.emailForm.controls.secure.value ? true : false,
        this.emailForm.controls.saveLog.value ? true : false,
      )
      .subscribe({
        next: success => {
          this.snackBar.open(CREATE_SUCCESSFUL, CLOSE, { duration: DURATION });
          this.router.navigate(RouteMapping[this.routeKey]);
        },
        error: error =>
          this.snackBar.open(CREATE_ERROR, CLOSE, { duration: DURATION }),
      });
  }

  updateEmail() {
    this.emailService
      .updateEmail(
        this.uuid,
        this.emailForm.controls.name.value,
        this.emailForm.controls.isDisabled.value,
        this.emailForm.controls.host.value,
        this.emailForm.controls.port.value,
        this.emailForm.controls.user.value,
        this.emailForm.controls.pass.value,
        this.emailForm.controls.from.value,
        this.emailForm.controls.secure.value ? true : false,
        this.emailForm.controls.saveLog.value ? true : false,
      )
      .subscribe({
        next: success => {
          this.snackBar.open(UPDATE_SUCCESSFUL, CLOSE, { duration: DURATION });
          this.router.navigate(RouteMapping[this.routeKey]);
        },
        error: error =>
          this.snackBar.open(UPDATE_ERROR, CLOSE, { duration: DURATION }),
      });
  }

  delete() {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.emailService.deleteEmailAccount(this.uuid).subscribe({
          next: res => {
            this.router.navigate(RouteMapping[this.routeKey]);
          },
          error: error => {
            this.snackBar.open(DELETE_ERROR, CLOSE, { duration: DURATION });
          },
        });
      }
    });
  }

  back() {
    this.router.navigate(RouteMapping[this.routeKey]);
  }
}
