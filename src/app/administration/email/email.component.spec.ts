import { waitForAsync, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailComponent } from './email.component';
import { RouterTestingModule } from '@angular/router/testing';
import { AuthServerMaterialModule } from '../../auth-server-material/auth-server-material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EmailService } from './email.service';
import { of } from 'rxjs';

describe('EmailComponent', () => {
  let component: EmailComponent;
  let fixture: ComponentFixture<EmailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        AuthServerMaterialModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
      ],
      declarations: [EmailComponent],
      providers: [
        {
          provide: EmailService,
          useValue: {
            getEmail: () => of(''),
          },
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
