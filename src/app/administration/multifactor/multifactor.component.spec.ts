import { waitForAsync, ComponentFixture, TestBed } from '@angular/core/testing';

import { MultifactorComponent } from './multifactor.component';
import { AuthServerMaterialModule } from '../../auth-server-material/auth-server-material.module';

import { FormsModule } from '@angular/forms';
import { ProfileService } from '../profile/profile.service';
import { of } from 'rxjs';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { provideHttpClientTesting } from '@angular/common/http/testing';
import { TokenService } from '../../auth/token/token.service';
import {
  provideHttpClient,
  withInterceptorsFromDi,
} from '@angular/common/http';

describe('MultifactorComponent', () => {
  let component: MultifactorComponent;
  let fixture: ComponentFixture<MultifactorComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [MultifactorComponent],
      imports: [
        AuthServerMaterialModule,
        FormsModule,
        RouterTestingModule,
        BrowserAnimationsModule,
      ],
      providers: [
        {
          provide: ProfileService,
          useValue: {
            getAuthServerUser() {
              return of({});
            },
          },
        },
        {
          provide: TokenService,
          useValue: {},
        },
        provideHttpClient(withInterceptorsFromDi()),
        provideHttpClientTesting(),
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultifactorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
