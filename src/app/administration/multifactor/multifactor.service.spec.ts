import { TestBed } from '@angular/core/testing';

import { MultifactorService } from './multifactor.service';
import { provideHttpClientTesting } from '@angular/common/http/testing';
import { TokenService } from '../../auth/token/token.service';
import {
  provideHttpClient,
  withInterceptorsFromDi,
} from '@angular/common/http';

describe('MultifactorService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [],
      providers: [
        {
          provide: TokenService,
          useValue: {},
        },
        provideHttpClient(withInterceptorsFromDi()),
        provideHttpClientTesting(),
      ],
    }),
  );

  it('should be created', () => {
    const service: MultifactorService = TestBed.get(MultifactorService);
    expect(service).toBeTruthy();
  });
});
