import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TokenService } from '../../auth/token/token.service';
import { switchMap } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class MultifactorService {
  constructor(
    private http: HttpClient,
    private token: TokenService,
  ) {}

  enable2fa() {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        return this.http.post(
          config.issuerUrl +
            environment.routes.INITIALIZE_2FA +
            '?restart=true',
          null,
          { headers },
        );
      }),
    );
  }

  verify2fa(otp: string) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        return this.http.post(
          config.issuerUrl + environment.routes.VERIFY_2FA,
          { otp },
          { headers },
        );
      }),
    );
  }

  disable2fa() {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        return this.http.post(
          config.issuerUrl + environment.routes.DISABLE_2FA,
          null,
          { headers },
        );
      }),
    );
  }
}
