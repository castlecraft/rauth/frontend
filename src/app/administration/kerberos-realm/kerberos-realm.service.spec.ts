import { TestBed } from '@angular/core/testing';

import { KerberosRealmService } from './kerberos-realm.service';
import { provideHttpClientTesting } from '@angular/common/http/testing';
import {
  provideHttpClient,
  withInterceptorsFromDi,
} from '@angular/common/http';

describe('KerberosRealmService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [],
      providers: [
        provideHttpClient(withInterceptorsFromDi()),
        provideHttpClientTesting(),
      ],
    }),
  );

  it('should be created', () => {
    const service: KerberosRealmService = TestBed.get(KerberosRealmService);
    expect(service).toBeTruthy();
  });
});
