import { Component, OnInit } from '@angular/core';
import { FormArray, FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { KerberosRealmService } from './kerberos-realm.service';
import { NEW_ID, DURATION } from '../../constants/common';
import {
  KERBEROS_REALM_ERROR,
  KERBEROS_REALM_UPDATED,
  KERBEROS_REALM_CREATED,
  CLOSE,
  UPDATE_ERROR,
  DELETE_ERROR,
} from '../../constants/messages';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';
import { KeyEventService } from '../../common/key-event/key-event.service';
import { RouteMapping } from '../../constants/route-mapping';
// export const KERBEROS_REALM_LIST_ROUTE = ['admin', 'list', 'kerberos_realm'];

@Component({
  selector: 'app-kerberos-realm',
  templateUrl: './kerberos-realm.component.html',
})
export class KerberosRealmComponent implements OnInit {
  uuid: string;
  name: string;
  isDisabled: boolean = false;
  hideAdminPassword: boolean = true;
  scopesForm = new FormArray([]);
  kerberosRealmForm = new FormGroup({
    name: new FormControl(),
    isDisabled: new FormControl(),
    description: new FormControl(),
    domain: new FormControl(),
    servicePrincipalName: new FormControl(),
    forceOTP: new FormControl(),
    ldapClient: new FormControl(),
  });
  new = NEW_ID;
  routeKey: string = '';
  clientList: any[];

  constructor(
    private realmService: KerberosRealmService,
    private route: ActivatedRoute,
    private router: Router,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private keyEventService: KeyEventService,
  ) {
    this.uuid = this.route.snapshot.params.id;
  }

  ngOnInit() {
    this.keyEventService.registerRouteFunction(
      '/admin/form/kerberos_realm/:id',
      this.HitSave.bind(this),
    );
    if (this.uuid && this.uuid !== this.new) {
      this.subscribeGetLDAPClients(this.uuid);
    } else if (this.uuid === NEW_ID) {
      this.uuid = undefined;
    }
    this.realmService.getClientList().subscribe({
      next: (response: any[]) => {
        this.clientList = response;
      },
      error: error => {},
    });
    this.routeKey =
      this.router.url.split('/')[this.router.url.split('/').length - 2];
  }

  HitSave() {
    if (this.uuid) {
      this.updateRealm();
    } else {
      this.createRealm();
    }
  }

  subscribeGetLDAPClients(uuid: string) {
    this.realmService.getRealm(uuid).subscribe({
      next: response => {
        if (response) {
          this.populateForm(response);
        }
      },
    });
  }

  populateForm(realm) {
    this.name = realm.name;
    this.isDisabled = realm.disabled;
    this.kerberosRealmForm.controls.name.setValue(realm.name);
    this.kerberosRealmForm.controls.isDisabled.setValue(realm.disabled);
    this.kerberosRealmForm.controls.description.setValue(realm.description);
    this.kerberosRealmForm.controls.ldapClient.setValue(realm.ldapClient);
    this.kerberosRealmForm.controls.servicePrincipalName.setValue(
      realm.servicePrincipalName,
    );
    this.kerberosRealmForm.controls.forceOTP.setValue(realm.forceOTP);
    this.kerberosRealmForm.controls.domain.setValue(realm.domain);
  }

  createRealm() {
    this.realmService
      .createRealm(
        this.kerberosRealmForm.controls.name.value,
        this.kerberosRealmForm.controls.isDisabled.value,
        this.kerberosRealmForm.controls.description.value,
        this.kerberosRealmForm.controls.domain.value,
        this.kerberosRealmForm.controls.servicePrincipalName.value,
        this.kerberosRealmForm.controls.forceOTP.value,
        this.kerberosRealmForm.controls.ldapClient.value,
      )
      .subscribe({
        next: success => {
          this.snackBar.open(KERBEROS_REALM_CREATED, CLOSE, {
            duration: DURATION,
          });
          this.router.navigate(RouteMapping[this.routeKey]);
        },
        error: error =>
          this.snackBar.open(KERBEROS_REALM_ERROR, CLOSE, {
            duration: DURATION,
          }),
      });
  }

  updateRealm() {
    this.realmService
      .updateRealm(
        this.uuid,
        this.kerberosRealmForm.controls.name.value,
        this.kerberosRealmForm.controls.isDisabled.value,
        this.kerberosRealmForm.controls.description.value,
        this.kerberosRealmForm.controls.domain.value,
        this.kerberosRealmForm.controls.servicePrincipalName.value,
        this.kerberosRealmForm.controls.forceOTP.value,
        this.kerberosRealmForm.controls.ldapClient.value,
      )
      .subscribe({
        next: success => {
          this.snackBar.open(KERBEROS_REALM_UPDATED, CLOSE, {
            duration: DURATION,
          });
          this.router.navigate(RouteMapping[this.routeKey]);
        },
        error: error =>
          this.snackBar.open(UPDATE_ERROR, CLOSE, { duration: DURATION }),
      });
  }

  delete() {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.realmService.deleteRealm(this.uuid).subscribe({
          next: res => {
            this.router.navigate(RouteMapping[this.routeKey]);
          },
          error: error => {
            this.snackBar.open(DELETE_ERROR, CLOSE, { duration: DURATION });
          },
        });
      }
    });
  }

  back() {
    this.router.navigate(RouteMapping[this.routeKey]);
  }
}
