import { waitForAsync, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { provideHttpClientTesting } from '@angular/common/http/testing';

import { KerberosRealmComponent } from './kerberos-realm.component';
import { AuthServerMaterialModule } from '../../auth-server-material/auth-server-material.module';
import { KerberosRealmService } from './kerberos-realm.service';
import { routes } from '../../app-routing.module';
import { of } from 'rxjs';
import {
  provideHttpClient,
  withInterceptorsFromDi,
} from '@angular/common/http';

describe('KerberosRealmComponent', () => {
  let component: KerberosRealmComponent;
  let fixture: ComponentFixture<KerberosRealmComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [KerberosRealmComponent],
      imports: [
        RouterModule.forRoot(routes),
        AuthServerMaterialModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
      ],
      providers: [
        {
          provide: KerberosRealmService,
          useValue: {
            getClientList: () => of([]),
          },
        },
        provideHttpClient(withInterceptorsFromDi()),
        provideHttpClientTesting(),
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KerberosRealmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
