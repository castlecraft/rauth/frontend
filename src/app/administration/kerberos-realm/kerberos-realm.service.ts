import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, switchMap } from 'rxjs/operators';
import { TokenService } from '../../auth/token/token.service';
import {
  CREATE_KERBEROS_REALM_ENDPOINT,
  DELETE_KERBEROS_REALM_ENDPOINT,
  GET_KERBEROS_REALM_ENDPOINT,
  LIST_LDAP_CLIENTS_ENDPOINT,
  UDPATE_KERBEROS_REALM_ENDPOINT,
} from '../../constants/url-paths';

@Injectable({
  providedIn: 'root',
})
export class KerberosRealmService {
  constructor(
    private readonly http: HttpClient,
    private readonly token: TokenService,
  ) {}

  getClientList() {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url = config.issuerUrl + `${LIST_LDAP_CLIENTS_ENDPOINT}`;
        return this.http.get<{
          docs: unknown[];
          length: number;
          offset: number;
        }>(url, { headers });
      }),
      map(data => data.docs),
    );
  }

  getRealm(uuid: string) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url = config.issuerUrl + `${GET_KERBEROS_REALM_ENDPOINT}/${uuid}`;
        return this.http.get<string>(url, { headers });
      }),
    );
  }

  createRealm(
    name: string,
    disabled: boolean = false,
    description: string,
    domain: string,
    servicePrincipalName: string,
    forceOTP: boolean = false,
    ldapClient: string,
  ) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const requestURL =
          config.issuerUrl + `${CREATE_KERBEROS_REALM_ENDPOINT}`;
        return this.http.post(
          requestURL,
          {
            name,
            disabled,
            description,
            domain,
            servicePrincipalName,
            forceOTP,
            ldapClient,
          },
          { headers },
        );
      }),
    );
  }

  updateRealm(
    uuid: string,
    name: string,
    disabled: boolean = false,
    description: string,
    domain: string,
    servicePrincipalName: string,
    forceOTP: boolean = false,
    ldapClient: string,
  ) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const requestURL =
          config.issuerUrl + `${UDPATE_KERBEROS_REALM_ENDPOINT}/${uuid}`;
        return this.http.post(
          requestURL,
          {
            name,
            disabled,
            description,
            domain,
            servicePrincipalName,
            forceOTP,
            ldapClient,
          },
          { headers },
        );
      }),
    );
  }

  deleteRealm(uuid: string) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url =
          config.issuerUrl + `${DELETE_KERBEROS_REALM_ENDPOINT}/${uuid}`;
        return this.http.post(url, undefined, { headers });
      }),
    );
  }
}
