import { Component, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ListingDataSource } from './listing-datasource';
import { ListingService } from './listing.service';
import { Router } from '@angular/router';
import { Observable, Subject, debounceTime, distinctUntilChanged } from 'rxjs';

@Component({
  selector: 'app-listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.css'],
})
export class ListingComponent {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  dataSource: ListingDataSource;

  displayedColumns = ['name', 'uuid', 'actions'];
  model: string;
  search: string = '';
  searchSubject = new Subject<string>();
  searchObservable$: Observable<string>;
  constructor(
    private listingService: ListingService,
    private router: Router,
  ) {
    this.searchObservable$ = this.searchSubject
      .asObservable()
      .pipe(debounceTime(400), distinctUntilChanged());
  }

  ngOnInit() {
    if (!this.model) {
      this.model = this.router.url.split('/').pop();
      this.loadDataSource();
    }
    this.searchObservable$.subscribe(searchItem => {
      this.setFilter();
    });
  }

  loadDataSource() {
    this.dataSource = new ListingDataSource(this.model, this.listingService);
    this.dataSource.loadItems();
  }

  getUpdate(event) {
    this.dataSource.loadItems(
      this.search,
      this.sort?.direction,
      event.pageIndex,
      event.pageSize,
    );
  }

  setFilter() {
    this.dataSource.loadItems(
      this.search,
      this.sort?.direction,
      this.paginator?.pageIndex,
      this.paginator?.pageSize,
    );
  }

  routeNew() {
    this.router.navigate(['admin', 'form', this.model, 'new']);
  }

  snakeToTitleCase(string: string) {
    if (!string) return;

    return string
      .split('_')
      .map(word => word.charAt(0).toUpperCase() + word.slice(1))
      .join(' ');
  }
}
