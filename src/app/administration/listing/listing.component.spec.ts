import { waitForAsync, ComponentFixture, TestBed } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ListingComponent } from './listing.component';
import { RouterTestingModule } from '@angular/router/testing';

import { of } from 'rxjs';
import { FormsModule } from '@angular/forms';
import { ListingService } from './listing.service';
import { AuthServerMaterialModule } from '../../auth-server-material/auth-server-material.module';

const listingService: Partial<ListingService> = {
  findModels: (...args) => of([]),
};

describe('ListingComponent', () => {
  let component: ListingComponent;
  let fixture: ComponentFixture<ListingComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ListingComponent],
      imports: [
        NoopAnimationsModule,
        AuthServerMaterialModule,
        RouterTestingModule,
        FormsModule,
      ],
      providers: [
        {
          provide: ListingService,
          useValue: listingService,
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should compile', () => {
    expect(component).toBeTruthy();
  });
});
