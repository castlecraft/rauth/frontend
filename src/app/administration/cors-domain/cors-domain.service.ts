import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { TokenService } from '../../auth/token/token.service';
import {
  ADD_CORS_DOMAIN_ENDPOINT,
  FIND_ROLE_ENDPOINT,
  GET_CORS_DOMAIN_ENDPOINT,
  REMOVE_CORS_DOMAIN_ENDPOINT,
  UPDATE_CORS_DOMAIN_ENDPOINT,
} from '../../constants/url-paths';

@Injectable({
  providedIn: 'root',
})
export class CORSDomainService {
  constructor(
    private readonly http: HttpClient,
    private token: TokenService,
  ) {}

  addDomain(domain: string) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url = config.issuerUrl + ADD_CORS_DOMAIN_ENDPOINT;
        const domainData = {
          domain,
        };

        return this.http.post(url, domainData, {
          headers,
        });
      }),
    );
  }

  getDomain(uuid: string): Observable<any> {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url = config.issuerUrl + `${GET_CORS_DOMAIN_ENDPOINT}/${uuid}`;
        return this.http.get<string>(url, { headers });
      }),
    );
  }

  getDomains() {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url = config.issuerUrl + FIND_ROLE_ENDPOINT;
        return this.http.get<string>(url, { headers });
      }),
    );
  }

  updateDomain(uuid: string, domain: string) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url = config.issuerUrl + UPDATE_CORS_DOMAIN_ENDPOINT + '/' + uuid;
        return this.http.post(url, { domain }, { headers });
      }),
    );
  }

  removeDomain(uuid: string) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url = config.issuerUrl + `${REMOVE_CORS_DOMAIN_ENDPOINT}/${uuid}`;
        return this.http.post(url, undefined, { headers });
      }),
    );
  }
}
