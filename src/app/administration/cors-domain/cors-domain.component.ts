import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { FormGroup, FormControl } from '@angular/forms';
import { NEW_ID, DURATION } from '../../constants/common';
import { CORSDomainService } from './cors-domain.service';
import {
  CREATE_SUCCESSFUL,
  CLOSE,
  CREATE_ERROR,
  UPDATE_SUCCESSFUL,
  UPDATE_ERROR,
  DELETE_ERROR,
} from '../../constants/messages';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';
import { KeyEventService } from '../../common/key-event/key-event.service';
import { RouteMapping } from '../../constants/route-mapping';

@Component({
  selector: 'app-cors-domain',
  templateUrl: './cors-domain.component.html',
})
export class CORSDomainComponent implements OnInit {
  uuid: string;
  domain: string;
  corsDomainForm = new FormGroup({
    domain: new FormControl(),
  });
  new = NEW_ID;
  routeKey: string = '';
  constructor(
    private readonly corsDomainService: CORSDomainService,
    route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private router: Router,
    private dialog: MatDialog,
    private keyEventService: KeyEventService,
  ) {
    this.uuid = route.snapshot.params.id;
  }

  ngOnInit() {
    this.keyEventService.registerRouteFunction(
      '/admin/form/cors_domain/:id',
      this.HitSave.bind(this),
    );
    if (this.uuid && this.uuid !== this.new) {
      this.corsDomainService.getDomain(this.uuid).subscribe({
        next: response => {
          this.domain = response.domain;
          this.corsDomainForm.controls.domain.setValue(response.domain);
        },
      });
    }
    this.routeKey =
      this.router.url.split('/')[this.router.url.split('/').length - 2];
  }

  HitSave() {
    if (this.domain) {
      this.updateDomain();
    } else {
      this.addDomain();
    }
  }

  addDomain() {
    this.corsDomainService
      .addDomain(this.corsDomainForm.controls.domain.value)
      .subscribe({
        next: success => {
          this.snackBar.open(CREATE_SUCCESSFUL, CLOSE, { duration: DURATION });
          this.router.navigate(RouteMapping[this.routeKey]);
        },
        error: error =>
          this.snackBar.open(CREATE_ERROR, CLOSE, { duration: DURATION }),
      });
  }

  updateDomain() {
    this.corsDomainService
      .updateDomain(this.uuid, this.corsDomainForm.controls.domain.value)
      .subscribe({
        next: success => {
          this.snackBar.open(UPDATE_SUCCESSFUL, CLOSE, { duration: DURATION });
          this.router.navigate(RouteMapping[this.routeKey]);
        },
        error: error =>
          this.snackBar.open(UPDATE_ERROR, CLOSE, { duration: DURATION }),
      });
  }

  removeDomain() {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.corsDomainService.removeDomain(this.uuid).subscribe({
          next: res => {
            this.router.navigate(RouteMapping[this.routeKey]);
          },
          error: error => {
            this.snackBar.open(DELETE_ERROR, CLOSE, { duration: DURATION });
          },
        });
      }
    });
  }

  back() {
    this.router.navigate(RouteMapping[this.routeKey]);
  }
}
