import { waitForAsync, ComponentFixture, TestBed } from '@angular/core/testing';

import { CORSDomainComponent } from './cors-domain.component';
import { RouterTestingModule } from '@angular/router/testing';
import { AuthServerMaterialModule } from '../../auth-server-material/auth-server-material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CORSDomainService } from './cors-domain.service';
import { of } from 'rxjs';

describe('CORSDomainComponent', () => {
  let component: CORSDomainComponent;
  let fixture: ComponentFixture<CORSDomainComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        AuthServerMaterialModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
      ],
      declarations: [CORSDomainComponent],
      providers: [
        {
          provide: CORSDomainService,
          useValue: {
            getEmail: () => of(''),
          },
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CORSDomainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
