import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup } from '@angular/forms';
import { KeyEventService } from '../../common/key-event/key-event.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SAMLAppService } from './saml-app.service';
import { DURATION, NEW_ID } from '../../constants/common';
import { RouteMapping } from '../../constants/route-mapping';
import {
  CLOSE,
  DELETE_ERROR,
  SAML_CREATED,
  SAML_ERROR,
  SAML_UPDATED,
  UPDATE_ERROR,
} from '../../constants/messages';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-saml-app',
  templateUrl: './saml-app.component.html',
})
export class SAMLAppComponent implements OnInit {
  name: string;
  uuid: string;
  claimTypesForm = new FormArray([]);
  hidePrivateKeyPass = true;
  new = NEW_ID;
  scopeList: any[];
  routeKey: string = '';

  samlForm = new FormGroup({
    name: new FormControl(),
    description: new FormControl(),
    cert: new FormControl(),
    key: new FormControl(),
    relayState: new FormControl(),
    scope: new FormControl(),
    nameIDAttribute: new FormControl(),
    spMetadataXml: new FormControl(),
    customLoginUrl: new FormControl(),
    claimTypes: this.claimTypesForm,
    encryptThenSign: new FormControl(),
    privateKeyPass: new FormControl(),
    isDisabled: new FormControl(),
  });

  constructor(
    private samlService: SAMLAppService,
    private route: ActivatedRoute,
    private router: Router,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private keyEventService: KeyEventService,
  ) {
    this.uuid = this.route.snapshot.params.id;
  }

  ngOnInit(): void {
    this.keyEventService.registerRouteFunction(
      '/admin/form/saml/:id',
      this.HitSave.bind(this),
    );
    if (this.uuid && this.uuid !== this.new) {
      this.subscribeGetSAML(this.uuid);
    } else if (this.uuid === NEW_ID) {
      this.uuid = undefined;
    }
    this.samlService.getScopes().subscribe({
      next: response => {
        this.scopeList = response;
      },
      error: error => {},
    });
    this.routeKey =
      this.router.url.split('/')[this.router.url.split('/').length - 2];
  }

  HitSave() {
    if (this.uuid) {
      this.updateSAML();
    } else {
      this.createSAML();
    }
  }

  subscribeGetSAML(uuid: string) {
    this.samlService.getSocialLogin(uuid).subscribe({
      next: response => {
        if (response) {
          this.populateForm(response);
        }
      },
    });
  }

  populateForm(saml) {
    this.name = saml.name;
    this.samlForm.controls.name.setValue(saml.name);
    this.samlForm.controls.description.setValue(saml.description);
    this.samlForm.controls.cert.setValue(saml.cert);
    this.samlForm.controls.key.setValue(saml.key);
    this.samlForm.controls.relayState.setValue(saml.relayState);
    this.samlForm.controls.scope.setValue(saml.scope);
    this.samlForm.controls.nameIDAttribute.setValue(saml.nameIDAttribute);
    this.samlForm.controls.customLoginUrl.setValue(saml.customLoginUrl);
    (saml?.claimTypes || []).forEach(claim => {
      this.addClaimTypes(
        claim.claimId,
        claim.claimDisplayName,
        claim.name,
        claim.valueTag,
        claim.nameFormat,
        claim.valueXsiType,
      );
    });
    this.samlForm.controls.encryptThenSign.setValue(saml.encryptThenSign);
    this.samlForm.controls.privateKeyPass.setValue(saml.privateKeyPass);
    this.samlForm.controls.isDisabled.setValue(saml.disable);
    this.samlForm.controls.spMetadataXml.setValue(saml.spMetadataXml);
  }

  createSAML() {
    this.samlService
      .createSAML(
        this.samlForm.controls.name.value,
        this.samlForm.controls.description.value,
        this.samlForm.controls.cert.value,
        this.samlForm.controls.key.value,
        this.samlForm.controls.relayState.value,
        this.samlForm.controls.scope.value,
        this.samlForm.controls.nameIDAttribute.value,
        this.samlForm.controls.customLoginUrl.value,
        this.getClaimsTypes(),
        this.samlForm.controls.encryptThenSign.value ? true : false,
        this.samlForm.controls.privateKeyPass.value
          ? this.samlForm.controls.privateKeyPass.value
          : undefined,
        this.samlForm.controls.isDisabled.value ? true : false,
        this.samlForm.controls.spMetadataXml.value,
      )
      .subscribe({
        next: success => {
          this.snackBar.open(SAML_CREATED, CLOSE, {
            duration: DURATION,
          });
          this.router.navigate(RouteMapping[this.routeKey]);
        },
        error: error =>
          this.snackBar.open(SAML_ERROR, CLOSE, { duration: DURATION }),
      });
  }

  updateSAML() {
    this.samlService
      .updateSAML(
        this.uuid,
        this.samlForm.controls.name.value,
        this.samlForm.controls.description.value,
        this.samlForm.controls.cert.value,
        this.samlForm.controls.key.value,
        this.samlForm.controls.relayState.value,
        this.samlForm.controls.scope.value,
        this.samlForm.controls.nameIDAttribute.value,
        this.samlForm.controls.customLoginUrl.value,
        this.getClaimsTypes(),
        this.samlForm.controls.encryptThenSign.value ? true : false,
        this.samlForm.controls.privateKeyPass.value
          ? this.samlForm.controls.privateKeyPass.value
          : undefined,
        this.samlForm.controls.isDisabled.value ? true : false,
        this.samlForm.controls.spMetadataXml.value,
      )
      .subscribe({
        next: success => {
          this.snackBar.open(SAML_UPDATED, CLOSE, {
            duration: DURATION,
          });
          this.router.navigate(RouteMapping[this.routeKey]);
        },
        error: error =>
          this.snackBar.open(UPDATE_ERROR, CLOSE, { duration: DURATION }),
      });
  }

  delete() {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.samlService.deleteSAML(this.uuid).subscribe({
          next: res => {
            this.router.navigate(RouteMapping[this.routeKey]);
          },
          error: err => {
            this.snackBar.open(DELETE_ERROR, CLOSE, { duration: DURATION });
          },
        });
      }
    });
  }

  getClaimsTypes(): {
    claimId: string;
    claimDisplayName: string;
    name: string;
    valueTag: string;
    nameFormat: string;
    valueXsiType: string;
  }[] {
    const claimTypes: {
      claimId: string;
      claimDisplayName: string;
      name: string;
      valueTag: string;
      nameFormat: string;
      valueXsiType: string;
    }[] = [];
    for (const control of this.claimTypesForm.controls) {
      claimTypes.push({
        claimId: control.value.claimId,
        claimDisplayName: control.value.claimDisplayName,
        name: control.value.name,
        valueTag: control.value.valueTag,
        nameFormat: control.value.nameFormat,
        valueXsiType: control.value.valueXsiType,
      });
    }
    return claimTypes;
  }

  addClaimTypes(
    claimId?: string,
    claimDisplayName?: string,
    name?: string,
    valueTag?: string,
    nameFormat?: string,
    valueXsiType?: string,
  ) {
    this.claimTypesForm.push(
      new FormGroup({
        claimId: new FormControl(claimId),
        claimDisplayName: new FormControl(claimDisplayName),
        name: new FormControl(name),
        valueTag: new FormControl(valueTag),
        nameFormat: new FormControl(nameFormat),
        valueXsiType: new FormControl(valueXsiType),
      }),
    );
  }

  removeClaimTypes(formGroupID: number) {
    this.claimTypesForm.removeAt(formGroupID);
  }

  back() {
    this.router.navigate(RouteMapping[this.routeKey]);
  }
}
