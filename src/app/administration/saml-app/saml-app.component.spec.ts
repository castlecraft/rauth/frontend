import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SAMLAppComponent } from './saml-app.component';
import { RouterModule } from '@angular/router';
import { EMPTY } from 'rxjs';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AuthServerMaterialModule } from '../../auth-server-material/auth-server-material.module';
import { SAMLAppService } from './saml-app.service';

describe('SAMLComponent', () => {
  let component: SAMLAppComponent;
  let fixture: ComponentFixture<SAMLAppComponent>;

  beforeEach(waitForAsync(() => {
    const mockSAMLSvc = jasmine.createSpyObj(['getScopes']);
    mockSAMLSvc.getScopes.and.returnValue(EMPTY);
    TestBed.configureTestingModule({
      imports: [
        AuthServerMaterialModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forRoot([]),
        BrowserAnimationsModule,
      ],
      declarations: [SAMLAppComponent],
      providers: [
        {
          provide: SAMLAppService,
          useValue: mockSAMLSvc,
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SAMLAppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
