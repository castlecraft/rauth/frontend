import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { switchMap } from 'rxjs/operators';
import { TokenService } from '../../auth/token/token.service';
import {
  CREATE_SAML_ENDPOINT,
  DELETE_SAML_ENDPOINT,
  FIND_SCOPE_ENDPOINT,
  GET_SAML_ENDPOINT,
  UDPATE_SAML_ENDPOINT,
} from '../../constants/url-paths';
import { forkJoin } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class SAMLAppService {
  constructor(
    private readonly http: HttpClient,
    private readonly token: TokenService,
  ) {}

  createSAML(
    name: string,
    description: string,
    cert: string,
    key: string,
    relayState: string,
    scope: string,
    nameIDAttribute: string,
    customLoginUrl: string,
    claimTypes: {
      claimId: string;
      claimDisplayName: string;
      name: string;
      valueTag: string;
      nameFormat: string;
      valueXsiType: string;
    }[] = [],
    encryptThenSign: boolean,
    privateKeyPass: string,
    disable: boolean,
    spMetadataXml: string,
  ) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url = config.issuerUrl + `${CREATE_SAML_ENDPOINT}`;
        return this.http.post(
          url,
          {
            name,
            description,
            cert,
            key,
            relayState,
            scope,
            nameIDAttribute,
            customLoginUrl,
            claimTypes,
            encryptThenSign,
            privateKeyPass,
            disable,
            spMetadataXml,
          },
          { headers },
        );
      }),
    );
  }

  updateSAML(
    uuid: string,
    name: string,
    description: string,
    cert: string,
    key: string,
    relayState: string,
    scope: string,
    nameIDAttribute: string,
    customLoginUrl: string,
    claimTypes: {
      claimId: string;
      claimDisplayName: string;
      name: string;
      valueTag: string;
      nameFormat: string;
      valueXsiType: string;
    }[],
    encryptThenSign: boolean,
    privateKeyPass: string,
    disable: boolean,
    spMetadataXml: string,
  ) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url = config.issuerUrl + `${UDPATE_SAML_ENDPOINT}/${uuid}`;
        return this.http.post(
          url,
          {
            name,
            description,
            cert,
            key,
            relayState,
            scope,
            nameIDAttribute,
            customLoginUrl,
            claimTypes,
            encryptThenSign,
            privateKeyPass,
            disable,
            spMetadataXml,
          },
          { headers },
        );
      }),
    );
  }

  deleteSAML(uuid: string) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url = config.issuerUrl + `${DELETE_SAML_ENDPOINT}/${uuid}`;
        return this.http.post(url, undefined, { headers });
      }),
    );
  }

  getSocialLogin(uuid: string) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url = config.issuerUrl + `${GET_SAML_ENDPOINT}/${uuid}`;
        return this.http.get<string>(url, { headers });
      }),
    );
  }

  getScopes() {
    return forkJoin({
      headers: this.token.getHeaders(),
      config: this.token.config,
    }).pipe(
      switchMap(({ headers, config }) => {
        const url = `${config.issuerUrl}${FIND_SCOPE_ENDPOINT}`;
        return this.http.get<any[]>(url, { headers });
      }),
    );
  }
}
