import { Component, OnInit } from '@angular/core';
import { FormArray, FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { SocialLoginService } from './social-login.service';
import { NEW_ID, DURATION } from '../../constants/common';
import {
  SOCIAL_LOGIN_ERROR,
  SOCIAL_LOGIN_UPDATED,
  SOCIAL_LOGIN_CREATED,
  CLOSE,
  UPDATE_ERROR,
  DELETE_ERROR,
} from '../../constants/messages';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';
import { KeyEventService } from '../../common/key-event/key-event.service';

// export const SOCIAL_LOGIN_LIST_ROUTE = ['admin', 'list', 'social_login'];
import { RouteMapping } from '../../constants/route-mapping';

@Component({
  selector: 'app-social-login',
  templateUrl: './social-login.component.html',
})
export class SocialLoginComponent implements OnInit {
  name: string;
  isDisabled: boolean = false;
  description: string;
  isUserCreationDisabled: boolean = false;
  uuid: string;
  clientId: string;
  clientSecret: string;
  authorizationURL: string;
  tokenURL: string;
  introspectionURL: string;
  baseURL: string;
  profileURL: string;
  revocationURL: string;
  emailAttribute: string;
  phoneAttribute: string;
  scope: string[];
  claimsScope: string;
  clientSecretToTokenEndpoint: boolean;
  hideClientSecret: boolean = true;
  redirectURL: string;
  routeKey: string = '';
  scopesForm = new FormArray([]);
  claimForm = new FormArray([]);

  socialLoginForm = new FormGroup({
    name: new FormControl(),
    isDisabled: new FormControl(),
    description: new FormControl(),
    isUserCreationDisabled: new FormControl(),
    clientId: new FormControl(),
    clientSecret: new FormControl(),
    authorizationURL: new FormControl(),
    tokenURL: new FormControl(),
    introspectionURL: new FormControl(),
    baseURL: new FormControl(),
    isTrusted: new FormControl(),
    profileURL: new FormControl(),
    revocationURL: new FormControl(),
    emailAttribute: new FormControl(),
    phoneAttribute: new FormControl(),
    clientSecretToTokenEndpoint: new FormControl(),
    scope: this.scopesForm,
    claimsScope: new FormControl(),
    redirectURL: new FormControl(),
    claimsMap: this.claimForm,
  });
  new = NEW_ID;
  claimsScopeList: any[];

  constructor(
    private socialLoginService: SocialLoginService,
    private route: ActivatedRoute,
    private router: Router,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private keyEventService: KeyEventService,
  ) {
    this.uuid = this.route.snapshot.params.id;
  }

  ngOnInit() {
    this.keyEventService.registerRouteFunction(
      '/admin/form/social_login/:id',
      this.HitSave.bind(this),
    );
    if (this.uuid && this.uuid !== this.new) {
      this.subscribeGetSocialLogin(this.uuid);
    } else if (this.uuid === NEW_ID) {
      this.uuid = undefined;
    }
    this.socialLoginService.getScopes().subscribe({
      next: response => {
        this.claimsScopeList = response;
      },
      error: error => {},
    });
    this.routeKey =
      this.router.url.split('/')[this.router.url.split('/').length - 2];
  }

  HitSave() {
    if (this.uuid) {
      this.updateSocialLogin();
    } else {
      this.createSocialLogin();
    }
  }

  subscribeGetSocialLogin(uuid: string) {
    this.socialLoginService.getSocialLogin(uuid).subscribe({
      next: response => {
        if (response) {
          this.populateForm(response);
        }
      },
    });
  }

  populateForm(socialLogin) {
    this.name = socialLogin.name;
    this.isDisabled = socialLogin.disabled;
    (socialLogin?.claimsMap || []).forEach(claim => {
      this.addClaim(claim.claim, claim.mapTo);
    });
    this.description = socialLogin.description;
    this.isUserCreationDisabled = socialLogin.disableUserCreation;
    this.clientId = socialLogin.clientId;
    this.clientSecret = socialLogin.clientSecret;
    this.authorizationURL = socialLogin.authorizationURL;
    this.tokenURL = socialLogin.tokenURL;
    this.introspectionURL = socialLogin.introspectionURL;
    this.baseURL = socialLogin.baseURL;
    this.profileURL = socialLogin.profileURL;
    this.revocationURL = socialLogin.revocationURL;
    this.emailAttribute = socialLogin.emailAttribute;
    this.phoneAttribute = socialLogin.phoneAttribute;
    this.scope = socialLogin.scope;
    this.claimsScope = socialLogin.claimsScope;
    this.clientSecretToTokenEndpoint = socialLogin.clientSecretToTokenEndpoint;
    this.socialLoginService
      .generateRedirectURL(socialLogin.uuid)
      .subscribe({ next: redirect => (this.redirectURL = redirect) });
    this.scope.forEach(scope => {
      this.addScope(scope);
    });
    this.socialLoginForm.controls.claimsScope.setValue(socialLogin.claimsScope);
    this.socialLoginForm.controls.name.setValue(socialLogin.name);
    this.socialLoginForm.controls.isDisabled.setValue(socialLogin.disabled);
    this.socialLoginForm.controls.description.setValue(socialLogin.description);
    this.socialLoginForm.controls.isUserCreationDisabled.setValue(
      socialLogin.disableUserCreation,
    );
    this.socialLoginForm.controls.clientId.setValue(socialLogin.clientId);
    this.socialLoginForm.controls.clientSecret.setValue(
      socialLogin.clientSecret,
    );
    this.socialLoginForm.controls.authorizationURL.setValue(
      socialLogin.authorizationURL,
    );
    this.socialLoginForm.controls.tokenURL.setValue(socialLogin.tokenURL);
    this.socialLoginForm.controls.introspectionURL.setValue(
      socialLogin.introspectionURL,
    );
    this.socialLoginForm.controls.baseURL.setValue(socialLogin.baseURL);
    this.socialLoginForm.controls.profileURL.setValue(socialLogin.profileURL);
    this.socialLoginForm.controls.revocationURL.setValue(
      socialLogin.revocationURL,
    );
    this.socialLoginForm.controls.emailAttribute.setValue(this.emailAttribute);
    this.socialLoginForm.controls.phoneAttribute.setValue(this.phoneAttribute);
    this.socialLoginForm.controls.clientSecretToTokenEndpoint.setValue(
      socialLogin.clientSecretToTokenEndpoint,
    );
    this.socialLoginForm.controls.redirectURL.setValue(this.redirectURL);
  }

  addScope(scope?: string) {
    this.scopesForm.push(new FormGroup({ scope: new FormControl(scope) }));
  }

  removeScope(formGroupID: number) {
    this.scopesForm.removeAt(formGroupID);
  }

  createSocialLogin() {
    this.socialLoginService
      .createSocialLogin(
        this.socialLoginForm.controls.name.value,
        this.socialLoginForm.controls.isDisabled.value,
        this.socialLoginForm.controls.description.value,
        this.socialLoginForm.controls.isUserCreationDisabled.value,
        this.socialLoginForm.controls.clientId.value,
        this.socialLoginForm.controls.clientSecret.value,
        this.socialLoginForm.controls.authorizationURL.value,
        this.socialLoginForm.controls.tokenURL.value,
        this.socialLoginForm.controls.introspectionURL.value,
        this.socialLoginForm.controls.baseURL.value,
        this.socialLoginForm.controls.profileURL.value,
        this.socialLoginForm.controls.revocationURL.value,
        this.socialLoginForm.controls.emailAttribute.value,
        this.socialLoginForm.controls.phoneAttribute.value,
        this.getScopes(),
        this.socialLoginForm.controls.claimsScope.value,
        this.socialLoginForm.controls.clientSecretToTokenEndpoint.value,
        this.getClaimsMap(),
      )
      .subscribe({
        next: success => {
          this.snackBar.open(SOCIAL_LOGIN_CREATED, CLOSE, {
            duration: DURATION,
          });
          this.router.navigate(RouteMapping[this.routeKey]);
        },
        error: error =>
          this.snackBar.open(SOCIAL_LOGIN_ERROR, CLOSE, { duration: DURATION }),
      });
  }

  getScopes(): string[] {
    const scopesFormGroup = this.socialLoginForm.get('scope') as FormArray;
    const scopes: string[] = [];
    for (const control of scopesFormGroup.controls) {
      scopes.push(control.value.scope);
    }
    return scopes;
  }

  updateSocialLogin() {
    this.socialLoginService
      .updateSocialLogin(
        this.uuid,
        this.socialLoginForm.controls.name.value,
        this.socialLoginForm.controls.isDisabled.value,
        this.socialLoginForm.controls.description.value,
        this.socialLoginForm.controls.isUserCreationDisabled.value,
        this.socialLoginForm.controls.clientId.value,
        this.socialLoginForm.controls.clientSecret.value,
        this.socialLoginForm.controls.authorizationURL.value,
        this.socialLoginForm.controls.tokenURL.value,
        this.socialLoginForm.controls.introspectionURL.value,
        this.socialLoginForm.controls.baseURL.value,
        this.socialLoginForm.controls.profileURL.value,
        this.socialLoginForm.controls.revocationURL.value,
        this.socialLoginForm.controls.emailAttribute.value,
        this.socialLoginForm.controls.phoneAttribute.value,
        this.getScopes(),
        this.socialLoginForm.controls.claimsScope.value,
        this.socialLoginForm.controls.clientSecretToTokenEndpoint.value,
        this.getClaimsMap(),
      )
      .subscribe({
        next: success => {
          this.snackBar.open(SOCIAL_LOGIN_UPDATED, CLOSE, {
            duration: DURATION,
          });
          this.router.navigate(RouteMapping[this.routeKey]);
        },
        error: error =>
          this.snackBar.open(UPDATE_ERROR, CLOSE, { duration: DURATION }),
      });
  }

  delete() {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.socialLoginService.deleteSocialLogin(this.uuid).subscribe({
          next: res => {
            this.router.navigate(RouteMapping[this.routeKey]);
          },
          error: err => {
            this.snackBar.open(DELETE_ERROR, CLOSE, { duration: DURATION });
          },
        });
      }
    });
  }

  addClaim(claim?: string, mapTo?: string) {
    this.claimForm.push(
      new FormGroup({
        claim: new FormControl(claim),
        mapTo: new FormControl(mapTo),
      }),
    );
  }

  removeClaim(formGroupID: number) {
    this.claimForm.removeAt(formGroupID);
  }

  getClaimsMap(): { claim: string; mapTo: string }[] {
    const claimsMap: { claim: string; mapTo: string }[] = [];
    for (const control of this.claimForm.controls) {
      claimsMap.push({
        claim: control.value.claim,
        mapTo: control.value.mapTo,
      });
    }
    return claimsMap;
  }

  back() {
    this.router.navigate(RouteMapping[this.routeKey]);
  }
}
