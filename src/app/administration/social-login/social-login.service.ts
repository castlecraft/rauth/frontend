import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, switchMap } from 'rxjs/operators';
import { TokenService } from '../../auth/token/token.service';
import {
  CREATE_SOCIAL_LOGIN_ENDPOINT,
  DELETE_SOCIAL_LOGIN_ENDPOINT,
  FIND_SCOPE_ENDPOINT,
  GET_SOCIAL_LOGIN_ENDPOINT,
  SOCIAL_LOGIN_CALLBACK_ENDPOINT,
  UPDATE_SOCIAL_LOGIN_ENDPOINT,
} from '../../constants/url-paths';
import { forkJoin } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class SocialLoginService {
  constructor(
    private readonly http: HttpClient,
    private readonly token: TokenService,
  ) {}

  getSocialLogin(uuid: string) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url = config.issuerUrl + `${GET_SOCIAL_LOGIN_ENDPOINT}/${uuid}`;
        return this.http.get<string>(url, { headers });
      }),
    );
  }

  createSocialLogin(
    name: string,
    disabled: boolean = false,
    description: string,
    disableUserCreation: boolean = false,
    clientId: string,
    clientSecret: string,
    authorizationURL: string,
    tokenURL: string,
    introspectionURL: string,
    baseURL: string,
    profileURL: string,
    revocationURL: string,
    emailAttribute: string,
    phoneAttribute: string,
    scope: string[],
    claimsScope: string,
    clientSecretToTokenEndpoint: boolean,
    claimsMap: { claim: string; mapTo: string }[],
  ) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url = config.issuerUrl + `${CREATE_SOCIAL_LOGIN_ENDPOINT}`;
        return this.http.post(
          url,
          {
            name,
            disabled,
            description,
            disableUserCreation,
            clientId,
            clientSecret,
            authorizationURL,
            tokenURL,
            introspectionURL,
            baseURL,
            profileURL,
            revocationURL,
            emailAttribute,
            phoneAttribute,
            scope,
            claimsScope,
            clientSecretToTokenEndpoint,
            claimsMap,
          },
          { headers },
        );
      }),
    );
  }

  updateSocialLogin(
    uuid: string,
    name: string,
    disabled: boolean = false,
    description: string,
    disableUserCreation: boolean = false,
    clientId: string,
    clientSecret: string,
    authorizationURL: string,
    tokenURL: string,
    introspectionURL: string,
    baseURL: string,
    profileURL: string,
    revocationURL: string,
    emailAttribute: string,
    phoneAttribute: string,
    scope: string[],
    claimsScope: string,
    clientSecretToTokenEndpoint: boolean,
    claimsMap: { claim: string; mapTo: string }[],
  ) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url =
          config.issuerUrl + `${UPDATE_SOCIAL_LOGIN_ENDPOINT}/${uuid}`;
        return this.http.post(
          url,
          {
            name,
            disabled,
            description,
            disableUserCreation,
            clientId,
            clientSecret,
            authorizationURL,
            tokenURL,
            introspectionURL,
            baseURL,
            profileURL,
            revocationURL,
            emailAttribute,
            phoneAttribute,
            scope,
            claimsScope,
            clientSecretToTokenEndpoint,
            claimsMap,
          },
          { headers },
        );
      }),
    );
  }

  generateRedirectURL(uuid: string) {
    return this.token.config.pipe(
      map(
        config =>
          config.issuerUrl + SOCIAL_LOGIN_CALLBACK_ENDPOINT + '/' + uuid,
      ),
    );
  }

  deleteSocialLogin(uuid: string) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url =
          config.issuerUrl + `${DELETE_SOCIAL_LOGIN_ENDPOINT}/${uuid}`;
        return this.http.post(url, undefined, { headers });
      }),
    );
  }

  getScopes() {
    return forkJoin({
      headers: this.token.getHeaders(),
      config: this.token.config,
    }).pipe(
      switchMap(({ headers, config }) => {
        const url = `${config.issuerUrl}${FIND_SCOPE_ENDPOINT}`;
        return this.http.get<any[]>(url, { headers });
      }),
    );
  }
}
