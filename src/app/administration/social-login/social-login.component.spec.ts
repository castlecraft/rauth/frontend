import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { EMPTY } from 'rxjs';
import { AuthServerMaterialModule } from '../../auth-server-material/auth-server-material.module';
import { SocialLoginComponent } from './social-login.component';
import { SocialLoginService } from './social-login.service';

describe('SocialLoginComponent', () => {
  let component: SocialLoginComponent;
  let fixture: ComponentFixture<SocialLoginComponent>;

  beforeEach(waitForAsync(() => {
    const mockSocialLoginSvc = jasmine.createSpyObj(['getScopes']);
    mockSocialLoginSvc.getScopes.and.returnValue(EMPTY);
    TestBed.configureTestingModule({
      imports: [
        AuthServerMaterialModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forRoot([]),
        BrowserAnimationsModule,
      ],
      declarations: [SocialLoginComponent],
      providers: [
        {
          provide: SocialLoginService,
          useValue: mockSocialLoginSvc,
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SocialLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
