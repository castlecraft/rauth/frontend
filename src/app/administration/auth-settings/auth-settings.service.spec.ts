import { TestBed } from '@angular/core/testing';
import { provideHttpClientTesting } from '@angular/common/http/testing';

import { AuthSettingsService } from './auth-settings.service';
import { StorageService } from '../../auth/storage/storage.service';
import { TokenService } from '../../auth/token/token.service';
import { of } from 'rxjs';
import {
  provideHttpClient,
  withInterceptorsFromDi,
} from '@angular/common/http';

describe('AuthSettingsService', () => {
  const token: Partial<TokenService> = {
    getToken: () => of('access_token'),
    logOut() {},
  };
  const store: Partial<StorageService> = {
    getItem: () => 'item',
  };
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [],
      providers: [
        { provide: TokenService, useValue: token },
        { provide: StorageService, useValue: store },
        provideHttpClient(withInterceptorsFromDi()),
        provideHttpClientTesting(),
      ],
    }),
  );

  it('should be created', () => {
    const service: AuthSettingsService = TestBed.get(AuthSettingsService);
    expect(service).toBeTruthy();
  });
});
