import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { KeyEventService } from '../../common/key-event/key-event.service';
import { DURATION, THIRTY, UNDO_DURATION } from '../../constants/common';
import {
  CLOSE,
  DELETING,
  UNDO,
  UPDATE_ERROR,
  UPDATE_SUCCESSFUL,
} from '../../constants/messages';
import { IAuthSettings } from './auth-settings.interface';
import { AuthSettingsService } from './auth-settings.service';

@Component({
  selector: 'app-auth-settings',
  templateUrl: './auth-settings.component.html',
  styleUrls: ['./auth-settings.component.css'],
})
export class AuthSettingsComponent implements OnInit {
  issuerUrl: string;
  identityProviderClientId: string;
  infrastructureConsoleClientId: string;
  communicationServerClientId: string;
  clientList: any[];
  storageList: any[];
  appURL: string;
  clientId: string;
  clientSecret: string;
  avatarBucket: string;
  systemEmailAccount: string;
  emailAccounts: any[];
  cloudStorageList: { uuid: string; name: string }[] = [];
  disableSignup: boolean;
  flagDeleteUserSessions: boolean = false;
  flagDeleteBearerTokens: boolean = false;
  disableDeleteSessions: boolean = false;
  disableDeleteTokens: boolean = false;
  enableChoosingAccount: boolean;
  enableCustomLogin: boolean;
  refreshTokenExpiresInHours: number = THIRTY;
  authCodeExpiresInMinutes: number = THIRTY;
  organizationName: string;
  enableUserPhone: boolean;
  isUserDeleteDisabled: boolean;
  passwordChangeRestricted: boolean;
  enableUserOTP: boolean;
  otpExpiry: number;
  regenerateOTP: boolean;
  allowedFailedLoginAttempts: number;
  scopes: any[] = [];

  authSettingsForm = new FormGroup({
    issuerUrl: new FormControl(),
    disableSignup: new FormControl(),
    infrastructureConsoleClientId: new FormControl(),
    systemEmailAccount: new FormControl(),
    enableChoosingAccount: new FormControl(),
    refreshTokenExpiresInHours: new FormControl(),
    authCodeExpiresInMinutes: new FormControl(),
    organizationName: new FormControl(),
    enableUserPhone: new FormControl(),
    enableCustomLogin: new FormControl(),
    isUserDeleteDisabled: new FormControl(),
    passwordChangeRestricted: new FormControl(),
    enableUserOTP: new FormControl(),
    logoURL: new FormControl(),
    faviconURL: new FormControl(),
    privacyURL: new FormControl(),
    helpURL: new FormControl(),
    termsURL: new FormControl(),
    copyrightMessage: new FormControl(),
    avatarBucket: new FormControl(),
    otpExpiry: new FormControl(),
    regenerateOTP: new FormControl(),
    allowedFailedLoginAttempts: new FormControl(),
    forgotPasswordMessage: new FormControl(),
    forgotPasswordSubject: new FormControl(),
    completeSignupMessage: new FormControl(),
    completeSignupSubject: new FormControl(),
    verifyExistingEmailMessage: new FormControl(),
    verifyExistingEmailSubject: new FormControl(),
    verifyNewEmailMessage: new FormControl(),
    verifyNewEmailSubject: new FormControl(),
    loginOtpMessage: new FormControl(),
    loginOtpSubject: new FormControl(),
    passwordExpiryMessage: new FormControl(),
    passwordExpirySubject: new FormControl(),
    scopes: new FormControl(),
    emailDelayInSeconds: new FormControl(),
    passwordResetIntervalDays: new FormControl(),
    resendOTPDelayInSec: new FormControl(),
  });

  constructor(
    private settingsService: AuthSettingsService,
    private snackBar: MatSnackBar,
    private keyEventService: KeyEventService,
  ) {}

  ngOnInit() {
    this.keyEventService.registerRouteFunction(
      '/admin/auth_settings',
      this.HitSave.bind(this),
    );
    this.settingsService.getSettings().subscribe({
      next: (response: IAuthSettings) => {
        this.issuerUrl = response.issuerUrl;
        this.communicationServerClientId = response.communicationServerClientId;
        this.disableSignup = response.disableSignup;
        this.enableChoosingAccount = response.enableChoosingAccount;
        this.refreshTokenExpiresInHours = response.refreshTokenExpiresInHours;
        this.authCodeExpiresInMinutes = response.authCodeExpiresInMinutes;
        this.enableUserPhone = response.enableUserPhone;
        this.isUserDeleteDisabled = response.isUserDeleteDisabled;
        this.passwordChangeRestricted = response.passwordChangeRestricted;
        this.enableUserOTP = response.enableUserOTP;
        this.systemEmailAccount = response.systemEmailAccount;
        this.avatarBucket = response.avatarBucket;
        this.enableCustomLogin = response.enableCustomLogin;
        this.otpExpiry = response.otpExpiry;
        this.regenerateOTP = response.regenerateOTP;
        this.infrastructureConsoleClientId =
          response.infrastructureConsoleClientId;
        this.allowedFailedLoginAttempts = response.allowedFailedLoginAttempts;
        this.populateForm(response);
      },
      error: error => {},
    });

    this.settingsService.getClientList().subscribe({
      next: (response: any[]) => {
        this.clientList = response;
      },
      error: error => {},
    });

    this.settingsService.getStorageList().subscribe({
      next: (response: any[]) => {
        this.storageList = response;
      },
      error: error => {},
    });

    this.settingsService.getEmailAccounts().subscribe({
      next: (response: any[]) => {
        this.emailAccounts = response;
      },
      error: error => {},
    });

    this.settingsService.getScopes().subscribe({
      next: (response: any) => {
        if (response) {
          response.map(scope => {
            this.scopes.push(scope.name);
          });
        }
      },
    });
  }

  HitSave() {
    if (this.authSettingsForm.valid) {
      this.updateAuthSettings();
    }
  }

  populateForm(response: IAuthSettings) {
    this.authSettingsForm.controls.issuerUrl.setValue(response.issuerUrl);
    this.authSettingsForm.controls.disableSignup.setValue(
      response.disableSignup,
    );
    this.authSettingsForm.controls.infrastructureConsoleClientId.setValue(
      response.infrastructureConsoleClientId,
    );
    this.authSettingsForm.controls.enableChoosingAccount.setValue(
      response.enableChoosingAccount,
    );
    this.authSettingsForm.controls.refreshTokenExpiresInHours.setValue(
      response.refreshTokenExpiresInHours,
    );
    this.authSettingsForm.controls.authCodeExpiresInMinutes.setValue(
      response.authCodeExpiresInMinutes,
    );
    this.authSettingsForm.controls.organizationName.setValue(
      response.organizationName,
    );
    this.authSettingsForm.controls.enableUserPhone.setValue(
      response.enableUserPhone,
    );
    this.authSettingsForm.controls.enableCustomLogin.setValue(
      response.enableCustomLogin,
    );
    this.authSettingsForm.controls.isUserDeleteDisabled.setValue(
      response.isUserDeleteDisabled,
    );
    this.authSettingsForm.controls.passwordChangeRestricted.setValue(
      response.passwordChangeRestricted,
    );
    this.authSettingsForm.controls.enableUserOTP.setValue(
      response.enableUserOTP,
    );
    this.authSettingsForm.controls.avatarBucket.setValue(response.avatarBucket);
    this.authSettingsForm.controls.logoURL.setValue(response.logoUrl);
    this.authSettingsForm.controls.termsURL.setValue(response.termsUrl);
    this.authSettingsForm.controls.privacyURL.setValue(response.privacyUrl);
    this.authSettingsForm.controls.helpURL.setValue(response.helpUrl);
    this.authSettingsForm.controls.faviconURL.setValue(response.faviconUrl);
    this.authSettingsForm.controls.copyrightMessage.setValue(
      response.copyrightMessage,
    );
    this.authSettingsForm.controls.systemEmailAccount.setValue(
      response.systemEmailAccount,
    );
    this.authSettingsForm.controls.organizationName.disable();
    this.authSettingsForm.controls.otpExpiry.setValue(response.otpExpiry);
    this.authSettingsForm.controls.regenerateOTP.setValue(
      response.regenerateOTP,
    );
    this.authSettingsForm.controls.allowedFailedLoginAttempts.setValue(
      response.allowedFailedLoginAttempts,
    );
    this.authSettingsForm.controls.forgotPasswordMessage.setValue(
      response.forgotPasswordMessage,
    );
    this.authSettingsForm.controls.forgotPasswordSubject.setValue(
      response.forgotPasswordSubject,
    );
    this.authSettingsForm.controls.completeSignupMessage.setValue(
      response.completeSignupMessage,
    );
    this.authSettingsForm.controls.completeSignupSubject.setValue(
      response.completeSignupSubject,
    );
    this.authSettingsForm.controls.verifyExistingEmailMessage.setValue(
      response.verifyExistingEmailMessage,
    );
    this.authSettingsForm.controls.verifyExistingEmailSubject.setValue(
      response.verifyExistingEmailSubject,
    );
    this.authSettingsForm.controls.verifyNewEmailMessage.setValue(
      response.verifyNewEmailMessage,
    );
    this.authSettingsForm.controls.verifyNewEmailSubject.setValue(
      response.verifyNewEmailSubject,
    );
    this.authSettingsForm.controls.loginOtpMessage.setValue(
      response.loginOtpMessage,
    );
    this.authSettingsForm.controls.loginOtpSubject.setValue(
      response.loginOtpSubject,
    );
    this.authSettingsForm.controls.passwordExpiryMessage.setValue(
      response.passwordExpiryMessage,
    );
    this.authSettingsForm.controls.passwordExpirySubject.setValue(
      response.passwordExpirySubject,
    );
    this.authSettingsForm.controls.emailDelayInSeconds.setValue(
      response.emailDelayInSeconds,
    );
    this.authSettingsForm.controls.passwordResetIntervalDays.setValue(
      response.passwordResetIntervalDays,
    );
    this.authSettingsForm.controls.resendOTPDelayInSec.setValue(
      response.resendOTPDelayInSec,
    );
    this.authSettingsForm.controls.scopes.setValue(response.allowedScopes);
  }

  updateAuthSettings() {
    const logoURL =
      this.authSettingsForm.controls.logoURL.value === ''
        ? null
        : this.authSettingsForm.controls.logoURL.value;
    const faviconURL =
      this.authSettingsForm.controls.faviconURL.value === ''
        ? null
        : this.authSettingsForm.controls.faviconURL.value;
    const privacyURL =
      this.authSettingsForm.controls.privacyURL.value === ''
        ? null
        : this.authSettingsForm.controls.privacyURL.value;
    const helpURL =
      this.authSettingsForm.controls.helpURL.value === ''
        ? null
        : this.authSettingsForm.controls.helpURL.value;
    const termsURL =
      this.authSettingsForm.controls.termsURL.value === ''
        ? null
        : this.authSettingsForm.controls.termsURL.value;
    this.settingsService
      .update(
        this.authSettingsForm.controls.issuerUrl.value,
        this.authSettingsForm.controls.disableSignup.value,
        this.authSettingsForm.controls.infrastructureConsoleClientId.value,
        this.authSettingsForm.controls.enableChoosingAccount.value,
        this.authSettingsForm.controls.refreshTokenExpiresInHours.value,
        this.authSettingsForm.controls.authCodeExpiresInMinutes.value,
        this.authSettingsForm.controls.organizationName.value,
        this.authSettingsForm.controls.enableUserPhone.value,
        this.authSettingsForm.controls.enableCustomLogin.value,
        this.authSettingsForm.controls.isUserDeleteDisabled.value,
        this.authSettingsForm.controls.passwordChangeRestricted.value,
        this.authSettingsForm.controls.enableUserOTP.value,
        logoURL,
        faviconURL,
        privacyURL,
        helpURL,
        termsURL,
        this.authSettingsForm.controls.copyrightMessage.value,
        this.authSettingsForm.controls.avatarBucket.value,
        this.authSettingsForm.controls.systemEmailAccount.value,
        Number(this.authSettingsForm.controls.otpExpiry.value || 5),
        this.authSettingsForm.controls.regenerateOTP.value,
        Number(
          this.authSettingsForm.controls.allowedFailedLoginAttempts.value || 0,
        ),
        this.authSettingsForm.controls.forgotPasswordMessage.value,
        this.authSettingsForm.controls.forgotPasswordSubject.value,
        this.authSettingsForm.controls.completeSignupMessage.value,
        this.authSettingsForm.controls.completeSignupSubject.value,
        this.authSettingsForm.controls.verifyExistingEmailMessage.value,
        this.authSettingsForm.controls.verifyExistingEmailSubject.value,
        this.authSettingsForm.controls.verifyNewEmailMessage.value,
        this.authSettingsForm.controls.verifyNewEmailSubject.value,
        this.authSettingsForm.controls.loginOtpMessage.value,
        this.authSettingsForm.controls.loginOtpSubject.value,
        this.authSettingsForm.controls.passwordExpiryMessage.value,
        this.authSettingsForm.controls.passwordExpirySubject.value,
        this.authSettingsForm.controls.scopes.value || [],
        this.authSettingsForm.controls.emailDelayInSeconds.value || 60,
        this.authSettingsForm.controls.passwordResetIntervalDays.value || 0,
        this.authSettingsForm.controls.resendOTPDelayInSec.value || 0,
      )
      .subscribe({
        next: response => {
          this.snackBar.open(UPDATE_SUCCESSFUL, CLOSE, { duration: DURATION });
        },
        error: error => {
          this.snackBar.open(UPDATE_ERROR, CLOSE, { duration: DURATION });
        },
      });
  }

  deleteUserSessions() {
    this.flagDeleteUserSessions = true;
    this.disableDeleteSessions = true;
    this.disableDeleteTokens = true;
    const snackBar = this.snackBar.open(DELETING, UNDO, {
      duration: UNDO_DURATION,
    });

    snackBar.afterDismissed().subscribe({
      next: dismissed => {
        if (this.flagDeleteUserSessions) {
          this.settingsService.deleteUserSessions().subscribe({
            next: deleted => {
              this.logout();
            },
            error: error => {},
          });
        }
      },
      error: error => {},
    });

    snackBar.onAction().subscribe({
      next: success => {
        this.flagDeleteUserSessions = false;
        this.disableDeleteSessions = false;
        this.disableDeleteTokens = false;
      },
      error: error => {},
    });
  }

  deleteBearerTokens() {
    this.flagDeleteUserSessions = true;
    this.disableDeleteSessions = true;
    this.disableDeleteTokens = true;
    const snackBar = this.snackBar.open(DELETING, UNDO, {
      duration: UNDO_DURATION,
    });

    snackBar.afterDismissed().subscribe({
      next: dismissed => {
        if (this.flagDeleteUserSessions) {
          this.settingsService.deleteBearerTokens().subscribe({
            next: deleted => {
              this.logout();
            },
            error: error => {},
          });
        }
      },
      error: error => {},
    });

    snackBar.onAction().subscribe({
      next: success => {
        this.flagDeleteUserSessions = false;
        this.disableDeleteSessions = false;
        this.disableDeleteTokens = false;
      },
      error: error => {},
    });
  }

  toggleOrgName() {
    if (this.authSettingsForm.controls.organizationName.disabled) {
      this.authSettingsForm.controls.organizationName.enable();
    }
  }

  logout() {
    this.settingsService.logout();
  }

  kebabToTitleCase(string: string) {
    return string
      .split('-')
      .map(word => word.charAt(0).toUpperCase() + word.slice(1))
      .join(' ');
  }
}
