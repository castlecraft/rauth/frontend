import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { switchMap } from 'rxjs';

import { TokenService } from '../../../auth/token/token.service';
import { RETRIEVE_USER_CLAIMS_ENDPOINT } from '../../../constants/url-paths';

@Injectable({
  providedIn: 'root',
})
export class ClaimsListingService {
  constructor(
    private token: TokenService,
    private http: HttpClient,
  ) {}

  findClaims(
    uuid: string,
    filter = '',
    sortOrder = 'asc',
    pageNumber = 0,
    pageSize = 10,
  ) {
    return this.token.config.pipe(
      switchMap(config => {
        const baseUrl = config.issuerUrl;
        const url = `${baseUrl}${RETRIEVE_USER_CLAIMS_ENDPOINT}?uuid=${uuid}`;
        const params = new HttpParams()
          .set('limit', pageSize.toString())
          .set('offset', (pageNumber * pageSize).toString())
          .set('search', filter)
          .set('sort', sortOrder);

        return this.token.getHeaders().pipe(
          switchMap(headers => {
            return this.http.get(url, {
              params,
              headers,
            });
          }),
        );
      }),
    );
  }
}
