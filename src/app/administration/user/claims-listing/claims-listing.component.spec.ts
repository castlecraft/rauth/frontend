import { waitForAsync, ComponentFixture, TestBed } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { of } from 'rxjs';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule } from '@angular/forms';
import { ClaimsListingComponent } from './claims-listing.component';
import { ClaimsListingService } from './claims-listing.service';
import { AuthServerMaterialModule } from '../../../auth-server-material/auth-server-material.module';

const listingService: Partial<ClaimsListingService> = {
  findClaims: (...args) => of([]),
};

describe('ClaimsListingComponent', () => {
  let component: ClaimsListingComponent;
  let fixture: ComponentFixture<ClaimsListingComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ClaimsListingComponent],
      imports: [
        NoopAnimationsModule,
        AuthServerMaterialModule,
        RouterTestingModule,
        FormsModule,
      ],
      providers: [
        {
          provide: ClaimsListingService,
          useValue: listingService,
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClaimsListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should compile', () => {
    expect(component).toBeTruthy();
  });
});
