import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppsComponent } from './apps/apps.component';
import { MultifactorComponent } from './multifactor/multifactor.component';
import { ProfileComponent } from './profile/profile.component';
import { UpdateEmailComponent } from './update-email/update-email.component';
import { UpdatePhoneComponent } from './update-phone/update-phone.component';
import { ProfileService } from './profile/profile.service';
import { MultifactorService } from './multifactor/multifactor.service';
import { UpdatePhoneService } from './update-phone/update-phone.service';
import { AuthServerMaterialModule } from '../auth-server-material/auth-server-material.module';
import { AuthSettingsComponent } from './auth-settings/auth-settings.component';
import { ClientComponent } from './client/client.component';
import { KerberosRealmComponent } from './kerberos-realm/kerberos-realm.component';
import { LDAPClientComponent } from './ldap-client/ldap-client.component';
import { RoleComponent } from './role/role.component';
import { ScopeComponent } from './scope/scope.component';
import { UserComponent } from './user/user.component';
import { SocialLoginComponent } from './social-login/social-login.component';
import { OAuth2ProviderComponent } from './oauth2-provider/oauth2-provider.component';
import { EmailComponent } from './email/email.component';
import { CloudStorageComponent } from './cloud-storage/cloud-storage.component';
import { NavigationComponent } from './navigation/navigation.component';
import { RouterModule } from '@angular/router';
import { NavigationService } from './navigation/navigation.service';
import { AuthSettingsService } from './auth-settings/auth-settings.service';
import { ClientService } from './client/client.service';
import { KerberosRealmService } from './kerberos-realm/kerberos-realm.service';
import { LDAPClientService } from './ldap-client/ldap-client.service';
import { RoleService } from './role/role.service';
import { ScopeService } from './scope/scope.service';
import { UserService } from './user/user.service';
import { SocialLoginService } from './social-login/social-login.service';
import { OAuth2ProviderService } from './oauth2-provider/oauth2-provider.service';
import { EmailService } from './email/email.service';
import { CloudStorageService } from './cloud-storage/cloud-storage.service';
import { ListingComponent } from './listing/listing.component';
import { ClaimsListingComponent } from './user/claims-listing/claims-listing.component';
import { ClaimsListingService } from './user/claims-listing/claims-listing.service';
import { CORSDomainComponent } from './cors-domain/cors-domain.component';
import { CORSDomainService } from './cors-domain/cors-domain.service';
import { SAMLAppComponent } from './saml-app/saml-app.component';
import { SAMLAppService } from './saml-app/saml-app.service';

@NgModule({
  declarations: [
    ProfileComponent,
    AppsComponent,
    MultifactorComponent,
    UpdatePhoneComponent,
    UpdateEmailComponent,
    AuthSettingsComponent,
    ClientComponent,
    KerberosRealmComponent,
    LDAPClientComponent,
    RoleComponent,
    ScopeComponent,
    UserComponent,
    SocialLoginComponent,
    OAuth2ProviderComponent,
    EmailComponent,
    CloudStorageComponent,
    NavigationComponent,
    ListingComponent,
    ClaimsListingComponent,
    CORSDomainComponent,
    SAMLAppComponent,
  ],
  imports: [
    RouterModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AuthServerMaterialModule,
  ],
  providers: [
    MultifactorService,
    UpdatePhoneService,
    ProfileService,
    NavigationService,
    AuthSettingsService,
    ClientService,
    KerberosRealmService,
    LDAPClientService,
    RoleService,
    ScopeService,
    UserService,
    SocialLoginService,
    OAuth2ProviderService,
    EmailService,
    CloudStorageService,
    ClaimsListingService,
    CORSDomainService,
    SAMLAppService,
  ],
})
export class AdministrationModule {}
