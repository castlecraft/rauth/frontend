import {
  Component,
  ElementRef,
  EventEmitter,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Observable, forkJoin } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { SET_ITEM, StorageService } from '../../auth/storage/storage.service';
import { LOGGED_IN } from '../../auth/token/constants';
import { TokenService } from '../../auth/token/token.service';
import {
  ADMINISTRATOR,
  DURATION,
  UNDO_DURATION,
} from '../../constants/app-constants';
import { LOCALES } from '../../constants/locale';
import {
  AVATAR_UPDATED,
  AVATAR_UPDATED_FAILED,
  CLOSE,
  DELETE_FAILED,
  DELETING,
  FEMALE_CONST,
  MALE_CONST,
  NO_AVATAR_SET,
  OTHER_CONST,
  OTP_DISABLED,
  OTP_ENABLED,
  PASSWORD_LESS_LOGIN_DISABLED,
  PASSWORD_LESS_LOGIN_ENABLED,
  PLEASE_CHECK_EMAIL_TO_VERIFY_ACCOUNT,
  TOKEN_REVOKE_ERROR,
  TOKEN_REVOKED,
  UNDO,
  UPDATE_FAILED,
  UPDATE_SUCCESSFUL,
} from '../../constants/messages';
import { USER_UUID } from '../../constants/storage';
import { TIME_ZONES } from '../../constants/timezones';
import { LOGOUT_URL } from '../../constants/url-paths';
import { UserResponse } from '../../interfaces/user-response.interface';
import { UpdateEmailComponent } from '../update-email/update-email.component';
import { IDTokenClaims } from './id-token-claims.interfaces';
import { ProfileService } from './profile.service';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
})
export class ProfileComponent implements OnInit {
  genders: string[] = [MALE_CONST, FEMALE_CONST, OTHER_CONST];
  timezones = TIME_ZONES;
  locales = LOCALES;
  missingAvatarImage = environment.routes.MISSING_AVATAR_IMAGE;

  uuid: string;
  selectedFile: File;
  fullName: string;
  givenName: string;
  middleName: string;
  familyName: string;
  nickName: string;
  gender: string;
  birthdate: Date;
  picture: string;
  website: string;
  zoneinfo: string;
  locale: string;
  roles: string[] = [];
  groups: string[] = [];
  apps: any[] = [];
  avatarUrl: string = NO_AVATAR_SET;
  checked2fa: boolean = true;
  showPasswordSection: boolean = false;
  currentPassword: string;
  newPassword: string;
  repeatPassword: string;
  hideAvatar: boolean = false;
  flagDeleteUser: boolean = false;
  isPasswordSet: boolean;
  sentForgotPasswordEmail: boolean = false;
  email: string;
  phone: string;
  enablePasswordLess: boolean;
  enableUserPhone: boolean;
  isEmailVerified: Observable<boolean> = this.profileService
    .getOIDCProfile()
    .pipe(map(res => res.email_verified));
  isVerifyEmailDisabled: boolean;
  enableOTP: boolean;

  personalForm = new FormGroup({
    fullName: new FormControl(),
    email: new FormControl(),
    phone: new FormControl(),
    givenName: new FormControl(),
    middleName: new FormControl(),
    familyName: new FormControl(),
    nickname: new FormControl(),
    gender: new FormControl(),
    birthdate: new FormControl(),
    picture: new FormControl(),
    website: new FormControl(),
    zoneinfo: new FormControl(),
    locale: new FormControl(),
  });

  changePasswordForm = new FormGroup({
    currentPassword: new FormControl(),
    newPassword: new FormControl(),
    repeatPassword: new FormControl(),
  });

  @Output() messageEvent = new EventEmitter<string>();

  @ViewChild('fileInput', { static: true }) fileInputRef: ElementRef;

  // accordions
  isExpanded = [true, false, false, false, false];

  constructor(
    private readonly token: TokenService,
    private readonly store: StorageService,
    private readonly profileService: ProfileService,
    private readonly snackBar: MatSnackBar,
    private readonly router: Router,
    private readonly dialog: MatDialog,
  ) {}

  ngOnInit() {
    forkJoin({
      config: this.token.config,
      token: this.token.getToken(),
    }).subscribe({
      next: ({ config, token }) => {
        this.missingAvatarImage =
          config.issuerUrl + environment.routes.MISSING_AVATAR_IMAGE;
        if (!token && config.session) {
          this.token.logIn();
        }
      },
      error: error => {
        if (error.InvalidExpiration || error.InvalidToken) {
          this.token.logIn();
        }
      },
    });
    this.subscribeStoreChanges();
    this.personalForm.controls.birthdate.disable();
    this.loadProfile();
    this.loadExternalApps();
  }

  loadProfile() {
    this.token.loadProfile().subscribe({
      next: (profile: IDTokenClaims) => {
        this.roles = profile?.roles;
        this.subscribeGetUser();
        this.checkServerForPhoneRegistration();
      },
      error: error => {},
    });
  }

  loadExternalApps() {
    this.profileService.getConnectedApps().subscribe({
      next: (apps: any) => {
        this.apps = apps;
      },
      error: error => {},
    });
  }

  revokeExternalAppToken(clientId: string) {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.profileService.removeExternalApplication(clientId).subscribe({
          next: success => {
            this.loadExternalApps();
            this.snackBar.open(TOKEN_REVOKED, CLOSE, { duration: DURATION });
          },
          error: error => {
            this.snackBar.open(TOKEN_REVOKE_ERROR, CLOSE, {
              duration: DURATION,
            });
          },
        });
      }
    });
  }

  subscribeGetUser() {
    this.profileService
      .getAuthServerUser()
      .pipe(
        map(project => {
          this.store.setItem(USER_UUID, (project as UserResponse).uuid);
          this.subscribeGetProfile();
          return project;
        }),
      )
      .subscribe({
        next: (response: UserResponse) => {
          this.personalForm.controls.fullName.setValue(response.name);
          this.personalForm.controls.email.setValue(response.email);
          this.personalForm.controls.phone.setValue(response.phone);
          this.checked2fa = response.enable2fa;
          this.uuid = response.uuid;
          this.isPasswordSet = response.isPasswordSet;
          this.enablePasswordLess = response.enablePasswordLess;
          this.phone = response.phone;
          this.enableOTP = response.enableOTP;
        },
        error: error => {},
      });
  }

  enablePasswordLessLogin() {
    this.profileService.enablePasswordLess().subscribe({
      next: success => {
        this.enablePasswordLess = true;
        this.snackBar.open(PASSWORD_LESS_LOGIN_ENABLED, CLOSE, {
          duration: DURATION,
        });
      },
      error: ({ error }) => {
        this.snackBar.open(error.message, CLOSE, {
          duration: DURATION,
        });
      },
    });
  }

  disablePasswordLess() {
    this.profileService.disablePasswordLess().subscribe({
      next: success => {
        this.enablePasswordLess = false;
        this.snackBar.open(PASSWORD_LESS_LOGIN_DISABLED, CLOSE, {
          duration: DURATION,
        });
      },
      error: error => {},
    });
  }

  enableProfileOTP() {
    this.profileService.enableProfileOTP().subscribe({
      next: success => {
        this.enableOTP = true;
        this.snackBar.open(OTP_ENABLED, CLOSE, {
          duration: DURATION,
        });
      },
      error: error => {},
    });
  }

  disableProfileOTP() {
    this.profileService.disableProfileOTP().subscribe({
      next: success => {
        this.enableOTP = false;
        this.snackBar.open(OTP_DISABLED, CLOSE, {
          duration: DURATION,
        });
      },
      error: error => {},
    });
  }

  subscribeGetProfile() {
    this.profileService.getOIDCProfile().subscribe({
      next: response => {
        if (response) {
          this.personalForm.controls.familyName.setValue(response.family_name);
          this.personalForm.controls.birthdate.setValue(response.birthdate);
          this.personalForm.controls.gender.setValue(response.gender);
          this.personalForm.controls.givenName.setValue(response.given_name);
          this.personalForm.controls.middleName.setValue(response.middle_name);
          this.personalForm.controls.nickname.setValue(response.nickname);
          if (response.picture) {
            this.picture = response.picture;
          } else {
            this.picture = environment.routes.MISSING_AVATAR_IMAGE;
          }
          this.messageEvent.emit(this.picture);
          this.personalForm.controls.website.setValue(response.website);
          this.personalForm.controls.zoneinfo.setValue(response.zoneinfo);
          this.personalForm.controls.locale.setValue(response.locale);
        }
      },
    });
  }

  onFileChanged() {
    this.selectedFile = this.fileInputRef.nativeElement.files[0];
    if (this.selectedFile) {
      const reader = new FileReader();
      this.profileService.uploadAvatar(this.selectedFile).subscribe({
        next: (profile: any) => {
          reader.readAsDataURL(this.fileInputRef.nativeElement.files[0]);
          reader.onload = (file: any) => {
            this.picture = file.target.result;
            this.messageEvent.emit(this.picture);
          };
          this.hideAvatar = false;
          this.snackBar.open(AVATAR_UPDATED, CLOSE, { duration: DURATION });
        },
        error: err => {
          this.snackBar.open(AVATAR_UPDATED_FAILED, CLOSE, {
            duration: DURATION,
          });
        },
      });
    }
  }

  updateOpenIDClaims() {
    this.profileService
      .updateOpenIDClaims(
        this.personalForm.controls.givenName.value || null,
        this.personalForm.controls.middleName.value || null,
        this.personalForm.controls.familyName.value || null,
        this.personalForm.controls.nickname.value || null,
        this.personalForm.controls.gender.value || null,
        this.personalForm.controls.birthdate.value || null,
        this.personalForm.controls.website.value || null,
        this.personalForm.controls.zoneinfo.value || null,
        this.personalForm.controls.locale.value || null,
      )
      .pipe(
        switchMap(res => {
          return this.profileService.setAuthServerUser({
            name: this.personalForm.controls.fullName.value,
          });
        }),
      )
      .subscribe({
        next: response => {
          this.snackBar.open(UPDATE_SUCCESSFUL, CLOSE, {
            duration: DURATION,
          });
        },
        error: error => {
          this.snackBar.open(UPDATE_FAILED, CLOSE, {
            duration: DURATION,
          });
        },
      });
  }

  togglePanel(event: MouseEvent, index_data): void {
    // Prevent the header click event from firing
    if (event) event.stopPropagation();
    this.isExpanded.forEach((a, index) => {
      if (index === index_data) {
        this.isExpanded[index] = !this.isExpanded[index];
      } else {
        this.isExpanded[index] = false;
      }
    });
  }

  toggleFileField() {
    this.hideAvatar = !this.hideAvatar;
  }

  enableDisable2fa() {
    this.router.navigate(['admin', 'profile', 'mfa']);
  }

  updatePhone() {
    this.router.navigate(['admin', 'profile', 'update_phone']);
  }

  updateBirthdate(type: string, event: MatDatepickerInputEvent<Date>) {
    this.birthdate = event.value;
  }

  showChangePassword() {
    this.togglePanel(null, 1);
    this.showPasswordSection = true;
  }

  changePasswordRequest() {
    this.profileService
      .changePassword(
        this.changePasswordForm.controls.currentPassword.value,
        this.changePasswordForm.controls.newPassword.value,
        this.changePasswordForm.controls.repeatPassword.value,
      )
      .subscribe({
        next: data => this.logout(),
        error: err => {
          let message = err.error.message;
          if (Array.isArray(err.error.message)) {
            message = err.error.message[0];
          }
          this.snackBar.open(message, CLOSE, { duration: DURATION });
        },
      });
  }

  deleteAvatar() {
    this.profileService.deleteAvatar().subscribe({
      next: response => {
        this.picture = undefined;
        this.messageEvent.emit(this.picture);
        this.hideAvatar = false;
      },
      error: error => {},
    });
  }

  deleteUser() {
    this.flagDeleteUser = true;
    const snackBar = this.snackBar.open(DELETING, UNDO, {
      duration: UNDO_DURATION,
    });

    snackBar.afterDismissed().subscribe({
      next: dismissed => {
        if (this.flagDeleteUser) {
          this.profileService.deleteUser().subscribe({
            next: deleted => {
              this.token.logOut();
              this.logout();
            },
            error: error => {
              this.snackBar.open(error?.error?.message || DELETE_FAILED, UNDO, {
                duration: UNDO_DURATION,
              });
            },
          });
        }
      },
      error: error => {},
    });

    snackBar.onAction().subscribe({
      next: success => {
        this.flagDeleteUser = false;
      },
      error: error => {},
    });
  }

  logout() {
    this.token.config.subscribe({
      next: config => {
        this.store.clear();
        window.location.href =
          config.issuerUrl +
          LOGOUT_URL +
          '?redirect=' +
          encodeURIComponent(location.href);
      },
    });
  }

  setPassword() {
    this.togglePanel(null, 1);
    this.profileService.setPassword().subscribe({
      next: response => {
        this.sentForgotPasswordEmail = true;
      },
      error: err => {
        this.snackBar.open(err?.error?.message, CLOSE, { duration: DURATION });
      },
    });
  }

  checkIfAdmin() {
    return this.roles.includes(ADMINISTRATOR);
  }

  manageKeys() {
    this.router.navigate(['admin', 'profile', 'keys', this.uuid]);
  }

  checkServerForPhoneRegistration() {
    this.profileService.checkServerForPhoneRegistration().subscribe({
      next: response => (this.enableUserPhone = response.enableUserPhone),
      error: error => (this.enableUserPhone = false),
    });
  }

  emailVerificationCode() {
    this.isVerifyEmailDisabled = true;
    this.profileService.emailVerificationCode().subscribe({
      next: res => {
        setTimeout(() => (this.isVerifyEmailDisabled = false), UNDO_DURATION);
        this.snackBar.open(PLEASE_CHECK_EMAIL_TO_VERIFY_ACCOUNT, CLOSE, {
          duration: UNDO_DURATION,
        });
      },
    });
  }

  updateEmail() {
    const dialogRef = this.dialog.open(UpdateEmailComponent);
    dialogRef.afterClosed().subscribe({
      next: email => {
        if (email) {
          return this.profileService.updateEmail(email).subscribe({
            next: success => {
              this.snackBar.open(PLEASE_CHECK_EMAIL_TO_VERIFY_ACCOUNT, CLOSE, {
                duration: UNDO_DURATION,
              });
            },
            error: error => {
              this.snackBar.open(error?.error?.message, CLOSE, {
                duration: UNDO_DURATION,
              });
            },
          });
        }
      },
      error: error => {
        this.snackBar.open(error?.error?.message, CLOSE, {
          duration: UNDO_DURATION,
        });
      },
    });
  }

  subscribeStoreChanges() {
    this.store.changes.subscribe({
      next: res => {
        if (res.event === SET_ITEM && res.value?.key === LOGGED_IN) {
          if (res.value?.value === 'true') {
            this.loadProfile();
          }
        }
      },
      error: error => {},
    });

    const loggedIn = this.store.getItem(LOGGED_IN);
    if (loggedIn === 'true') {
      this.loadProfile();
    }
  }
}
