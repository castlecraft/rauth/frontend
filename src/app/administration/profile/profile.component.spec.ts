import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSnackBar } from '@angular/material/snack-bar';
import { RouterTestingModule } from '@angular/router/testing';
import { Component } from '@angular/core';
import { from, of } from 'rxjs';
import { ProfileComponent } from './profile.component';
import { AuthServerMaterialModule } from '../../auth-server-material/auth-server-material.module';

import { ProfileService } from './profile.service';
import { IDTokenClaims } from './id-token-claims.interfaces';
import { TokenService } from '../../auth/token/token.service';

@Component({ selector: 'app-password-requirement', template: '' })
class PasswordRequirementComponent {}

describe('ProfileComponent', () => {
  let component: ProfileComponent;
  let fixture: ComponentFixture<ProfileComponent>;
  const tokenServiceStub: Partial<TokenService> = {
    loadProfile() {
      return of({});
    },
    getToken() {
      return of('token');
    },
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        AuthServerMaterialModule,
        ReactiveFormsModule,
        FormsModule,
        BrowserAnimationsModule,
        RouterTestingModule,
      ],
      providers: [
        {
          provide: TokenService,
          useValue: tokenServiceStub,
        },
        {
          provide: ProfileService,
          useValue: {
            getPersonalDetails() {
              return from([]);
            },
            getAuthServerUser() {
              return from([]);
            },
            getProfileDetails() {
              return from([]);
            },
            checkServerForPhoneRegistration() {
              return from([]);
            },
            getOIDCProfile() {
              return of({} as IDTokenClaims);
            },
            getConnectedApps() {
              return from([]);
            },
          } as Partial<ProfileService>,
        },
        MatSnackBar,
      ],
      declarations: [ProfileComponent, PasswordRequirementComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileComponent);
    component = fixture.componentInstance;
    component.ngOnInit();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
