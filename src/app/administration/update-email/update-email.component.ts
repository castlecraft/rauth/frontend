import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-update-email',
  templateUrl: './update-email.component.html',
})
export class UpdateEmailComponent {
  updateEmailForm = new FormGroup({
    email: new FormControl('', Validators.email),
  });

  constructor() {}
}
