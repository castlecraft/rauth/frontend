import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ScopeService } from './scope.service';
import { NEW_ID, DURATION } from '../../constants/common';
import {
  CREATE_SUCCESSFUL,
  CLOSE,
  CREATE_ERROR,
  UPDATE_SUCCESSFUL,
  UPDATE_ERROR,
  DELETE_ERROR,
} from '../../constants/messages';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';
import { KeyEventService } from '../../common/key-event/key-event.service';
import { RouteMapping } from '../../constants/route-mapping';
// export const SCOPE_LIST_ROUTE = ['admin', 'list', 'scope'];

@Component({
  selector: 'app-scope',
  templateUrl: './scope.component.html',
})
export class ScopeComponent implements OnInit {
  uuid: string;
  name: string;
  description: string;
  new = NEW_ID;
  routeKey: string = '';
  scopeForm: FormGroup = new FormGroup({
    name: new FormControl(),
    uuid: new FormControl(),
    description: new FormControl(),
  });

  constructor(
    private readonly scopeService: ScopeService,
    private route: ActivatedRoute,
    private router: Router,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private keyEventService: KeyEventService,
  ) {
    this.uuid = this.route.snapshot.params.id;
  }

  ngOnInit() {
    this.keyEventService.registerRouteFunction(
      '/admin/form/scope/:id',
      this.HitSave.bind(this),
    );
    if (this.uuid && this.uuid !== NEW_ID) {
      this.subscribeGetScope(this.uuid);
    } else if (this.uuid === NEW_ID) {
      this.uuid = undefined;
    }
    this.routeKey =
      this.router.url.split('/')[this.router.url.split('/').length - 2];
  }

  HitSave() {
    if (this.uuid) {
      this.updateScope();
    } else {
      this.createScope();
    }
  }

  subscribeGetScope(uuid: string) {
    this.scopeService.getScope(uuid).subscribe({
      next: response => {
        if (response) {
          this.populateScopeForm(response);
        }
      },
    });
  }

  createScope() {
    this.scopeService
      .createScope(
        this.scopeForm.controls.name.value,
        this.scopeForm.controls.description.value,
      )
      .subscribe({
        next: success => {
          this.snackBar.open(CREATE_SUCCESSFUL, CLOSE, { duration: DURATION });
          this.router.navigate(RouteMapping[this.routeKey]);
        },
        error: error =>
          this.snackBar.open(CREATE_ERROR, CLOSE, { duration: DURATION }),
      });
  }

  updateScope() {
    this.scopeService
      .updateScope(
        this.uuid,
        this.scopeForm.controls.name.value,
        this.scopeForm.controls.description.value,
      )
      .subscribe({
        next: success => {
          this.snackBar.open(UPDATE_SUCCESSFUL, CLOSE, { duration: DURATION });
          this.router.navigate(RouteMapping[this.routeKey]);
        },
        error: error =>
          this.snackBar.open(UPDATE_ERROR, CLOSE, { duration: DURATION }),
      });
  }

  populateScopeForm(scope) {
    this.uuid = scope.uuid;
    this.name = scope.name;
    this.scopeForm.controls.name.setValue(scope.name);
    this.scopeForm.controls.description.setValue(scope.description);
  }

  delete() {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.scopeService.deleteScope(this.name).subscribe(
          next => {
            this.router.navigate(RouteMapping[this.routeKey]);
          },
          error => {
            this.snackBar.open(DELETE_ERROR, CLOSE, { duration: DURATION });
          },
        );
      }
    });
  }

  back() {
    this.router.navigate(RouteMapping[this.routeKey]);
  }
}
