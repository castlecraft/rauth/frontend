import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { KeyEventService } from '../../common/key-event/key-event.service';
import { DURATION, NEW_ID } from '../../constants/common';
import {
  BASIC_HEADER,
  BODY_PARAM,
  CLIENT_CREATED,
  CLIENT_ERROR,
  CLIENT_UPDATED,
  CLOSE,
  DELETE_ERROR,
  DELETE_TOKEN,
  PUBLIC_CLIENT,
} from '../../constants/messages';
import { ClientService } from '../client/client.service';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';
import { ClientAuthentication } from './client-authentication.enum';
import { RouteMapping } from '../../constants/route-mapping';
// export const CLIENT_LIST_ROUTE = ['admin', 'list', 'client'];

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
})
export class ClientComponent implements OnInit {
  uuid: string;
  clientId: string;
  clientName: string;
  clientSecret: string;
  clientURL: string;
  isTrusted: boolean;
  isDisabled: boolean = false;
  autoApprove: boolean;
  enforcePKCE: boolean = false;
  allowPlain: boolean = false;
  clientScopes: any[];
  callbackURLs: string[];
  tokenDeleteEndpoint: string;
  userDeleteEndpoint: string;
  accessTokenValidity: number;
  refreshTokenExpiresInHours: number;
  restrictTokenUsage: boolean = false;
  is2FAMandated: boolean = false;
  revokeOnRefresh: boolean = false;
  authCodeExpiresInMinutes: number;
  changedClientSecret: string;
  customLoginRoute: string;

  hideClientSecret: boolean = true;
  hideChangedClientSecret: boolean = true;

  scopes: any[] = [];

  clientForm: FormGroup;
  callbackURLForms: FormArray;

  authenticationMethod = ClientAuthentication.PublicClient;
  authMethods = [
    { value: ClientAuthentication.BasicHeader, viewValue: BASIC_HEADER },
    { value: ClientAuthentication.PublicClient, viewValue: PUBLIC_CLIENT },
    { value: ClientAuthentication.BodyParam, viewValue: BODY_PARAM },
  ];
  new = NEW_ID;
  routeKey: string = '';
  constructor(
    private readonly clientService: ClientService,
    private route: ActivatedRoute,
    private router: Router,
    private snackBar: MatSnackBar,
    private formBuilder: FormBuilder,
    private dialog: MatDialog,
    private keyEventService: KeyEventService,
  ) {
    this.uuid = this.route.snapshot.params.id;
  }

  ngOnInit() {
    this.keyEventService.registerRouteFunction(
      '/admin/form/client/:id',
      this.HitSave.bind(this),
    );
    this.clientForm = this.formBuilder.group({
      clientName: this.clientName,
      authenticationMethod: this.authenticationMethod,
      clientURL: this.clientURL,
      clientScopes: this.clientScopes,
      tokenDeleteEndpoint: this.tokenDeleteEndpoint,
      userDeleteEndpoint: this.userDeleteEndpoint,
      accessTokenValidity: this.accessTokenValidity,
      refreshTokenExpiresInHours: this.refreshTokenExpiresInHours,
      restrictTokenUsage: this.restrictTokenUsage,
      is2FAMandated: this.is2FAMandated,
      revokeOnRefresh: this.revokeOnRefresh,
      authCodeExpiresInMinutes: this.authCodeExpiresInMinutes,
      callbackURLForms: this.formBuilder.array([]),
      isDisabled: this.isDisabled,
      isTrusted: this.isTrusted,
      enforcePKCE: this.enforcePKCE,
      allowPlain: this.allowPlain,
      autoApprove: this.autoApprove,
      clientId: this.clientId,
      clientSecret: this.clientSecret,
      changedClientSecret: this.changedClientSecret,
      customLoginRoute: this.customLoginRoute,
    });

    if (this.uuid && this.uuid !== this.new) {
      this.subscribeGetClient(this.uuid);
    }
    this.subscribeGetScopes();
    this.setupFieldObservables();
    this.routeKey =
      this.router.url.split('/')[this.router.url.split('/').length - 2];
    if (
      this.clientForm.controls.authenticationMethod.value ===
      ClientAuthentication.PublicClient
    ) {
      this.clientForm.controls.enforcePKCE.setValue(true);
      this.clientForm.controls.enforcePKCE.disable();
    }
    this.clientForm.get('authenticationMethod').valueChanges.subscribe({
      next: res => {
        if (res === ClientAuthentication.PublicClient) {
          this.clientForm.controls.enforcePKCE.setValue(true);
          this.clientForm.controls.enforcePKCE.disable();
        } else {
          this.clientForm.controls.enforcePKCE.enable();
        }
      },
    });
  }

  HitSave() {
    if (this.clientId) {
      this.updateClient();
    } else {
      this.createClient();
    }
  }

  createCallbackURLFormGroup(callbackURL?: string): FormGroup {
    return this.formBuilder.group({
      callbackURL,
    });
  }

  addCallbackURL(callbackURL?: string) {
    this.callbackURLForms = this.clientForm.get(
      'callbackURLForms',
    ) as FormArray;
    this.callbackURLForms.push(this.createCallbackURLFormGroup(callbackURL));
  }

  removeCallbackURL(formGroupID: number) {
    this.callbackURLForms.removeAt(formGroupID);
  }

  subscribeGetClient(clientId: string) {
    this.clientService.getClient(clientId).subscribe({
      next: response => {
        if (response) {
          this.changedClientSecret = response.changedClientSecret;
          this.populateClientForm(response);
        }
      },
    });
  }

  createClient() {
    if (!this.clientForm.valid) {
      this.snackBar.open(CLIENT_ERROR, CLOSE, { duration: DURATION });
      return;
    }
    this.clientService
      .createClient(
        this.clientForm.controls.clientName.value,
        this.clientForm.controls.authenticationMethod.value,
        this.getCallbackURLs(),
        this.clientForm.controls.clientScopes.value,
        this.clientForm.controls.isDisabled.value,
        this.clientForm.controls.isTrusted.value ? '1' : '0',
        this.clientForm.controls.autoApprove.value,
        this.clientForm.controls.enforcePKCE.value,
        this.getPKCEMethods(),
        this.getValueOrNull(this.clientForm.controls.customLoginRoute.value),
        this.clientForm.controls.tokenDeleteEndpoint.value,
        this.clientForm.controls.userDeleteEndpoint.value,
        Number(this.clientForm.controls.accessTokenValidity.value),
        Number(this.clientForm.controls.refreshTokenExpiresInHours.value),
        this.clientForm.controls.restrictTokenUsage.value,
        this.clientForm.controls.is2FAMandated.value,
        this.clientForm.controls.revokeOnRefresh.value,
        Number(this.clientForm.controls.authCodeExpiresInMinutes.value),
      )
      .subscribe({
        next: success => {
          this.snackBar.open(CLIENT_CREATED, CLOSE, { duration: DURATION });
          this.router.navigate(RouteMapping[this.routeKey]);
        },
        error: error => {
          if (error.error.message) {
            this.snackBar.open(error.error.message, CLOSE, {
              duration: DURATION,
            });
          } else {
            this.snackBar.open(CLIENT_ERROR, CLOSE, { duration: DURATION });
          }
        },
      });
  }

  getCallbackURLs(): string[] {
    const callbackURLFormGroups = this.clientForm.get(
      'callbackURLForms',
    ) as FormArray;
    const callbackURLs: string[] = [];
    for (const control of callbackURLFormGroups.controls) {
      callbackURLs.push(control.value.callbackURL);
    }
    return callbackURLs;
  }

  updateClient() {
    if (!this.clientForm.valid) {
      this.snackBar.open(CLIENT_ERROR, CLOSE, { duration: DURATION });
      return;
    }
    this.clientService
      .updateClient(
        this.clientId,
        this.clientForm.controls.clientName.value,
        this.clientForm.controls.authenticationMethod.value,
        this.clientForm.controls.tokenDeleteEndpoint.value,
        this.clientForm.controls.userDeleteEndpoint.value,
        Number(this.clientForm.controls.accessTokenValidity.value),
        Number(this.clientForm.controls.refreshTokenExpiresInHours.value),
        this.clientForm.controls.restrictTokenUsage.value,
        this.clientForm.controls.is2FAMandated.value,
        this.clientForm.controls.revokeOnRefresh.value,
        Number(this.clientForm.controls.authCodeExpiresInMinutes.value),
        this.getCallbackURLs(),
        this.clientForm.controls.clientScopes.value,
        this.clientForm.controls.isDisabled.value,
        this.clientForm.controls.isTrusted.value ? '1' : '0',
        this.clientForm.controls.autoApprove.value,
        this.clientForm.controls.enforcePKCE.value,
        this.getPKCEMethods(),
        this.getValueOrNull(this.clientForm.controls.customLoginRoute.value),
      )
      .subscribe({
        next: success => {
          this.snackBar.open(CLIENT_UPDATED, CLOSE, { duration: DURATION });
          this.router.navigate(RouteMapping[this.routeKey]);
        },
        error: error => {
          if (error.error.message) {
            this.snackBar.open(error.error.message, CLOSE, {
              duration: DURATION,
            });
          } else {
            this.snackBar.open(CLIENT_ERROR, CLOSE, { duration: DURATION });
          }
        },
      });
  }

  subscribeGetScopes() {
    this.clientService.getScopes().subscribe({
      next: (response: any) => {
        if (response) {
          response.map(scope => {
            this.scopes.push(scope.name);
          });
        }
      },
    });
  }

  getPKCEMethods() {
    const pkceMethods = ['s256'];
    if (this.clientForm.controls.allowPlain.value === true) {
      pkceMethods.push('plain');
    }
    return pkceMethods;
  }

  populateClientForm(client) {
    this.clientId = client.clientId;
    this.clientSecret = client.clientSecret;
    this.clientName = client.name;
    this.callbackURLs = client.redirectUris;
    this.isTrusted = client.isTrusted;
    this.isDisabled = client.disabled;
    this.enforcePKCE = client.enforcePKCE;
    this.allowPlain = client.pkceMethods?.includes('plain') || false;
    if (client.authenticationMethod) {
      this.authenticationMethod = client.authenticationMethod;
      this.clientForm.controls.authenticationMethod.setValue(
        client.authenticationMethod,
      );
    }

    this.clientForm.controls.tokenDeleteEndpoint.setValue(
      client.tokenDeleteEndpoint,
    );
    this.clientForm.controls.userDeleteEndpoint.setValue(
      client.userDeleteEndpoint,
    );
    this.clientForm.controls.accessTokenValidity.setValue(
      client.accessTokenValidity,
    );
    this.clientForm.controls.refreshTokenExpiresInHours.setValue(
      client.refreshTokenExpiresInHours,
    );
    this.clientForm.controls.restrictTokenUsage.setValue(
      client.restrictTokenUsage,
    );
    this.clientForm.controls.is2FAMandated.setValue(client.is2FAMandated);
    this.clientForm.controls.revokeOnRefresh.setValue(client.revokeOnRefresh);
    this.clientForm.controls.authCodeExpiresInMinutes.setValue(
      client.authCodeExpiresInMinutes,
    );
    this.clientForm.controls.changedClientSecret.setValue(
      client.changedClientSecret,
    );
    this.callbackURLs.forEach(element => {
      this.addCallbackURL(element);
    });
    this.clientForm.controls.clientId.setValue(client.clientId);
    this.clientForm.controls.clientSecret.setValue(client.clientSecret);
    this.clientForm.controls.clientName.setValue(client.name);
    this.clientForm.controls.isDisabled.setValue(client.disabled);
    this.clientForm.controls.isTrusted.setValue(client.isTrusted);
    this.clientForm.controls.clientScopes.setValue(client.allowedScopes);
    this.toggleTrustedAutoApprove(client.isTrusted);
    this.clientForm.controls.autoApprove.setValue(client.autoApprove);
    this.clientForm.controls.customLoginRoute.setValue(client.customLoginRoute);
    this.clientForm.controls.enforcePKCE.setValue(client.enforcePKCE);
    this.clientForm.controls.allowPlain.setValue(this.allowPlain);
  }

  setupFieldObservables() {
    this.clientForm.controls.isTrusted.valueChanges.subscribe({
      next: value => {
        this.toggleTrustedAutoApprove(value);
      },
      error: error => {},
    });
  }

  toggleTrustedAutoApprove(isTrusted: boolean) {
    if (isTrusted) {
      this.clientForm.controls.autoApprove.setValue(true);
      this.clientForm.controls.autoApprove.disable();
    } else {
      this.clientForm.controls.autoApprove.setValue(false);
      this.clientForm.controls.autoApprove.enable();
    }
  }

  delete() {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.clientService.deleteClient(this.clientId).subscribe({
          next: res => {
            this.router.navigate(RouteMapping[this.routeKey]);
          },
          error: err => {
            this.snackBar.open(DELETE_ERROR, CLOSE, { duration: DURATION });
          },
        });
      }
    });
  }

  deleteClientToken() {
    this.clientService.deleteClientToken(this.clientId).subscribe({
      next: res => {
        this.snackBar.open(DELETE_TOKEN, CLOSE, { duration: DURATION });
        this.router.navigate(RouteMapping[this.routeKey]);
      },
      error: error => {},
    });
  }

  back() {
    this.router.navigate(RouteMapping[this.routeKey]);
  }

  getValueOrNull(value: string) {
    return value ? value : null;
  }
}
