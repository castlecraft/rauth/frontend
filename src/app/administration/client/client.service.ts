import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, forkJoin, switchMap } from 'rxjs';
import { ClientAuthentication } from './client-authentication.enum';
import { TokenService } from '../../auth/token/token.service';
import {
  CREATE_CLIENT_ENDPOINT,
  DELETE_BEARER_TOKENS_ENDPOINT,
  DELETE_CLIENT_ENDPOINT,
  FIND_SCOPE_ENDPOINT,
  GET_CLIENT_ENDPOINT,
  INFO_ENDPOINT,
  UPDATE_CLIENT_ENDPOINT,
} from '../../constants/url-paths';

@Injectable()
export class ClientService {
  constructor(
    private readonly http: HttpClient,
    private readonly token: TokenService,
  ) {}

  getClient(clientID: string): Observable<any> {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url = config.issuerUrl + `${GET_CLIENT_ENDPOINT}/${clientID}`;
        return this.http.get<string>(url, { headers });
      }),
    );
  }

  verifyClient(clientURL: string) {
    const url = `${clientURL}/${INFO_ENDPOINT}`;
    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.get<string>(url, { headers });
      }),
    );
  }

  createClient(
    clientName: string,
    authenticationMethod: ClientAuthentication,
    callbackURLs: string[],
    scopes: string[],
    disabled = false,
    isTrusted: string,
    autoApprove: boolean,
    enforcePKCE: boolean,
    pkceMethods: string[],
    customLoginRoute: string,
    tokenDeleteEndpoint: string,
    userDeleteEndpoint: string,
    accessTokenValidity: number,
    refreshTokenExpiresInHours: number,
    restrictTokenUsage: boolean,
    is2FAMandated: boolean,
    revokeOnRefresh: boolean,
    authCodeExpiresInMinutes: number,
  ) {
    return forkJoin({
      headers: this.token.getHeaders(),
      config: this.token.config,
    }).pipe(
      switchMap(({ headers, config }) => {
        const url = `${config.issuerUrl}${CREATE_CLIENT_ENDPOINT}`;
        const clientData = {
          name: clientName,
          authenticationMethod,
          redirectUris: callbackURLs,
          allowedScopes: scopes,
          disabled,
          isTrusted,
          autoApprove,
          enforcePKCE,
          pkceMethods,
          customLoginRoute,
          tokenDeleteEndpoint,
          userDeleteEndpoint,
          accessTokenValidity,
          refreshTokenExpiresInHours,
          restrictTokenUsage,
          is2FAMandated,
          revokeOnRefresh,
          authCodeExpiresInMinutes,
        };
        return this.http.post(url, clientData, { headers });
      }),
    );
  }

  updateClient(
    clientId: string,
    clientName: string,
    authenticationMethod: ClientAuthentication,
    tokenDeleteEndpoint: string,
    userDeleteEndpoint: string,
    accessTokenValidity: number,
    refreshTokenExpiresInHours: number,
    restrictTokenUsage: boolean,
    is2FAMandated: boolean,
    revokeOnRefresh: boolean,
    authCodeExpiresInMinutes: number,
    callbackURLs: string[],
    scopes: string[],
    disabled: boolean = false,
    isTrusted: string,
    autoApprove: boolean,
    enforcePKCE: boolean,
    pkceMethods: string[],
    customLoginRoute: string,
  ) {
    return forkJoin({
      headers: this.token.getHeaders(),
      config: this.token.config,
    }).pipe(
      switchMap(({ headers, config }) => {
        const url = `${config.issuerUrl}${UPDATE_CLIENT_ENDPOINT}/${clientId}`;

        return this.http.post(
          url,
          {
            name: clientName,
            authenticationMethod,
            tokenDeleteEndpoint,
            userDeleteEndpoint,
            accessTokenValidity,
            refreshTokenExpiresInHours,
            restrictTokenUsage,
            is2FAMandated,
            revokeOnRefresh,
            authCodeExpiresInMinutes,
            redirectUris: callbackURLs,
            allowedScopes: scopes,
            disabled,
            isTrusted,
            autoApprove,
            enforcePKCE,
            pkceMethods,
            customLoginRoute,
          },
          { headers },
        );
      }),
    );
  }

  getScopes() {
    return forkJoin({
      headers: this.token.getHeaders(),
      config: this.token.config,
    }).pipe(
      switchMap(({ headers, config }) => {
        const url = `${config.issuerUrl}${FIND_SCOPE_ENDPOINT}`;
        return this.http.get<string>(url, { headers });
      }),
    );
  }

  deleteClient(clientId: string) {
    return forkJoin({
      headers: this.token.getHeaders(),
      config: this.token.config,
    }).pipe(
      switchMap(({ headers, config }) => {
        const url = `${config.issuerUrl}${DELETE_CLIENT_ENDPOINT}/${clientId}`;
        return this.http.post(url, undefined, { headers });
      }),
    );
  }

  deleteClientToken(clientId) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const requestUrl = config.issuerUrl + DELETE_BEARER_TOKENS_ENDPOINT;
        return this.http.post(requestUrl, { clientId }, { headers });
      }),
    );
  }
}
