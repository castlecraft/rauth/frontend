import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { OAuth2ProviderService } from './oauth2-provider.service';
import {
  CREATE_SUCCESSFUL,
  CLOSE,
  CREATE_ERROR,
  UPDATE_SUCCESSFUL,
  UPDATE_ERROR,
  DELETE_ERROR,
} from '../../constants/messages';
import { NEW_ID, DURATION } from '../../constants/common';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';
import { KeyEventService } from '../../common/key-event/key-event.service';
import { RouteMapping } from '../../constants/route-mapping';
// export const OAUTH2_PROVIDER_LIST_ROUTE = ['admin', 'list', 'oauth2_provider'];

@Component({
  selector: 'app-oauth2-provider',
  templateUrl: './oauth2-provider.component.html',
})
export class OAuth2ProviderComponent implements OnInit {
  uuid: string;
  new: string = NEW_ID;
  routeKey: string = '';
  hideClientSecret: boolean = true;
  name: string;
  isDisabled: boolean = false;
  scope: string[];
  scopesForm = new FormArray([]);

  providerForm = new FormGroup({
    name: new FormControl(),
    isDisabled: new FormControl(),
    authServerURL: new FormControl(),
    clientId: new FormControl(),
    clientSecret: new FormControl(),
    redirectURI: new FormControl(),
    profileURL: new FormControl(),
    tokenURL: new FormControl(),
    introspectionURL: new FormControl(),
    authorizationURL: new FormControl(),
    revocationURL: new FormControl(),
    scope: this.scopesForm,
  });

  constructor(
    private service: OAuth2ProviderService,
    private snackBar: MatSnackBar,
    private router: Router,
    route: ActivatedRoute,
    private readonly dialog: MatDialog,
    private keyEventService: KeyEventService,
  ) {
    this.uuid = route.snapshot.params.id;
  }

  ngOnInit() {
    this.keyEventService.registerRouteFunction(
      '/admin/form/oauth2_provider/:id',
      this.HitSave.bind(this),
    );
    if (this.uuid && this.uuid !== this.new) {
      this.subscribeGetSocialLogin(this.uuid);
    } else if (this.uuid === this.new) {
      this.uuid = undefined;
    }
    this.routeKey =
      this.router.url.split('/')[this.router.url.split('/').length - 2];
  }

  HitSave() {
    if (this.name) {
      this.updateProvider();
    } else {
      this.createProvider();
    }
  }

  createProvider() {
    this.service
      .createProvider(
        this.providerForm.controls.name.value,
        this.providerForm.controls.isDisabled.value,
        this.providerForm.controls.authServerURL.value,
        this.providerForm.controls.clientId.value,
        this.providerForm.controls.clientSecret.value,
        this.providerForm.controls.profileURL.value,
        this.providerForm.controls.tokenURL.value,
        this.providerForm.controls.introspectionURL.value,
        this.providerForm.controls.authorizationURL.value,
        this.providerForm.controls.revocationURL.value,
        this.getScopes(),
      )
      .subscribe({
        next: response => {
          this.snackBar.open(CREATE_SUCCESSFUL, CLOSE, { duration: DURATION });
          this.router.navigate(RouteMapping[this.routeKey]);
        },
        error: error => {
          this.snackBar.open(CREATE_ERROR, CLOSE, { duration: DURATION });
        },
      });
  }

  updateProvider() {
    this.service
      .updateProvider(
        this.uuid,
        this.providerForm.controls.name.value,
        this.providerForm.controls.isDisabled.value,
        this.providerForm.controls.authServerURL.value,
        this.providerForm.controls.clientId.value,
        this.providerForm.controls.clientSecret.value,
        this.providerForm.controls.profileURL.value,
        this.providerForm.controls.tokenURL.value,
        this.providerForm.controls.introspectionURL.value,
        this.providerForm.controls.authorizationURL.value,
        this.providerForm.controls.revocationURL.value,
        this.getScopes(),
      )
      .subscribe({
        next: response => {
          this.snackBar.open(UPDATE_SUCCESSFUL, CLOSE, { duration: DURATION });
          this.router.navigate(RouteMapping[this.routeKey]);
        },
        error: error => {
          this.snackBar.open(UPDATE_ERROR, CLOSE, { duration: DURATION });
        },
      });
  }

  removeScope(formGroupID: number) {
    this.scopesForm.removeAt(formGroupID);
  }

  addScope(scope?: string) {
    this.scopesForm.push(new FormGroup({ scope: new FormControl(scope) }));
  }

  getScopes(): string[] {
    const scopesFormGroup = this.providerForm.get('scope') as FormArray;
    const scopes: string[] = [];
    for (const control of scopesFormGroup.controls) {
      scopes.push(control.value.scope);
    }
    return scopes;
  }

  subscribeGetSocialLogin(uuid: string) {
    this.service.getProvider(this.uuid).subscribe({
      next: response => {
        if (response) {
          this.populateForm(response);
        }
      },
    });
  }

  populateForm(response) {
    this.name = response.name;
    this.isDisabled = response.disabled;
    this.providerForm.controls.name.setValue(response.name);
    this.providerForm.controls.isDisabled.setValue(response.disabled);
    this.providerForm.controls.authServerURL.setValue(response.authServerURL);
    this.providerForm.controls.clientId.setValue(response.clientId);
    this.providerForm.controls.clientSecret.setValue(response.clientSecret);

    this.service.generateRedirectURL(this.uuid).subscribe({
      next: redirect =>
        this.providerForm.controls.redirectURI.setValue(redirect),
    });
    this.providerForm.controls.profileURL.setValue(response.profileURL);
    this.providerForm.controls.tokenURL.setValue(response.tokenURL);
    this.providerForm.controls.introspectionURL.setValue(
      response.introspectionURL,
    );
    this.providerForm.controls.authorizationURL.setValue(
      response.authorizationURL,
    );
    this.providerForm.controls.revocationURL.setValue(response.revocationURL);
    this.scope = response.scope;
    this.scope.forEach(scope => {
      this.addScope(scope);
    });
  }

  delete() {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.service.deleteProvider(this.uuid).subscribe({
          next: res => {
            this.router.navigate(RouteMapping[this.routeKey]);
          },
          error: error => {
            this.snackBar.open(DELETE_ERROR, CLOSE, { duration: DURATION });
          },
        });
      }
    });
  }

  back() {
    this.router.navigate(RouteMapping[this.routeKey]);
  }
}
