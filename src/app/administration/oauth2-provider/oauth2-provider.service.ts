import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, switchMap } from 'rxjs';
import { TokenService } from '../../auth/token/token.service';
import {
  ADD_OAUTH2_PROVIDER_ENDPOINT,
  GET_OAUTH2_PROVIDER_ENDPOINT,
  OAUTH2_PROVIDER_CALLBACK,
  REMOVE_OAUTH2_PROVIDER_ENDPOINT,
  UPDATE_OAUTH2_PROVIDER_ENDPOINT,
} from '../../constants/url-paths';

@Injectable({
  providedIn: 'root',
})
export class OAuth2ProviderService {
  constructor(
    private readonly http: HttpClient,
    private readonly token: TokenService,
  ) {}

  createProvider(
    name: string,
    disabled: boolean = false,
    authServerURL: string,
    clientId: string,
    clientSecret: string,
    profileURL: string,
    tokenURL: string,
    introspectionURL: string,
    authorizationURL: string,
    revocationURL: string,
    scope: string[],
  ) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url = config.issuerUrl + ADD_OAUTH2_PROVIDER_ENDPOINT;
        const payload = {
          name,
          disabled,
          authServerURL,
          clientId,
          clientSecret,
          profileURL,
          tokenURL,
          introspectionURL,
          authorizationURL,
          revocationURL,
          scope,
        };

        return this.http.post(url, payload, { headers });
      }),
    );
  }

  updateProvider(
    uuid: string,
    name: string,
    disabled: boolean = false,
    authServerURL: string,
    clientId: string,
    clientSecret: string,
    profileURL: string,
    tokenURL: string,
    introspectionURL: string,
    authorizationURL: string,
    revocationURL: string,
    scope: string[],
  ) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url =
          config.issuerUrl + `${UPDATE_OAUTH2_PROVIDER_ENDPOINT}/${uuid}`;
        const payload = {
          name,
          disabled,
          authServerURL,
          clientId,
          clientSecret,
          profileURL,
          tokenURL,
          introspectionURL,
          authorizationURL,
          revocationURL,
          scope,
        };

        return this.http.post(url, payload, { headers });
      }),
    );
  }

  getProvider(uuid: string) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url =
          config.issuerUrl + `${GET_OAUTH2_PROVIDER_ENDPOINT}/${uuid}`;
        return this.http.get<any>(url, { headers });
      }),
    );
  }

  generateRedirectURL(uuid: string) {
    return this.token.config.pipe(
      map(config => config.issuerUrl + OAUTH2_PROVIDER_CALLBACK + '/' + uuid),
    );
  }

  deleteProvider(uuid: string) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url =
          config.issuerUrl + `${REMOVE_OAUTH2_PROVIDER_ENDPOINT}/${uuid}`;
        return this.http.post(url, undefined, { headers });
      }),
    );
  }
}
