import { TestBed } from '@angular/core/testing';

import { LDAPClientService } from './ldap-client.service';
import { provideHttpClientTesting } from '@angular/common/http/testing';
import {
  provideHttpClient,
  withInterceptorsFromDi,
} from '@angular/common/http';

describe('LDAPClientService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [],
      providers: [
        provideHttpClient(withInterceptorsFromDi()),
        provideHttpClientTesting(),
      ],
    }),
  );

  it('should be created', () => {
    const service: LDAPClientService = TestBed.get(LDAPClientService);
    expect(service).toBeTruthy();
  });
});
