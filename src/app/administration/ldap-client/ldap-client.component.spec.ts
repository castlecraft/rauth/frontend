import { waitForAsync, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { provideHttpClientTesting } from '@angular/common/http/testing';
import { EMPTY } from 'rxjs';

import { LDAPClientComponent } from './ldap-client.component';
import { AuthServerMaterialModule } from '../../auth-server-material/auth-server-material.module';
import { LDAPClientService } from './ldap-client.service';
import { routes } from '../../app-routing.module';
import {
  provideHttpClient,
  withInterceptorsFromDi,
} from '@angular/common/http';

describe('LDAPClientComponent', () => {
  let component: LDAPClientComponent;
  let fixture: ComponentFixture<LDAPClientComponent>;
  let ldapClientService: LDAPClientService;

  beforeEach(waitForAsync(() => {
    ldapClientService = jasmine.createSpyObj(['getScopes']);
    ldapClientService.getScopes = () => EMPTY;

    TestBed.configureTestingModule({
      declarations: [LDAPClientComponent],
      imports: [
        RouterModule.forRoot(routes),
        AuthServerMaterialModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
      ],
      providers: [
        {
          provide: LDAPClientService,
          useValue: ldapClientService,
        },
        provideHttpClient(withInterceptorsFromDi()),
        provideHttpClientTesting(),
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LDAPClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
