import { Component, OnInit } from '@angular/core';
import { FormArray, FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { LDAPClientService } from './ldap-client.service';
import { NEW_ID, DURATION } from '../../constants/common';
import {
  LDAP_CLIENT_ERROR,
  LDAP_CLIENT_UPDATED,
  LDAP_CLIENT_CREATED,
  CLOSE,
  UPDATE_ERROR,
  DELETE_ERROR,
} from '../../constants/messages';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';
import { AuthSettingsService } from '../auth-settings/auth-settings.service';
import { KeyEventService } from '../../common/key-event/key-event.service';
import { RouteMapping } from '../../constants/route-mapping';
// export const LDAP_CLIENT_LIST_ROUTE = ['admin', 'list', 'ldap_client'];

@Component({
  selector: 'app-ldap-client',
  templateUrl: './ldap-client.component.html',
})
export class LDAPClientComponent implements OnInit {
  uuid: string;
  name: string;
  isDisabled: boolean = false;
  skipMobileVerification: boolean = false;
  hideAdminPassword: boolean = true;
  hideKeyPassphrase: boolean = true;
  attributeForm = new FormArray([]);
  ca = new FormArray([]);
  disableUserCreation: boolean = false;
  ldapClientForm = new FormGroup({
    name: new FormControl(),
    isDisabled: new FormControl(),
    disableUserCreation: new FormControl(),
    skipMobileVerification: new FormControl(),
    description: new FormControl(),
    url: new FormControl(),
    adminDn: new FormControl(),
    adminPassword: new FormControl(),
    userSearchBase: new FormControl(),
    usernameAttribute: new FormControl(),
    emailAttribute: new FormControl(),
    fullNameAttribute: new FormControl(),
    timeoutMs: new FormControl(),
    allowedFailedLoginAttempts: new FormControl(),
    phoneAttribute: new FormControl(),
    clientId: new FormControl(),
    scope: new FormControl(),
    attributes: this.attributeForm,
    isSecure: new FormControl(false),
    key: new FormControl(),
    keyPassphrase: new FormControl(),
    cert: new FormControl(),
    ca: this.ca,
  });

  addCA(ca?: string) {
    this.ca.push(new FormGroup({ ca: new FormControl(ca) }));
  }

  removeCA(formGroupID: number) {
    this.ca.removeAt(formGroupID);
  }

  new = NEW_ID;
  routeKey: string = '';
  clientList: any[];
  scopeList: any[];

  constructor(
    private ldapClientService: LDAPClientService,
    private settings: AuthSettingsService,
    private route: ActivatedRoute,
    private router: Router,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private keyEventService: KeyEventService,
  ) {
    this.uuid = this.route.snapshot.params.id;
  }

  ngOnInit() {
    this.keyEventService.registerRouteFunction(
      '/admin/form/ldap_client/:id',
      this.HitSave.bind(this),
    );
    if (this.uuid && this.uuid !== this.new) {
      this.subscribeGetLDAPClients(this.uuid);
    } else if (this.uuid === NEW_ID) {
      this.uuid = undefined;
    }
    this.settings.getClientList().subscribe({
      next: (response: any[]) => {
        this.clientList = response;
      },
      error: error => {},
    });
    this.ldapClientService.getScopes().subscribe({
      next: response => {
        this.scopeList = response;
      },
      error: error => {},
    });
    this.routeKey =
      this.router.url.split('/')[this.router.url.split('/').length - 2];
  }

  HitSave() {
    if (this.uuid) {
      this.updateLDAPClient();
    } else {
      this.createLDAPClient();
    }
  }

  subscribeGetLDAPClients(uuid: string) {
    this.ldapClientService.getLDAPClient(uuid).subscribe({
      next: response => {
        if (response) {
          this.populateForm(response);
        }
      },
    });
  }

  populateForm(ldapClient) {
    this.name = ldapClient.name;
    this.isDisabled = ldapClient.disabled;
    this.skipMobileVerification = ldapClient.skipMobileVerification;
    (ldapClient?.attributes || []).forEach(attribute => {
      this.addAttribute(attribute.claim, attribute.mapTo);
    });
    this.ldapClientForm.controls.name.setValue(ldapClient.name);
    this.ldapClientForm.controls.isDisabled.setValue(ldapClient.disabled);
    this.ldapClientForm.controls.skipMobileVerification.setValue(
      ldapClient.skipMobileVerification,
    );
    this.ldapClientForm.controls.disableUserCreation.setValue(
      ldapClient.disableUserCreation,
    );
    this.ldapClientForm.controls.description.setValue(ldapClient.description);
    this.ldapClientForm.controls.clientId.setValue(ldapClient.clientId);
    this.ldapClientForm.controls.adminPassword.setValue(
      ldapClient.adminPassword,
    );
    this.ldapClientForm.controls.adminDn.setValue(ldapClient.adminDn);
    this.ldapClientForm.controls.userSearchBase.setValue(
      ldapClient.userSearchBase,
    );
    this.ldapClientForm.controls.usernameAttribute.setValue(
      ldapClient.usernameAttribute,
    );
    this.ldapClientForm.controls.emailAttribute.setValue(
      ldapClient.emailAttribute,
    );
    this.ldapClientForm.controls.fullNameAttribute.setValue(
      ldapClient.fullNameAttribute,
    );
    this.ldapClientForm.controls.timeoutMs.setValue(ldapClient.timeoutMs);
    this.ldapClientForm.controls.allowedFailedLoginAttempts.setValue(
      ldapClient.allowedFailedLoginAttempts,
    );
    this.ldapClientForm.controls.phoneAttribute.setValue(
      ldapClient.phoneAttribute,
    );
    this.ldapClientForm.controls.url.setValue(ldapClient.url);
    this.ldapClientForm.controls.scope.setValue(ldapClient.scope);
    this.ldapClientForm.controls.isSecure.setValue(ldapClient.isSecure);
    this.ldapClientForm.controls.cert.setValue(ldapClient.cert);
    (ldapClient?.ca || []).forEach(cert => this.addCA(cert));
    this.ldapClientForm.controls.ca.setValue(ldapClient.ca);
  }

  createLDAPClient() {
    this.ldapClientService
      .createLDAPClient(
        this.ldapClientForm.controls.name.value,
        this.ldapClientForm.controls.isDisabled.value,
        this.ldapClientForm.controls.skipMobileVerification.value,
        this.ldapClientForm.controls.disableUserCreation.value,
        this.ldapClientForm.controls.description.value,
        this.ldapClientForm.controls.url.value,
        this.ldapClientForm.controls.adminDn.value,
        this.ldapClientForm.controls.adminPassword.value,
        this.ldapClientForm.controls.userSearchBase.value,
        this.ldapClientForm.controls.usernameAttribute.value,
        this.ldapClientForm.controls.emailAttribute.value,
        this.ldapClientForm.controls.phoneAttribute.value,
        this.ldapClientForm.controls.fullNameAttribute.value,
        Number(this.ldapClientForm.controls.timeoutMs.value),
        Number(this.ldapClientForm.controls.allowedFailedLoginAttempts.value),
        this.ldapClientForm.controls.clientId.value,
        this.ldapClientForm.controls.scope.value,
        this.ldapClientForm.controls.isSecure.value,
        this.ldapClientForm.controls.key.value,
        this.ldapClientForm.controls.keyPassphrase.value,
        this.ldapClientForm.controls.cert.value,
        this.getCA(),
        this.getAttributes(),
      )
      .subscribe({
        next: success => {
          this.snackBar.open(LDAP_CLIENT_CREATED, CLOSE, {
            duration: DURATION,
          });
          this.router.navigate(RouteMapping[this.routeKey]);
        },
        error: error =>
          this.snackBar.open(LDAP_CLIENT_ERROR, CLOSE, { duration: DURATION }),
      });
  }

  getCA(): string[] {
    const caFormGroup = this.ldapClientForm.get('ca') as FormArray;
    const ca: string[] = [];
    for (const control of caFormGroup.controls) {
      ca.push(control.value.ca);
    }
    return ca;
  }

  updateLDAPClient() {
    this.ldapClientService
      .updateLDAPClient(
        this.uuid,
        this.ldapClientForm.controls.name.value,
        this.ldapClientForm.controls.isDisabled.value,
        this.ldapClientForm.controls.skipMobileVerification.value,
        this.ldapClientForm.controls.disableUserCreation.value,
        this.ldapClientForm.controls.description.value,
        this.ldapClientForm.controls.url.value,
        this.ldapClientForm.controls.adminDn.value,
        this.ldapClientForm.controls.adminPassword.value,
        this.ldapClientForm.controls.userSearchBase.value,
        this.ldapClientForm.controls.usernameAttribute.value,
        this.ldapClientForm.controls.emailAttribute.value,
        this.ldapClientForm.controls.phoneAttribute.value,
        this.ldapClientForm.controls.fullNameAttribute.value,
        Number(this.ldapClientForm.controls.timeoutMs.value),
        Number(this.ldapClientForm.controls.allowedFailedLoginAttempts.value),
        this.ldapClientForm.controls.clientId.value,
        this.ldapClientForm.controls.scope.value,
        this.ldapClientForm.controls.isSecure.value,
        this.ldapClientForm.controls.cert.value,
        this.ldapClientForm.controls.key.value,
        this.ldapClientForm.controls.keyPassphrase.value,
        this.getCA(),
        this.getAttributes(),
      )
      .subscribe({
        next: success => {
          this.snackBar.open(LDAP_CLIENT_UPDATED, CLOSE, {
            duration: DURATION,
          });
          this.router.navigate(RouteMapping[this.routeKey]);
        },
        error: error =>
          this.snackBar.open(UPDATE_ERROR, CLOSE, { duration: DURATION }),
      });
  }

  delete() {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.ldapClientService.deleteLDAPClient(this.uuid).subscribe({
          next: res => {
            this.router.navigate(RouteMapping[this.routeKey]);
          },
          error: error => {
            this.snackBar.open(DELETE_ERROR, CLOSE, { duration: DURATION });
          },
        });
      }
    });
  }

  addAttribute(attribute?: string, mapTo?: string) {
    this.attributeForm.push(
      new FormGroup({
        claim: new FormControl(attribute),
        mapTo: new FormControl(mapTo),
      }),
    );
  }

  removeAttribute(formGroupID: number) {
    this.attributeForm.removeAt(formGroupID);
  }

  getAttributes(): { claim: string; mapTo: string }[] {
    const attributes: { claim: string; mapTo: string }[] = [];
    for (const control of this.attributeForm.controls) {
      attributes.push({
        claim: control.value.claim,
        mapTo: control.value.mapTo,
      });
    }
    return attributes;
  }

  back() {
    this.router.navigate(RouteMapping[this.routeKey]);
  }
}
