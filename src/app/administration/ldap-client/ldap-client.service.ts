import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { switchMap } from 'rxjs/operators';
import { TokenService } from '../../auth/token/token.service';
import {
  CREATE_LDAP_CLIENT_ENDPOINT,
  DELETE_LDAP_CLIENT_ENDPOINT,
  FIND_SCOPE_ENDPOINT,
  GET_LDAP_CLIENT_ENDPOINT,
  UDPATE_LDAP_CLIENT_ENDPOINT,
} from '../../constants/url-paths';
import { forkJoin } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class LDAPClientService {
  constructor(
    private readonly http: HttpClient,
    private readonly token: TokenService,
  ) {}

  getLDAPClient(uuid: string) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url = config.issuerUrl + `${GET_LDAP_CLIENT_ENDPOINT}/${uuid}`;
        return this.http.get<string>(url, { headers });
      }),
    );
  }

  createLDAPClient(
    name: string,
    disabled: boolean = false,
    skipMobileVerification: boolean = false,
    disableUserCreation: boolean = false,
    description: string,
    url: string,
    adminDn: string,
    adminPassword: string,
    userSearchBase: string,
    usernameAttribute: string,
    emailAttribute: string,
    phoneAttribute: string,
    fullNameAttribute: string,
    timeoutMs: number,
    allowedFailedLoginAttempts: number,
    clientId: string,
    scope: string,
    isSecure: boolean,
    key: string,
    keyPassphrase: string,
    cert: string,
    ca: string[],
    attributes: { claim: string; mapTo: string }[],
  ) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const requestURL = config.issuerUrl + `${CREATE_LDAP_CLIENT_ENDPOINT}`;
        return this.http.post(
          requestURL,
          {
            name,
            disabled,
            skipMobileVerification,
            disableUserCreation,
            description,
            url,
            adminDn,
            adminPassword,
            userSearchBase,
            usernameAttribute,
            emailAttribute,
            phoneAttribute,
            fullNameAttribute,
            timeoutMs,
            allowedFailedLoginAttempts,
            clientId,
            scope,
            isSecure,
            key,
            keyPassphrase,
            cert,
            ca,
            attributes,
          },
          { headers },
        );
      }),
    );
  }

  updateLDAPClient(
    uuid: string,
    name: string,
    disabled: boolean = false,
    skipMobileVerification: boolean = false,
    disableUserCreation: boolean = false,
    description: string,
    url: string,
    adminDn: string,
    adminPassword: string,
    userSearchBase: string,
    usernameAttribute: string,
    emailAttribute: string,
    phoneAttribute: string,
    fullNameAttribute: string,
    timeoutMs: number,
    allowedFailedLoginAttempts: number,
    clientId: string,
    scope: string,
    isSecure: boolean,
    cert: string,
    key: string,
    keyPassphrase: string,
    ca: string[],
    attributes: { claim: string; mapTo: string }[],
  ) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const requestURL =
          config.issuerUrl + `${UDPATE_LDAP_CLIENT_ENDPOINT}/${uuid}`;
        return this.http.post(
          requestURL,
          {
            name,
            disabled,
            skipMobileVerification,
            disableUserCreation,
            description,
            url,
            adminDn,
            adminPassword,
            userSearchBase,
            usernameAttribute,
            emailAttribute,
            phoneAttribute,
            fullNameAttribute,
            timeoutMs,
            allowedFailedLoginAttempts,
            clientId,
            scope,
            isSecure,
            cert,
            key,
            keyPassphrase,
            ca,
            attributes,
          },
          { headers },
        );
      }),
    );
  }

  deleteLDAPClient(uuid: string) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url = config.issuerUrl + `${DELETE_LDAP_CLIENT_ENDPOINT}/${uuid}`;
        return this.http.post(url, undefined, { headers });
      }),
    );
  }

  getScopes() {
    return forkJoin({
      headers: this.token.getHeaders(),
      config: this.token.config,
    }).pipe(
      switchMap(({ headers, config }) => {
        const url = `${config.issuerUrl}${FIND_SCOPE_ENDPOINT}`;
        return this.http.get<any[]>(url, { headers });
      }),
    );
  }
}
