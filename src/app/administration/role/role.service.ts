import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { TokenService } from '../../auth/token/token.service';
import {
  CREATE_ROLE_ENDPOINT,
  DELETE_ROLE_ENDPOINT,
  GET_ROLE_ENDPOINT,
  LIST_ROLES_ENDPOINT,
  UPDATE_ROLE_ENDPOINT,
} from '../../constants/url-paths';

@Injectable({
  providedIn: 'root',
})
export class RoleService {
  constructor(
    private readonly http: HttpClient,
    private readonly token: TokenService,
  ) {}

  createRole(name: string) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url = config.issuerUrl + `${CREATE_ROLE_ENDPOINT}`;
        const clientData = { name };
        return this.http.post(url, clientData, { headers });
      }),
    );
  }

  getRole(role: string): Observable<any> {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url = config.issuerUrl + `${GET_ROLE_ENDPOINT}/${role}`;
        return this.http.get<string>(url, { headers });
      }),
    );
  }

  getRoles() {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url = config.issuerUrl + `${LIST_ROLES_ENDPOINT}`;
        return this.http.get<string>(url, { headers });
      }),
    );
  }

  updateRole(uuid: string, roleName: string) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url = config.issuerUrl + `${UPDATE_ROLE_ENDPOINT}/${uuid}`;
        const roleData = {
          name: roleName,
        };
        return this.http.post(url, roleData, { headers });
      }),
    );
  }

  deleteRole(roleName: string) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url = config.issuerUrl + `${DELETE_ROLE_ENDPOINT}/${roleName}`;

        return this.http.post(url, undefined, { headers });
      }),
    );
  }
}
