import { NgModule, LOCALE_ID } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  provideHttpClient,
  withInterceptorsFromDi,
} from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthServerMaterialModule } from './auth-server-material/auth-server-material.module';
import { AuthService } from './auth/auth.service';
import { AVAILABLE_TRANSLATIONS } from './constants/app-strings';
import { CallbackPage } from './auth/callback/callback.page';
import { BrandInfoService } from './common/brand-info/brand-info.service';
import { TokenService } from './auth/token/token.service';
import { StorageService } from './auth/storage/storage.service';
import { AdministrationModule } from './administration/administration.module';
import { AuthenticationModule } from './authentication/authentication.module';
import { KeyEventService } from './common/key-event/key-event.service';
import { NgxSpinnerModule } from 'ngx-spinner';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { SpinnerInterceptor } from './common/interceptor/spinner.interceptor';
import { ErrorComponent } from './common/error/error.component';
import { ErrorService } from './common/error/error.service';

let lang = navigator.language;
if (!AVAILABLE_TRANSLATIONS.includes(lang)) {
  lang = 'en-US';
}

@NgModule({
  declarations: [AppComponent, CallbackPage, ErrorComponent],
  bootstrap: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AuthServerMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    AdministrationModule,
    AuthenticationModule,
    NgxSpinnerModule.forRoot({ type: 'ball-scale-multiple' }),
  ],
  providers: [
    { provide: LOCALE_ID, useValue: lang },
    AuthService,
    BrandInfoService,
    TokenService,
    StorageService,
    ErrorService,
    KeyEventService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: SpinnerInterceptor,
      multi: true,
    },
    provideHttpClient(withInterceptorsFromDi()),
  ],
})
export class AppModule {}
