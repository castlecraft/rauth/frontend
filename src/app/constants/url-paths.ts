export const LOGOUT_URL = '/api/auth/logout';
export const GET_SETTINGS_ENDPOINT = '/api/settings/v1/get';
export const UPDATE_SETTINGS_ENDPOINT = '/api/settings/v1/update';
export const GET_TRUSTED_CLIENTS_ENDPOINT = '/api/client/v1/trusted_clients';
export const OPENID_CONFIGURATON_ENDPOINT = '/.well-known/openid-configuration';
export const INDEX_PAGE = '/index.html';
export const LIST_EMAILS_ENDPOINT = '/api/email/v1/list';
export const DELETE_SVC_TYPE_ENDPOINT = '/api/service_type/v1/delete';
export const CREATE_SVC_TYPE_ENDPOINT = '/api/service_type/v1/create';
export const GET_SVC_TYPE_BY_UUID_ENDPOINT = '/service_type/v1/get_by_uuid';
export const DELETE_SVC_ENDPOINT = '/api/service/v1/delete';
export const CREATE_SVC_ENDPOINT = '/api/service/v1/create';
export const MODIFY_SVC_ENDPOINT = '/api/service/v1/modify';
export const REGISTER_SVC_ENDPOINT = '/api/service/v1/register';
export const GET_SVC_BY_UUID_ENDPOINT = '/api/service/v1/get_by_uuid';
export const RETRIEVE_BRAND_INFO = '/api/brand/v1/retrieve_info';
export const UPDATE_BRAND_INFO = '/api/brand/v1/update_info';
export const CLEAR_TOKEN_CACHE_ENDPOINT = '/api/settings/v1/clear_token_cache';
export const LIST_STORAGE_ENDPOINT = '/api/storage/v1/list';
export const OAUTH2_PROVIDER_CALLBACK = '/api/oauth2_provider/callback';
export const GET_OAUTH2_PROVIDER_ENDPOINT =
  '/api/oauth2_provider/v1/retrieve_provider';
export const UPDATE_OAUTH2_PROVIDER_ENDPOINT =
  '/api/oauth2_provider/v1/update_provider';
export const ADD_OAUTH2_PROVIDER_ENDPOINT =
  '/api/oauth2_provider/v1/add_provider';
export const EMAIL_DELETE_ENDPOINT = '/api/email/v1/delete';
export const EMAIL_UPDATE_ENDPOINT = '/api/email/v1/update';
export const FIND_ROLE_ENDPOINT = '/api/role/v1/find';
export const GET_EMAIL_ENDPOINT = '/api/email/v1/get';
export const CREATE_EMAIL_ENDPOINT = '/api/email/v1/create';
export const REMOVE_STORAGE_ENDPOINT = '/api/storage/v1/remove';
export const REMOVE_OAUTH2_PROVIDER_ENDPOINT =
  '/api/oauth2_provider/v1/remove_provider';
export const RETRIEVE_USER_CLAIMS_ENDPOINT =
  '/api/user_claim/v1/retrieve_user_claims';
export const USER_DELETE_ENDPOINT = '/api/user/v1/delete';
export const USER_OTP_ENABLE_DISABLE_ENDPOINT = '/api/user/v1/enable_otp';
export const USER_ENABLE_DISABLE_ENDPOINT = '/api/user/v1/disable_user';
export const USER_DISABLE_PASSWORDLESS_ENDPOINT =
  '/api/user/v1/disable_password_less_login';
export const USER_ENABLE_PASSWORDLESS_ENDPOINT =
  '/api/user/v1/enable_password_less_login';
export const USER_CLEAR_AUTH_FAILURE_LOGS_ENDPOINT =
  '/api/user/v1/clear_auth_failure_logs';
export const SETUP_ENDPOINT = '/api/setup';
export const UPDATE_USER_ENDPOINT = '/api/user/v1/update';
export const CREATE_USER_ENDPOINT = '/api/user/v1/create';
export const INFO_ENDPOINT = '/api/info';
export const GET_USER_ENDPOINT = '/api/user/v1/get';
export const DELETE_SOCIAL_LOGIN_ENDPOINT = '/api/social_login/v1/delete';
export const SOCIAL_LOGIN_CALLBACK_ENDPOINT = '/api/social_login/callback';
export const UPDATE_SOCIAL_LOGIN_ENDPOINT = '/api/social_login/v1/update';
export const CREATE_SOCIAL_LOGIN_ENDPOINT = '/api/social_login/v1/create';
export const GET_SOCIAL_LOGIN_ENDPOINT = '/api/social_login/v1/get';
export const GET_SCOPE_ENDPOINT = '/api/scope/v1';
export const UPDATE_SCOPE_ENDPOINT = '/api/scope/v1/update';
export const CREATE_SCOPE_ENDPOINT = '/api/scope/v1/create';
export const DELETE_SCOPE_ENDPOINT = '/api/scope/v1/delete';
export const LIST_ROLES_ENDPOINT = '/api/role/v1/find';
export const CREATE_ROLE_ENDPOINT = '/api/role/v1/create';
export const UPDATE_ROLE_ENDPOINT = '/api/role/v1/update';
export const GET_ROLE_ENDPOINT = '/api/role/v1';
export const DELETE_ROLE_ENDPOINT = '/api/role/v1/delete';
export const DELETE_BEARER_TOKENS_ENDPOINT =
  '/api/settings/v1/delete_bearer_tokens';
export const DELETE_USER_SESSIONS_ENDPOINT =
  '/api/settings/v1/delete_user_sessions';
export const GET_CLIENT_ENDPOINT = '/api/client/v1/get';
export const CREATE_CLIENT_ENDPOINT = '/api/client/v1/create';
export const DELETE_CLIENT_ENDPOINT = '/api/client/v1/delete';
export const UPDATE_CLIENT_ENDPOINT = '/api/client/v1/update';
export const FIND_SCOPE_ENDPOINT = '/api/scope/v1/find';
export const GET_LDAP_CLIENT_ENDPOINT = '/api/ldap_client/v1/get';
export const CREATE_LDAP_CLIENT_ENDPOINT = '/api/ldap_client/v1/create';
export const DELETE_LDAP_CLIENT_ENDPOINT = '/api/ldap_client/v1/delete';
export const UDPATE_LDAP_CLIENT_ENDPOINT = '/api/ldap_client/v1/update';
export const LIST_LDAP_CLIENTS_ENDPOINT = '/api/ldap_client/v1/list';
export const GET_KERBEROS_REALM_ENDPOINT = '/api/kerberos_realm/v1/get';
export const CREATE_KERBEROS_REALM_ENDPOINT = '/api/kerberos_realm/v1/create';
export const DELETE_KERBEROS_REALM_ENDPOINT = '/api/kerberos_realm/v1/delete';
export const UDPATE_KERBEROS_REALM_ENDPOINT = '/api/kerberos_realm/v1/update';
export const ADD_CORS_DOMAIN_ENDPOINT = '/api/cors_domain/v1/add';
export const UPDATE_CORS_DOMAIN_ENDPOINT = '/api/cors_domain/v1/update';
export const REMOVE_CORS_DOMAIN_ENDPOINT = '/api/cors_domain/v1/remove';
export const GET_CORS_DOMAIN_ENDPOINT = '/api/cors_domain/v1/get';
export const FIND_CORS_DOMAIN_ENDPOINT = '/api/cors_domain/v1/find';
export const LIST_CORS_DOMAIN_ENDPOINT = '/api/cors_domain/v1/list';
export const GET_SAML_ENDPOINT = '/api/saml_app/v1/get';
export const CREATE_SAML_ENDPOINT = '/api/saml_app/v1/create';
export const DELETE_SAML_ENDPOINT = '/api/saml_app/v1/delete';
export const UDPATE_SAML_ENDPOINT = '/api/saml_app/v1/update';
export const LIST_SAML_ENDPOINT = '/api/saml_app/v1/list';
export const REVOKE_TOKEN_URL = '/api/auth/revoke';
