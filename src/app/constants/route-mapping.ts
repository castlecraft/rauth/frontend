export const RouteMapping = {
  social_login: ['admin', 'list', 'social_login'],
  client: ['admin', 'list', 'client'],
  user: ['admin', 'list', 'user'],
  role: ['admin', 'list', 'role'],
  scope: ['admin', 'list', 'scope'],
  ldap_client: ['admin', 'list', 'ldap_client'],
  kerberos_realm: ['admin', 'list', 'kerberos_realm'],
  oauth2_provider: ['admin', 'list', 'oauth2_provider'],
  email: ['admin', 'list', 'email'],
  cors_domain: ['admin', 'list', 'cors_domain'],
  saml_app: ['admin', 'list', 'saml_app'],
  storage: ['admin', 'list', 'storage'],
};
