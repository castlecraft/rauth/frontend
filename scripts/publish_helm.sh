#!/bin/sh

curl --request POST \
  --form token=${RELEASE_TRIGGER_TOKEN} \
  --form ref=main \
  --form variables[FRONTEND_VERSION]="$1" \
  "https://gitlab.com/api/v4/projects/59578333/trigger/pipeline"
